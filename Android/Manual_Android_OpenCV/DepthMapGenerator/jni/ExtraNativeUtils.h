#include <opencv2/opencv.hpp>

using namespace cv;

void Mat2DToVector2D(Mat &src2DMat, vector<vector<Point2f> > &vector2DMat);
void PrintVectorOfVectors2D(vector<vector<Point2f> > &vector2DMat);
double ComputeStereoAverageReprojectionError(
		vector<vector<Point2f> > imagePointsLeft,
		vector<vector<Point2f> > &imagePointsRight, Mat &intrinsicMatrixLeft,
		Mat& intrinsicMatrixRight, Mat& distCoeffMatrixRight,
		Mat& distCoeffMatrixLeft, Mat& fMatrix);
