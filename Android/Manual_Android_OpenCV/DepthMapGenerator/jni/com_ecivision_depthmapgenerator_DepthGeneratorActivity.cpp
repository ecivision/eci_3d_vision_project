#include "com_ecivision_depthmapgenerator_DepthGeneratorActivity.h"
#include <opencv2/opencv.hpp>
#include <android/log.h>
#include "ExtraNativeUtils.h"
#include <stdio.h>

using namespace cv;
using namespace std;


JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeMatLoadProcess(
		JNIEnv * env, jobject javaObject, jlong addrMat, jstring fileName,
		jstring matName) {
	cv::Mat& toLoadMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName, NULL);
	const char *_matName = env->GetStringUTFChars(matName, NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName, cv::FileStorage::READ);
	try {
		matrixFile[_matName] >> toLoadMat;
		matrixFile.release();
		__android_log_write(ANDROID_LOG_ERROR, "Loaded", _fileName);
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
	} catch (int e) {
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
		__android_log_write(ANDROID_LOG_ERROR, "Ecivision-Depth-Generator",
				"Error loading Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeMatSaveProcess(
		JNIEnv * env, jobject javaObject, jlong addrMat, jstring fileName,
		jstring matName) {
	cv::Mat& savedMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName, NULL);
	const char *_matName = env->GetStringUTFChars(matName, NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName, cv::FileStorage::WRITE);
	try {
		matrixFile << _matName << savedMat;
		matrixFile.release();
		__android_log_write(ANDROID_LOG_ERROR, "Saved", _fileName);
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
	} catch (int e) {
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
		__android_log_write(ANDROID_LOG_ERROR, "Ecivision-Stereo",
				"Error saving Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeDepthMapBM(
		JNIEnv *env, jobject javaObject, jlong addrImageLeft,
		jlong addrImageRight, jlong addrValidROILeftMat,
		jlong addrValidROIRightMat, jlong addrDepthMap, jlong addrQMat,
		jint SADWindowSize, jint minDisparity, jint numberOfDisparities,
		jint textureThreshold, jint uniquenessRatio, jint speckleWindowSize,
		jint speckleRange, jint disp12MaxDiff, jint prefilterCap) {

	StereoBM BlockMatcher;
	stringstream buffer;

	cv::Mat& validROILeftMat = *(cv::Mat*) addrValidROILeftMat;
	cv::Mat& validROIRightMat = *(cv::Mat*) addrValidROIRightMat;
	cv::Mat& imageLeft = *(cv::Mat*) addrImageLeft;
	cv::Mat& imageRight = *(cv::Mat*) addrImageRight;
	cv::Mat& depthMap = *(cv::Mat*) addrDepthMap;
	cv::Mat& QMat = *(cv::Mat*) addrQMat;

	Mat imageLeftGray, imageRightGray;
	cvtColor(imageLeft, imageLeftGray, COLOR_BGR2GRAY);
	cvtColor(imageRight, imageRightGray, COLOR_BGR2GRAY);


	const Point* validROILeftPtr = validROILeftMat.ptr<Point>(0);
	const Point* validROIRightPtr = validROIRightMat.ptr<Point>(0);
	Rect validROILeft(*validROILeftPtr, *(validROILeftPtr + 1));
	Rect validROIRight(*validROIRightPtr, *(validROIRightPtr + 1));

	Mat disp,XYZ;

	BlockMatcher.state->roi1 = validROILeft;
	BlockMatcher.state->roi2 = validROIRight;
	BlockMatcher.state->preFilterCap = prefilterCap;
	BlockMatcher.state->SADWindowSize = SADWindowSize;
	BlockMatcher.state->minDisparity = minDisparity;
	BlockMatcher.state->numberOfDisparities = numberOfDisparities;
	BlockMatcher.state->textureThreshold = textureThreshold;
	BlockMatcher.state->uniquenessRatio = uniquenessRatio;
	BlockMatcher.state->speckleWindowSize = speckleWindowSize;
	BlockMatcher.state->speckleRange = speckleRange;
	BlockMatcher.state->disp12MaxDiff = disp12MaxDiff;
	BlockMatcher.state->preFilterSize = 9;
	BlockMatcher(imageLeftGray, imageRightGray, disp);
	//reprojectImageTo3D(disp, XYZ, QMat, true);
	disp.convertTo(depthMap, CV_8U, 255 / (numberOfDisparities * 16.));
}

JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeDepthMapSGBM(
		JNIEnv *env, jobject javaObject, jlong addrImageLeft,
		jlong addrImageRight, jlong addrValidROILeftMat,
		jlong addrValidROIRightMat, jlong addrDepthMap, jlong addrQMat,
		jint SADWindowSize, jint minDisparity, jint numberOfDisparities,
		jint textureThreshold, jint uniquenessRatio, jint speckleWindowSize,
		jint speckleRange, jint disp12MaxDiff, jint prefilterCap) {

	StereoSGBM sgbm;
	stringstream buffer;
	bool gray=false;

	cv::Mat& imageLeft = *(cv::Mat*) addrImageLeft;
	cv::Mat& imageRight = *(cv::Mat*) addrImageRight;
	cv::Mat& depthMap = *(cv::Mat*) addrDepthMap;
	cv::Mat& QMat = *(cv::Mat*) addrQMat;
	Mat imageLeftGray, imageRightGray;

	if(gray){
	cvtColor(imageLeft, imageLeftGray, COLOR_BGR2GRAY);
	cvtColor(imageRight, imageRightGray, COLOR_BGR2GRAY);
	}

	Mat disp,XYZ;

	int cn = imageLeft.channels();
	sgbm.preFilterCap = prefilterCap;
	sgbm.SADWindowSize = SADWindowSize;
	sgbm.minDisparity = minDisparity;
	sgbm.numberOfDisparities = numberOfDisparities;
	sgbm.uniquenessRatio = uniquenessRatio;
	sgbm.speckleWindowSize = speckleWindowSize;
	sgbm.speckleRange = speckleRange;
	sgbm.disp12MaxDiff = disp12MaxDiff;
	sgbm.P1 = 8*cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
	sgbm.P2 = 32*cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
	sgbm.fullDP=false;
	if(gray)
	sgbm(imageLeftGray, imageRightGray, disp);
	else
	sgbm(imageLeft, imageRight, disp);
	disp.convertTo(depthMap, CV_8U, 255 / (numberOfDisparities * 16.));
}
