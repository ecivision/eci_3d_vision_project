/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_ecivision_depthmapgenerator_DepthGeneratorActivity */

#ifndef _Included_com_ecivision_depthmapgenerator_DepthGeneratorActivity
#define _Included_com_ecivision_depthmapgenerator_DepthGeneratorActivity
#ifdef __cplusplus
extern "C" {
#endif
#undef com_ecivision_depthmapgenerator_DepthGeneratorActivity_REMAP_LEFT_OK
#define com_ecivision_depthmapgenerator_DepthGeneratorActivity_REMAP_LEFT_OK 0L
#undef com_ecivision_depthmapgenerator_DepthGeneratorActivity_REMAP_RIGHT_OK
#define com_ecivision_depthmapgenerator_DepthGeneratorActivity_REMAP_RIGHT_OK 1L
/*
 * Class:     com_ecivision_depthmapgenerator_DepthGeneratorActivity
 * Method:    NativeMatLoadProcess
 * Signature: (JLjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeMatLoadProcess
  (JNIEnv *, jobject, jlong, jstring, jstring);

/*
 * Class:     com_ecivision_depthmapgenerator_DepthGeneratorActivity
 * Method:    NativeMatSaveProcess
 * Signature: (JLjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeMatSaveProcess
  (JNIEnv *, jobject, jlong, jstring, jstring);

/*
 * Class:     com_ecivision_depthmapgenerator_DepthGeneratorActivity
 * Method:    NativeDepthMapBM
 * Signature: (JJJJJJIIIIIIIII)V
 */
JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeDepthMapBM
  (JNIEnv *, jobject, jlong, jlong, jlong, jlong, jlong, jlong, jint, jint, jint, jint, jint, jint, jint, jint, jint);

/*
 * Class:     com_ecivision_depthmapgenerator_DepthGeneratorActivity
 * Method:    NativeDepthMapSGBM
 * Signature: (JJJJJJIIIIIIIII)V
 */
JNIEXPORT void JNICALL Java_com_ecivision_depthmapgenerator_DepthGeneratorActivity_NativeDepthMapSGBM
  (JNIEnv *, jobject, jlong, jlong, jlong, jlong, jlong, jlong, jint, jint, jint, jint, jint, jint, jint, jint, jint);

#ifdef __cplusplus
}
#endif
#endif
