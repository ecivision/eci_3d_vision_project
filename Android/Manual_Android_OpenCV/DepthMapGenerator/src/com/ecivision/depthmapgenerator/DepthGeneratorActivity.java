package com.ecivision.depthmapgenerator;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class DepthGeneratorActivity extends Activity {

	private static final String TAG = "Ecivision::DepthGen::Activity";
	private static final int REMAP_LEFT_OK = 0;
	private static final int REMAP_RIGHT_OK = 1;
	private int currentDisplayImageNumber = 1;
	private int numberImages = 16;
	// GUI elements BM
	private ImageView image_view_disparity;

	private TextView numberOfDisparitiesText;
	private SeekBar numberOfDisparitiesSeek;

	private TextView minDisparityText;
	private SeekBar minDisparitySeek;

	private TextView SADWindowSizeText;
	private SeekBar SADWindowSizeSeek;
	private String SADWindowSizeString;

	private TextView textureThreshText;
	private SeekBar textureThreshSeek;

	private TextView uniquenessRatioText;
	private SeekBar uniquenessRatioSeek;

	private TextView MaxDisparityCheckDiffText;
	private SeekBar MaxDisparityCheckDiffSeek;

	private TextView speckleWindowText;
	private SeekBar speckleWindowSeek;

	private TextView speckleRangeText;
	private SeekBar speckleRangeSeek;

	private TextView sgbmSmoothP1Text;
	private SeekBar sgbmSmoothP1Seek;

	private TextView sgbmsmoothP2Text;
	private SeekBar sgbmsmoothP2Seek;

	private TextView prefilterCapText;
	private SeekBar prefilterCapSeek;
	private MySeekBarListener mSeekBarListener;

	// Other UI elements
	private Button computeButton;
	private Spinner stereoMethodSpinner;
	private ArrayAdapter<String> stereoMethodAdapter;

	private enum StereoMethod {
		BM, SGBM, VAR
	}

	private List<String> stereoMethodList = new ArrayList<String>();
	private StereoMethod stereoMethod = StereoMethod.BM;
	private Handler handler;

	DepthParamsObject BMParams;

	private long timerStart;
	private long timerStop;

	private IntentFilter ifilter;
	private Intent batteryStatus;

	// Objects required for image rectification and disparity map calculation.
	private Mat rMat, tMat, eMat, fMat, R1Mat, R2Mat, P1Mat, P2Mat, QMat;
	private Mat imageLeft, imageRight;
	private Mat rectifiedLeft, rectifiedRight;
	private Mat map1Left, map1Right, map2Left, map2Right;
	private Mat cameraMatrixLeft, cameraMatrixRight;
	private Mat distCoeffsLeft, distCoeffsRight;
	private Mat validROIRightMat, validROILeftMat;
	private Mat imageDisp;

	private String imagePathBase;
	private String imageExtension;

	private Context mContext;

	private boolean matsAllocated = false;
	private boolean remapLeftOK = false;
	private boolean remapRightOK = false;
	private boolean alreadyLoaded = false;
	private boolean doRemap = true;

	private int start_battery_level = 0;
	private int end_battery_level = 0;

	Resources mResources;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_depth_generator);
		imagePathBase = Environment.getExternalStorageDirectory()
				+ "/DCIM/LG3D17MAY2/" + "StereoCircles_";
		imageExtension = ".JPG";
		BMParams = new DepthParamsObject();
		mResources = getResources();
		mContext = this;
		// Setup UI elements
		// TextViews
		SADWindowSizeText = (TextView) findViewById(R.id.param_textview1);
		numberOfDisparitiesText = (TextView) findViewById(R.id.param_textview2);
		minDisparityText = (TextView) findViewById(R.id.param_textview3);
		textureThreshText = (TextView) findViewById(R.id.param_textview4);
		uniquenessRatioText = (TextView) findViewById(R.id.param_textview5);
		speckleWindowText = (TextView) findViewById(R.id.param_textview6);
		speckleRangeText = (TextView) findViewById(R.id.param_textview7);
		MaxDisparityCheckDiffText = (TextView) findViewById(R.id.param_textview8);
		prefilterCapText = (TextView) findViewById(R.id.param_textview9);
		// seekbars
		SADWindowSizeSeek = (SeekBar) findViewById(R.id.param_seekbar1);
		numberOfDisparitiesSeek = (SeekBar) findViewById(R.id.param_seekbar2);
		minDisparitySeek = (SeekBar) findViewById(R.id.param_seekbar3);
		textureThreshSeek = (SeekBar) findViewById(R.id.param_seekbar4);
		uniquenessRatioSeek = (SeekBar) findViewById(R.id.param_seekbar5);
		speckleWindowSeek = (SeekBar) findViewById(R.id.param_seekbar6);
		speckleRangeSeek = (SeekBar) findViewById(R.id.param_seekbar7);
		MaxDisparityCheckDiffSeek = (SeekBar) findViewById(R.id.param_seekbar8);
		prefilterCapSeek = (SeekBar) findViewById(R.id.param_seekbar9);
		// Setting seekbar change listener
		mSeekBarListener = new MySeekBarListener();
		SADWindowSizeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		numberOfDisparitiesSeek.setOnSeekBarChangeListener(mSeekBarListener);
		minDisparitySeek.setOnSeekBarChangeListener(mSeekBarListener);
		textureThreshSeek.setOnSeekBarChangeListener(mSeekBarListener);
		uniquenessRatioSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleWindowSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleRangeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		MaxDisparityCheckDiffSeek.setOnSeekBarChangeListener(mSeekBarListener);
		prefilterCapSeek.setOnSeekBarChangeListener(mSeekBarListener);
		// Setting up the seekbars initial positions

		SADWindowSizeText.setText(mResources
				.getString(R.string.param_block_size)
				+ String.valueOf(BMParams.SADWindowSize));
		prefilterCapText.setText(mResources
				.getString(R.string.param_prefilter_cap)
				+ String.valueOf(BMParams.prefilterCap));
		numberOfDisparitiesText.setText(mResources
				.getString(R.string.param_number_disparities)
				+ String.valueOf(BMParams.SADWindowSize));
		minDisparityText.setText(mResources
				.getString(R.string.param_min_disparity)
				+ String.valueOf(BMParams.minDisparity));
		textureThreshText.setText(mResources
				.getString(R.string.param_texture_thresh)
				+ String.valueOf(BMParams.SADWindowSize));
		uniquenessRatioText.setText(mResources
				.getString(R.string.param_uniqueness)
				+ String.valueOf(BMParams.SADWindowSize));
		speckleWindowText.setText(mResources
				.getString(R.string.param_speckle_window)
				+ String.valueOf(BMParams.SADWindowSize));
		speckleRangeText.setText(mResources
				.getString(R.string.param_speckle_range)
				+ String.valueOf(BMParams.SADWindowSize));
		MaxDisparityCheckDiffText.setText(mResources
				.getString(R.string.param_disp12maxdiff)
				+ String.valueOf(BMParams.maxDisparityCheckDiff));

		// Buttons
		computeButton = (Button) findViewById(R.id.compute_button);

		// Spinner
		stereoMethodList.add("BM");
		stereoMethodList.add("SGBM");
		stereoMethodSpinner = (Spinner) findViewById(R.id.stereo_method_spinner);
		stereoMethodAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stereoMethodList);
		stereoMethodAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		stereoMethodSpinner.setAdapter(stereoMethodAdapter);

		stereoMethodSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							stereoMethod = StereoMethod.BM;
						} else {
							if (id == 1) {
								stereoMethod = StereoMethod.SGBM;
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		// ImageView
		image_view_disparity = (ImageView) findViewById(R.id.image_view_disp);

		// Battery status intent
		ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		batteryStatus = mContext.registerReceiver(null, ifilter);
		// thread message handler

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case REMAP_LEFT_OK:
					// if(remapRightOK){
					// doComputation();
					// }
					break;
				case REMAP_RIGHT_OK:
					if (remapLeftOK) {
						doComputation();
					}
					break;
				}
			}
		};

		// Compute Button
		computeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!alreadyLoaded) {
					if (doRemap)
						loadMats();
					imageLeft = Highgui.imread(imagePathBase
							+ String.valueOf(01) + "_L" + imageExtension);
					imageRight = Highgui.imread(imagePathBase
							+ String.valueOf(01) + "_R" + imageExtension);
					alreadyLoaded = true;
				}
				timerStart = android.os.SystemClock.uptimeMillis();
				start_battery_level = batteryStatus.getIntExtra(
						BatteryManager.EXTRA_LEVEL, -1);
				if (doRemap) {
					new Thread(new Runnable() {
						public void run() {
							Imgproc.remap(imageLeft, rectifiedLeft, map1Left,
									map2Left, Imgproc.INTER_LINEAR);
							Message msg = Message.obtain();
							msg.what = REMAP_LEFT_OK;
							remapLeftOK = true;
							handler.sendMessage(msg);
						}
					}).start();
					new Thread(new Runnable() {
						public void run() {
							Imgproc.remap(imageRight, rectifiedRight,
									map1Right, map2Right, Imgproc.INTER_LINEAR);
							Message msg = Message.obtain();
							msg.what = REMAP_RIGHT_OK;
							remapRightOK = true;
							handler.sendMessage(msg);
						}
					}).start();
				} else
					doComputation();
			}
		});

		image_view_disparity.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (currentDisplayImageNumber <= numberImages) {
						imageLeft = Highgui.imread(imagePathBase
								+ String.valueOf(currentDisplayImageNumber)
								+ "_L" + imageExtension);
						imageRight = Highgui.imread(imagePathBase
								+ String.valueOf(currentDisplayImageNumber)
								+ "_R" + imageExtension);
						if (imageLeft.empty() | imageRight.empty()) {
							Toast.makeText(
									mContext,
									"Image #"
											+ String.valueOf(currentDisplayImageNumber)
											+ " not found", Toast.LENGTH_SHORT)
									.show();
						} else {
							timerStart = android.os.SystemClock.uptimeMillis();
							start_battery_level = batteryStatus.getIntExtra(
									BatteryManager.EXTRA_LEVEL, -1);
							if (doRemap) {
								new Thread(new Runnable() {
									public void run() {
										Imgproc.remap(imageLeft, rectifiedLeft,
												map1Left, map2Left,
												Imgproc.INTER_LINEAR);
										Message msg = Message.obtain();
										msg.what = REMAP_LEFT_OK;
										remapLeftOK = true;
										handler.sendMessage(msg);
									}
								}).start();
								new Thread(new Runnable() {
									public void run() {
										Imgproc.remap(imageRight,
												rectifiedRight, map1Right,
												map2Right, Imgproc.INTER_LINEAR);
										Message msg = Message.obtain();
										msg.what = REMAP_RIGHT_OK;
										remapRightOK = true;
										handler.sendMessage(msg);
									}
								}).start();
							} else
								doComputation();
						}
						currentDisplayImageNumber++;
					} else {
						currentDisplayImageNumber = 1;
					}
				}
				return true;
			}
		});

	}

	private void doComputation() {
		new Thread(new Runnable() {
			public void run() {
				// for(int i=0;i<1500;i++){
				imageDisp = new Mat();
				if (stereoMethod == StereoMethod.BM)
					if (doRemap)
						imageDisp = computeDepthMapBM(rectifiedLeft,
								rectifiedRight, validROILeftMat,
								validROIRightMat, QMat, BMParams);
					else
						imageDisp = computeDepthMapBM(imageLeft, imageRight,
								validROILeftMat, validROIRightMat, QMat,
								BMParams);
				else {
					if (stereoMethod == StereoMethod.SGBM)
						if (doRemap)
							imageDisp = computeDepthMapSGBM(rectifiedLeft,
									rectifiedRight, validROILeftMat,
									validROIRightMat, QMat, BMParams);
						else
							imageDisp = computeDepthMapSGBM(imageLeft,
									imageRight, validROILeftMat,
									validROIRightMat, QMat, BMParams);
				}
				Highgui.imwrite(imagePathBase + "Disparity_Map.png", imageDisp);
				// imageDisp = computeDepthMapBM(rectifiedLeft, rectifiedRight,
				// validROILeftMat, validROIRightMat, QMat, BMParams);
				timerStop = android.os.SystemClock.uptimeMillis();

				Log.e(TAG,
						"Execution Time: "
								+ String.valueOf(timerStop - timerStart) + "ms");

				runOnUiThread(new Runnable() {
					public void run() {
						displayLoadedImage(imageDisp, image_view_disparity);
						sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
								Uri.parse("file://"
										+ Environment
												.getExternalStorageDirectory())));
					}
				});
				// }
				end_battery_level = batteryStatus.getIntExtra(
						BatteryManager.EXTRA_LEVEL, -1);
				Log.e(TAG,
						"Battery level difference: "
								+ String.valueOf(end_battery_level
										- start_battery_level));
			}
		}).start();
	}



	private boolean displayLoadedImage(Mat src, ImageView imageSurface) {
		if (src.empty()) {
			Log.e(TAG, "Image structure is empty.");
			return false;
		} else {
			try {
				Bitmap src_bmp = Bitmap.createBitmap(src.width(), src.height(),
						Bitmap.Config.ARGB_8888);
				Mat tmpImage = new Mat();
				if (src.channels() > 1) {
					if (src.channels() == 3) {
						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGR2RGBA);
					} else {

						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGRA2RGBA);
					}
				} else {
					Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_GRAY2RGBA);
				}
				Utils.matToBitmap(tmpImage, src_bmp);
				imageSurface.setImageBitmap(src_bmp);
				return true;
			} catch (RuntimeException e) {
				Log.e(TAG, "Error while trying to show the image: ");
				Log.e(TAG, e.toString());
				return false;
			}
		}
	}

	private boolean allocateMats() {
		QMat = new Mat();
		imageLeft = new Mat();
		imageRight = new Mat();
		rectifiedLeft = new Mat();
		rectifiedRight = new Mat();
		map1Left = new Mat();
		map1Right = new Mat();
		map2Left = new Mat();
		map2Right = new Mat();
		imageDisp = new Mat();
		validROIRightMat = new Mat();
		validROILeftMat = new Mat();
		return true;
	}

	private void loadMats() {

		NativeMatLoadProcess(QMat.getNativeObjAddr(), imagePathBase
				+ "QMatrix.yml", "Q_Matrix");
		NativeMatLoadProcess(map1Left.getNativeObjAddr(), imagePathBase
				+ "Remap1Left.yml", "Remap1_Matrix");
		NativeMatLoadProcess(map2Left.getNativeObjAddr(), imagePathBase
				+ "Remap2Left.yml", "Remap2_Matrix");
		NativeMatLoadProcess(map1Right.getNativeObjAddr(), imagePathBase
				+ "Remap1Right.yml", "Remap1_Matrix");
		NativeMatLoadProcess(map2Right.getNativeObjAddr(), imagePathBase
				+ "Remap2Right.yml", "Remap2_Matrix");
		NativeMatLoadProcess(validROILeftMat.getNativeObjAddr(), imagePathBase
				+ "ValidROILeft.yml", "ROILeft_Matrix");
		NativeMatLoadProcess(validROIRightMat.getNativeObjAddr(), imagePathBase
				+ "ValidROIRight.yml", "ROIRight_Matrix");
	}

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV loaded successfully");
				System.loadLibrary("ecivision-depth-generator");
				matsAllocated = allocateMats();
				Log.e(TAG, "Mats allocated correctly");
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	// Extremely important line, loads OpenCV Library
	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	// Auxiliar functions
	private Mat computeDepthMapBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params) {

		Mat depthMap = new Mat();
		NativeDepthMapBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), valiROILeft.getNativeObjAddr(),
				validROIRight.getNativeObjAddr(), depthMap.getNativeObjAddr(),
				qMat.getNativeObjAddr(), params.SADWindowSize,
				params.minDisparity, params.numberOfDisparities,
				params.textureThresh, params.uniquenessRatio,
				params.speckeWidowSize, params.speckleRange,
				params.maxDisparityCheckDiff, params.prefilterCap);
		return depthMap;
	}

	private Mat computeDepthMapSGBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params) {

		Mat depthMap = new Mat();
		NativeDepthMapSGBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), valiROILeft.getNativeObjAddr(),
				validROIRight.getNativeObjAddr(), depthMap.getNativeObjAddr(),
				qMat.getNativeObjAddr(), params.SADWindowSize,
				params.minDisparity, params.numberOfDisparities,
				params.textureThresh, params.uniquenessRatio,
				params.speckeWidowSize, params.speckleRange,
				params.maxDisparityCheckDiff, params.prefilterCap);
		return depthMap;
	}

	// Native functions
	public native void NativeMatLoadProcess(long addrMat, String fileName,
			String matName);

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	public native void NativeDepthMapBM(long addrImageLeft,
			long addrImageRight, long addrValidROILeftMat,
			long addrValidROIRightMat, long addrDepthMap, long addrQMat,
			int SADWindow, int minDisparity, int numberDisparities,
			int textureThreshold, int uniquenessRatio, int speckleWindowSize,
			int speckleRange, int disp12MaxDiff, int prefilterCap);

	public native void NativeDepthMapSGBM(long addrImageLeft,
			long addrImageRight, long addrValidROILeftMat,
			long addrValidROIRightMat, long addrDepthMap, long addrQMat,
			int SADWindow, int minDisparity, int numberDisparities,
			int textureThreshold, int uniquenessRatio, int speckleWindowSize,
			int speckleRange, int disp12MaxDiff, int prefilterCap);

	private class MySeekBarListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			switch (seekBar.getId()) {
			case R.id.param_seekbar1:
				if (progress < 5) {
					BMParams.SADWindowSize = 5;
				} else {
					BMParams.SADWindowSize = (2 * progress + 1);
				}
				SADWindowSizeText.setText(mResources
						.getString(R.string.param_block_size)
						+ String.valueOf(BMParams.SADWindowSize));
				break;
			case R.id.param_seekbar2:
				if (progress < 1) {
					BMParams.numberOfDisparities = 16;
				} else {
					BMParams.numberOfDisparities = progress * 16;
				}
				numberOfDisparitiesText.setText(mResources
						.getString(R.string.param_number_disparities)
						+ String.valueOf(BMParams.numberOfDisparities));
				break;
			case R.id.param_seekbar3:
				BMParams.minDisparity = progress * 16;
				minDisparityText.setText(mResources
						.getString(R.string.param_min_disparity)
						+ String.valueOf(BMParams.minDisparity));
				break;
			case R.id.param_seekbar4:
				BMParams.textureThresh = progress * 16;
				textureThreshText.setText(mResources
						.getString(R.string.param_texture_thresh)
						+ String.valueOf(BMParams.textureThresh));
				break;

			case R.id.param_seekbar5:
				if (progress <= 0)
					BMParams.uniquenessRatio = 1;
				else
					BMParams.uniquenessRatio = progress;
				uniquenessRatioText.setText(mResources
						.getString(R.string.param_uniqueness)
						+ String.valueOf(BMParams.uniquenessRatio));
				break;
			case R.id.param_seekbar6:
				BMParams.speckeWidowSize = progress;
				speckleWindowText.setText(mResources
						.getString(R.string.param_speckle_window)
						+ String.valueOf(BMParams.speckeWidowSize));
				break;
			case R.id.param_seekbar7:
				BMParams.speckleRange = progress;
				speckleRangeText.setText(mResources
						.getString(R.string.param_speckle_range)
						+ String.valueOf(BMParams.speckleRange));
				break;
			case R.id.param_seekbar8:
				BMParams.maxDisparityCheckDiff = progress;
				MaxDisparityCheckDiffText.setText(mResources
						.getString(R.string.param_disp12maxdiff)
						+ String.valueOf(BMParams.maxDisparityCheckDiff));
				break;
			case R.id.param_seekbar9:
				if (progress < 1)
					BMParams.prefilterCap = 1;
				else
					BMParams.prefilterCap = progress;

				prefilterCapText.setText(mResources
						.getString(R.string.param_prefilter_cap)
						+ String.valueOf(BMParams.prefilterCap));
				break;
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			timerStart = android.os.SystemClock.uptimeMillis();
			start_battery_level = batteryStatus.getIntExtra(
					BatteryManager.EXTRA_LEVEL, -1);
			doComputation();
			Log.e(TAG, "released Seekbar");
		}

	}

}
