package com.ecivision.calibrationsample;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ecivision.calibrationsample.CalibrationObject.CalibType;

public class CalibrationActivity extends Activity implements
		CvCameraViewListener2, OnClickListener, OnCheckedChangeListener {
	private static final String TAG = "OCVSample::Activity";

	private CameraBridgeViewBase mOpenCvCameraView;
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				System.loadLibrary("ecivision-calibration");
				mOpenCvCameraView.enableView();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	public native void NativeMatLoadProcess(long addrMat, String fileName,
			String matName);

	public native void NativeUndistortProcess(long addrSrc, long addrDst,
			long addrIntr, long addrDist);


	private enum StateMachine {
		IDLE, CAPTURE_STATE, PROCESS_STATE, UNDISTORT_STATE
	}

	static final int toggleCamera = Menu.FIRST;
	static final int toggleCalibType = Menu.FIRST + 1;
	private int imageWidth;
	private int imageHeight;

	private CalibrationObject calibCamera1;
	private boolean calibrated = false;
	private boolean saveImages = false;
	private int group1Id = 1;
	private double error=0,error2=0;
	private static int numberofCaptures = 4;
	private int captureCount = 0;
	private StateMachine currentState;
	static int patternHeight = 0;
	static int patternWidth = 0;
	private Size patternSize;
	private CalibType calibType = CalibType.CALIB_CIRCLES;
	private boolean CameraNative = true;
	private boolean calibButtonOK = false;
	private boolean loadedMatrixes = false;

	// UI elements
	private Mat loadedIntrinsicsMat;
	private Mat loadedDistCoeffsMat;
	private Mat undistordedImage;
	private Button startButton;
	private Button calibButton;
	private Button resetButton;
	private boolean widthTextOk = false;
	private boolean heightTextOk = false;
	private boolean numCapturesTextOk = false;
	private Spinner calibTypeSpinner;
	private EditText patternWidthEditText;
	private EditText patternHeightEditText;
	private EditText patternNumberEditText;
	private ArrayAdapter<String> calibTypeAdapter;
	private List<String> calibTypeList = new ArrayList<String>();
	private Context mContext;
	private CheckBox saveImageCheckBox;
	private CheckBox undistortImageBox;

	public CalibrationActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_calibration);
		if (CameraNative) {
			mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.calibration_surface_native_view);
		} else {
			mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.calibration_surface_java_view);
		}

		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(this);
		mOpenCvCameraView.setMaxFrameSize(640, 480);
		currentState = StateMachine.IDLE;
		mContext = this;

		startButton = (Button) findViewById(R.id.start_button);
		calibButton = (Button) findViewById(R.id.calibrate_button);
		resetButton = (Button) findViewById(R.id.reset_button);
		calibTypeSpinner = (Spinner) findViewById(R.id.calibtype_spinner);
		patternWidthEditText = (EditText) findViewById(R.id.patternwidth_edittext);
		patternHeightEditText = (EditText) findViewById(R.id.patternheight_edittext);
		patternNumberEditText = (EditText) findViewById(R.id.patternNumber_edittext);
		saveImageCheckBox = (CheckBox) findViewById(R.id.save_checkbox);
		undistortImageBox = (CheckBox) findViewById(R.id.undistort_checkbox);
		resetButton.setEnabled(false);

		// Setting up the spinner
		calibTypeList.add("Chessboard");
		calibTypeList.add("Circles");
		calibTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, calibTypeList);
		calibTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calibTypeSpinner.setAdapter(calibTypeAdapter);

		calibTypeSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							calibType = CalibType.CALIB_CHESSBOARD;
						} else {
							if (id == 1) {
								calibType = CalibType.CALIB_CIRCLES;
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		// Setting up pattern widht and height edit texts
		patternHeightEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Heigth",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						heightTextOk = false;
					} else {
						patternHeight = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Height: " + patternHeight,
								Toast.LENGTH_SHORT).show();
						heightTextOk = true;
						if (widthTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					heightTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternWidthEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Width",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						widthTextOk = false;
					} else {
						patternWidth = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Width: " + patternWidth,
								Toast.LENGTH_SHORT).show();
						widthTextOk = true;
						if (heightTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					widthTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternNumberEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 3) {
						Toast.makeText(
								mContext,
								"Please enter a valid positive number of Patterns",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						numCapturesTextOk = false;
					} else {
						numberofCaptures = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Numero de Capturas: " + numberofCaptures,
								Toast.LENGTH_SHORT).show();
						numCapturesTextOk = true;
						if (heightTextOk & widthTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					numCapturesTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		// Funcionalidad del boton de arranque de calibracion.Start Button.
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				patternSize = new Size(patternWidth, patternHeight);
				patternNumberEditText.setEnabled(false);
				patternHeightEditText.setEnabled(false);
				patternWidthEditText.setEnabled(false);
				saveImageCheckBox.setEnabled(false);
				calibTypeSpinner.setEnabled(false);
				resetButton.setEnabled(true);
				calibCamera1 = new CalibrationObject(patternSize, calibType,
						saveImages);
				calibCamera1.allocateImages(imageWidth, imageHeight);
				currentState = StateMachine.CAPTURE_STATE;
				startButton.setEnabled(false);
				calibButton.setEnabled(true);
			}
		});

		resetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				calibCamera1.close();
				try {
					calibCamera1.finalize();
				} catch (Throwable e) {
					e.printStackTrace();
				}
				patternHeightEditText.setText("");
				patternWidthEditText.setText("");
				patternNumberEditText.setText("");
				currentState = StateMachine.IDLE;
				patternHeight = 0;
				patternWidth = 0;
				loadedMatrixes = false;
				widthTextOk = false;
				heightTextOk = false;
				numCapturesTextOk = false;
				calibButtonOK = false;
				calibrated = false;
				numberofCaptures = 4;
				captureCount = 0;
				patternNumberEditText.setEnabled(true);
				patternHeightEditText.setEnabled(true);
				patternWidthEditText.setEnabled(true);
				saveImageCheckBox.setEnabled(true);
				calibTypeSpinner.setEnabled(true);
				currentState = StateMachine.IDLE;
				startButton.setEnabled(false);
				calibButton.setEnabled(false);
				resetButton.setEnabled(false);
			}
		});

		calibButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (calibButtonOK) {
					patternNumberEditText.setEnabled(false);
					patternHeightEditText.setEnabled(false);
					patternWidthEditText.setEnabled(false);
					calibTypeSpinner.setEnabled(false);
					currentState = StateMachine.IDLE;
					startButton.setEnabled(false);
					calibButton.setEnabled(false);
					new Thread(new Runnable() {
						public void run() {
							error = calibCamera1.RunCalibration();
							error2 = calibCamera1.computeReprojectionErrors();
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									Toast.makeText(mContext,
											"Camera Calibrated Successfully",
											Toast.LENGTH_SHORT).show();
									Toast.makeText(mContext,
											"Calibration Error is: "+String.valueOf(error),
											Toast.LENGTH_SHORT).show();
									Log.e(TAG,"Calibration Error is: "+String.valueOf(error));
									Log.e(TAG,"Reprojection Error is: "+String.valueOf(error2));
								}
							});
							calibrated = true;
							NativeMatSaveProcess(calibCamera1
									.getIntrinsicsMatrix().getNativeObjAddr(),
									Environment.getExternalStorageDirectory()
											+ "/DCIM/CameraMatrix.yml",
									"Camera_Matrix");
							NativeMatSaveProcess(calibCamera1
									.getDistCoeffsMatrix().getNativeObjAddr(),
									Environment.getExternalStorageDirectory()
											+ "/DCIM/DistCoeffs.yml",
									"Dist_Coeffs");
							sendBroadcast(new Intent(
									Intent.ACTION_MEDIA_MOUNTED,
									Uri.parse("file://"
											+ Environment
													.getExternalStorageDirectory())));
						}
					}).start();
				}
			}
		});

		saveImageCheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						saveImages = isChecked;
					}
				});

		undistortImageBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						if (calibrated) {
							if (isChecked)
								currentState = StateMachine.UNDISTORT_STATE;
							else
								currentState = StateMachine.IDLE;
						}
					}
				});

	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		menu.add(group1Id, toggleCalibType, toggleCalibType,
				"Toggle Calibration Method");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case toggleCalibType:
			if (calibType == CalibType.CALIB_CHESSBOARD) {
				calibType = CalibType.CALIB_CIRCLES;
				Toast.makeText(this, "Changed to Circles Calibration method",
						Toast.LENGTH_SHORT).show();
			} else {
				calibType = CalibType.CALIB_CHESSBOARD;
				Toast.makeText(this,
						"Changed to Chessboard Calibration method",
						Toast.LENGTH_SHORT).show();
			}
		}
		return true;
	}

	public void onCameraViewStarted(int width, int height) {
		imageWidth = width;
		imageHeight = height;
		loadedIntrinsicsMat = new Mat();
		loadedDistCoeffsMat = new Mat();
		undistordedImage = new Mat();
	}

	public void onCameraViewStopped() {
	}

	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		if (currentState == StateMachine.IDLE) {
			return inputFrame.rgba();
		} else {
			if (currentState == StateMachine.CAPTURE_STATE) {
				if (numberofCaptures > captureCount) {
					captureCount = calibCamera1.CalibProcess(inputFrame.rgba());
				} else {
					currentState = StateMachine.PROCESS_STATE;
					sendBroadcast(new Intent(
							Intent.ACTION_MEDIA_MOUNTED,
							Uri.parse("file://"
									+ Environment.getExternalStorageDirectory())));
				}
				return calibCamera1.returnCornersImage();
			} else {
				if (currentState == StateMachine.PROCESS_STATE) {
					if (!calibrated) {
						calibButtonOK = true;
					} else {
						currentState = StateMachine.IDLE;
					}
				} else {
					if (currentState == StateMachine.UNDISTORT_STATE) {
						if (calibrated) {
							if (!loadedMatrixes) {
								NativeMatLoadProcess(
										loadedIntrinsicsMat.getNativeObjAddr(),
										Environment
												.getExternalStorageDirectory()
												+ "/DCIM/CameraMatrix.yml",
										"Camera_Matrix");
								NativeMatLoadProcess(
										loadedDistCoeffsMat.getNativeObjAddr(),
										Environment
												.getExternalStorageDirectory()
												+ "/DCIM/DistCoeffs.yml",
										"Dist_Coeffs");
								Log.e(TAG, loadedIntrinsicsMat.dump());
								Log.e(TAG, loadedDistCoeffsMat.dump());
								loadedMatrixes = true;
							}
							NativeUndistortProcess(inputFrame.rgba()
									.getNativeObjAddr(),
									undistordedImage.getNativeObjAddr(),
									loadedIntrinsicsMat.getNativeObjAddr(),
									loadedDistCoeffsMat.getNativeObjAddr());
							return undistordedImage;
						} else
							return inputFrame.rgba();
					}
				}
				return inputFrame.rgba();
			}
		}
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

	}
}
