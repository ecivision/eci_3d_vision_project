package com.manual.opencv_1;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class CVActivity extends Activity implements CvCameraViewListener {

	private native void NativeImageProcess(long addrSrc, long addrDst,
			double angle);


	// Variables auxiliares
	private static final String TAG = "OpenCV::Calib::Activity";
	private CameraBridgeViewBase cameraBack;
	private Mat cameraImage;
	private Mat cameraTemp;
	private SeekBar seek_angle;
	private TextView text_angle;
	private float rotAngle;
	private boolean CameraNative=true;
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.v(TAG, "OpenCV Library Loaded Successfully");
				System.loadLibrary("opencv_ex1");
				cameraBack.setMaxFrameSize(320, 240);
				cameraBack.enableView();
				cameraBack.enableFpsMeter();
				break;
			}
			default: {
				Log.e(TAG, "ERROR Could not Load OpenCV Library");
				super.onManagerConnected(status);
				break;
			}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cv);
		if(CameraNative)
			cameraBack = (CameraBridgeViewBase) findViewById(R.id.calibration_surface_native_view);
		else{
			cameraBack = (CameraBridgeViewBase) findViewById(R.id.calibration_surface_java_view);	
		}
		cameraBack.setVisibility(SurfaceView.VISIBLE);
		cameraBack.setCvCameraViewListener(this);
		seek_angle = (SeekBar) findViewById(R.id.seek_angle);
		text_angle = (TextView) findViewById(R.id.text_angle);
		text_angle.setText("Angulo: " + 0 + " Grados");
		rotAngle = 0;

		seek_angle.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				text_angle.setText("Angulo: " + progress + " Grados");
				rotAngle = (float) progress;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.cv, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem Item) {
		return true;
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
		cameraImage = new Mat(height, width, CvType.CV_8UC4);
		cameraTemp = new Mat(height, width, CvType.CV_8UC4);
	}

	@Override
	public void onCameraViewStopped() {
		cameraImage.release();
		cameraTemp.release();
	}

	@Override
	public void onPause() {
		if (cameraBack != null)
			cameraBack.disableView();
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	@Override
	public Mat onCameraFrame(Mat inputFrame) {
		inputFrame.copyTo(cameraImage);
		NativeImageProcess(cameraImage.getNativeObjAddr(),
				cameraTemp.getNativeObjAddr(), rotAngle);
		return cameraTemp;
	}
}