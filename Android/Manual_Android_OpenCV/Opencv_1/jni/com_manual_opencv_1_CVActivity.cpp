#include "com_manual_opencv_1_CVActivity.h"

#include <opencv2/opencv.hpp>
#include <android/log.h>

JNIEXPORT void JNICALL Java_com_manual_opencv_11_CVActivity_NativeImageProcess(
		JNIEnv *, jobject, jlong addrSrc, jlong addrDst, jdouble angle) {
	cv::Mat& src = *(cv::Mat*) addrSrc;
	cv::Mat& dst = *(cv::Mat*) addrDst;
	__android_log_print(ANDROID_LOG_DEBUG, "libopencv_ex1",
			"Images loaded correctly");
	cv::Point2f src_center(src.cols / 2.0F, src.rows / 2.0F);
	cv::Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
	cv::warpAffine(src, dst, rot_mat, src.size());
	__android_log_print(ANDROID_LOG_DEBUG,"libopencv_ex1","Images processed_correctly");
	}
