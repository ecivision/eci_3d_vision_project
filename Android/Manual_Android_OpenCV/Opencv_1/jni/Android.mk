LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
include $(OPENCV_ANDROID_SDK)/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := opencv_ex1
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_SRC_FILES := com_manual_opencv_1_CVActivity.cpp
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)