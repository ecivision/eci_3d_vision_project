package com.manual.hello;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity { //
	static {
		System.loadLibrary("hello_native");
	}

	// Funciones
	private native float NativeOperation(float var1, float var2);

	// Variables
	EditText edtxt_val1;
	EditText edtxt_val2;
	TextView txt_Result;
	Button calcBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		edtxt_val1 = (EditText) findViewById(R.id.edtxt_val1);
		edtxt_val2 = (EditText) findViewById(R.id.edtxt_val2);
		txt_Result = (TextView) findViewById(R.id.txt_Result);
		calcBtn = (Button) findViewById(R.id.calcBtn);
		calcBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				float result = 0;
				result = NativeOperation(
						Float.valueOf(edtxt_val1.getText().toString()),
						Float.valueOf(edtxt_val2.getText().toString()));
				txt_Result.setText(Float.toString(result));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
