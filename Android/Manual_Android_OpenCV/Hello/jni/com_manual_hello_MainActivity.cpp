#include "com_manual_hello_MainActivity.h"
#include <math.h>

JNIEXPORT jfloat JNICALL Java_com_manual_hello_MainActivity_NativeOperation
  (JNIEnv *, jobject, jfloat var1, jfloat var2){
    float result=0.0;
  result = sqrt((pow(var1,2)+pow(var2,2))/2.0);
  return result;
}
