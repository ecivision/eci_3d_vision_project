#ifndef TALLER_5_H
#define TALLER_5_H

#include <QMainWindow>
#include <QTimer>
#include <opencv.hpp>

namespace Ui {
    class Taller_5;
}

class Taller_5 : public QMainWindow
{
    Q_OBJECT

public:
    explicit Taller_5(QWidget *parent = 0);
    ~Taller_5();

private slots:
    void on_push_Gaussian_clicked();
    void on_push_Close_clicked();

    void on_pushContinue_clicked();

    void on_pushCalibrar_clicked();
    void update_info();
    void update_info2();

private:
    Ui::Taller_5 *ui;
    int n_boards;
    int board_dt;
    int board_w;
    int board_h;
    int found;
    int count;
    QTimer *timer1;
    QTimer *timer2;
    bool Finish,Cal;
    CvMat* image_points	;
    CvMat* object_points;
    CvMat* point_counts;
    CvMat* intrinsic_matrix;
    CvMat* distortion_coeffs;
    CvPoint2D32f* corners;
    int corner_count;
    int successes;
    int step, frame;
    int board_n ;
    CvSize board_sz;
    CvCapture* capture;
    IplImage *image;
    IplImage *gray_image;
    CvMat* object_points2;
    CvMat* image_points2;
    CvMat* point_counts2;
    CvMat *intrinsic;
    CvMat *distortion;
    IplImage* mapx;
    IplImage* mapy;
};

#endif // TALLER_5_H
