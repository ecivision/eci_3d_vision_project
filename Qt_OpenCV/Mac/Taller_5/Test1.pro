#-------------------------------------------------
#
# Project created by QtCreator 2013-03-15T13:52:46
#
#-------------------------------------------------

QT       += core gui

TARGET = Test1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH +='C:\OpenCV_QT\release\include'
LIBS += -LC:\OpenCV_QT\release\lib -lopencv_core242 -lopencv_imgproc242 -lopencv_highgui242 -lopencv_ml242 -lopencv_video242 -lopencv_features2d242 -lopencv_calib3d242 -lopencv_objdetect242 -lopencv_contrib242 -lopencv_legacy242 -lopencv_flann242 -lopencv_nonfree242 -lopencv_photo242
