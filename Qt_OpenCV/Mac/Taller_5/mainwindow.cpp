#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QTimer>
#include <stdio.h>
#include <math.h>
#include <complex>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QWidget::move(459,600);
    ui->pushCapture->setEnabled(false);
    ui->pushCalibrate->setEnabled(false);
    ui->pushReset->setEnabled(false);
    Finish=false;
    timer1 = new QTimer(this);
    timer2 = new QTimer(this);
    patternsfound=0;
    Cal=false;
//    successes=0;
//    step=0;
//    frame=0;
    n_boards=0;
    count=0;
    //board_dt=20;
    board_w = 8; // Board width in squares
    board_h = 6; // Board height
    n_boards = 10; // Number of boards
    board_n = board_w * board_h;
    for(int j=0;j<board_n;j++)
           obj.push_back(cv::Point3f(j/board_h, j%board_w, 0.0f));
    connect(timer1,SIGNAL(timeout()),this,SLOT(update_info()));
    connect(timer2,SIGNAL(timeout()),this,SLOT(update_info2()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushStart_clicked()
{
        patternSize = cv::Size(board_w,board_h);
        //camara derecha
        cv::namedWindow("Imagen Derecha");
        cv::moveWindow("Imagen Derecha",0,0);

        capture_right.open(1);
        capture_right.set(CV_CAP_PROP_FRAME_WIDTH,320);
        capture_right.set(CV_CAP_PROP_FRAME_HEIGHT,240);
        capture_right>>src_right;
        cv::imshow("Imagen Derecha",src_right);


        //camara izquierda
//        cv::namedWindow("Imagen Izquierda");
//        cv::moveWindow("Imagen Izquierda",656,0);

//        capture_left.open(0);
//        capture_left >> src_left;
//        cv::imshow("Imagen Izquierda",src_left);

        //timer1->start(15);
        ui->pushCapture->setEnabled(true);
}

void MainWindow::on_pushCapture_clicked()
{
    capture_right >> src_right;
//    capture_left >> src_left;
    cv::imshow("Imagen Derecha",src_right);
//    cv::imshow("Imagen Izquierda",src_left);
    // Find chessboard corners:
    count=0;

    cv::cvtColor(src_right,srcGray_right,CV_BGR2GRAY);
    patternsfound = cv::findChessboardCorners(srcGray_right,patternSize,chessCorners_right,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
    if(patternsfound)
    {
        qDebug()<<"Chessboard found!";
    }
    cv::imshow("Imagen Derecha",src_right);
//    cv::imshow("Imagen Izquierda",src_left);
}

void MainWindow::on_pushCalibrate_clicked()
{

}

void MainWindow::on_pushReset_clicked()
{

}


void MainWindow::update_info()
{
    capture_right >> src_right;
//    capture_left >> src_left;
    cv::imshow("Imagen Derecha",src_right);
//    cv::imshow("Imagen Izquierda",src_left);
    // Find chessboard corners:
    count=0;

    cv::cvtColor(src_right,srcGray_right,CV_BGR2GRAY);
    patternsfound = cv::findChessboardCorners(srcGray_right,patternSize,chessCorners_right,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
    if(patternsfound)
    {
        qDebug()<<"Chessboard found!";
    }
    cv::imshow("Imagen Derecha",src_right);
//    cv::imshow("Imagen Izquierda",src_left);
}


void MainWindow::update_info2()
{
//    IplImage *t = cvCloneImage( image );
//    cvShowImage( "Calibration", image ); // Show raw image
//    cvRemap( t, image, mapx, mapy ); // undistort image
//    cvReleaseImage( &t );
//    cvShowImage( "Undistort", image ); // Show corrected image
//    cvWaitKey( 15 );
//    image = cvQueryFrame( capture );
}

void MainWindow::on_pushClose_clicked()
{
 timer1->stop();
 if(capture_right.isOpened())
 capture_right.release();
 if(capture_left.isOpened())
 capture_left.release();
 try{
 cv::destroyAllWindows();
 }
 catch(cv::Exception e)
 {
     qDebug()<<"No highui windows opened";
 }
}
