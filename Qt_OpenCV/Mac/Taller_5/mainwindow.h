#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <opencv.hpp>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_pushStart_clicked();

    void on_pushCapture_clicked();

    void on_pushCalibrate_clicked();

    void on_pushReset_clicked();

    void update_info();

    void update_info2();

    void on_pushClose_clicked();

private:
    Ui::MainWindow *ui;
        int n_boards;
        int board_dt;
        int board_w;
        int board_h;
        int board_n;
        int found;
        int count;
        QTimer *timer1;
        QTimer *timer2;
        bool Finish,Cal;
    //new cpp interface
        cv::Mat src_right;
        cv::Mat dst_right;
        cv::Mat srcGray_right;
        cv::vector<cv::Point3f> objPoints_right;
        cv::vector<cv::Point2f> imgPoints_right;
        cv::VideoCapture capture_right;
        cv::Size patternSize;
        cv::Mat src_left;
        cv::Mat dst_left;
        cv::Mat srcGray_left;
        cv::vector<cv::Point3f> objPoints_left;
        cv::vector<cv::Point2f> imgPoints_left;
        cv::vector<cv::Point2f> chessCorners_right;
        cv::vector<cv::Point2f> chessCorners_left;
        cv::VideoCapture capture_left;
        int success_right;
        int success_left;
        cv::vector<cv::Point3f> obj;
        bool patternsfound;
};

#endif // MAINWINDOW_H
