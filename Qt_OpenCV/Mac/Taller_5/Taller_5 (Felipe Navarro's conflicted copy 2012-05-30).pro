#-------------------------------------------------
#
# Project created by QtCreator 2011-10-08T22:01:16
#
#-------------------------------------------------

QT       += core gui

TARGET = Taller_5
TEMPLATE = app


SOURCES += main.cpp\
        taller_5.cpp

HEADERS  += taller_5.h

FORMS    += taller_5.ui

INCLUDEPATH +='/usr/local/include/opencv'
INCLUDEPATH +='/usr/local/include/opencv2'
INCLUDEPATH +='/usr/include'
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann -lfftw3 -lm
