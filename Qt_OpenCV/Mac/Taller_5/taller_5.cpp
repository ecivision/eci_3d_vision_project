
// Felipe Navarro, Mauricio Asprilla

#include <stdio.h>
#include <math.h>
#include <complex>
#include "taller_5.h"
#include "ui_taller_5.h"
#include <opencv.hpp>
#include <QSettings>
#include <QTimer>
//assert( capture );
// Allocate Sotrage

Taller_5::Taller_5(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Taller_5)
{
    ui->setupUi(this);
    QWidget::move(459,600);
    ui->pushContinue->setEnabled(false);
    ui->pushCalibrar->setEnabled(false);
    ui->push_Close->setEnabled(false);
    Finish=false;
    timer1 = new QTimer(this);
    timer2 = new QTimer(this);
    Cal=false;
    successes=0;
    step=0;
    frame=0;
    n_boards=0;
    count=0;
    //board_dt=20;
    board_w = 5; // Board width in squares
    board_h = 8; // Board height
    n_boards = 10; // Number of boards
    board_n = board_w * board_h;
    connect(timer1,SIGNAL(timeout()),this,SLOT(update_info()));
    connect(timer2,SIGNAL(timeout()),this,SLOT(update_info2()));
}

Taller_5::~Taller_5()
{
    delete ui;
}

void Taller_5::on_push_Gaussian_clicked()
{
    ui->pushContinue->setEnabled(true);
    ui->push_Close->setEnabled(true);
    ui->push_Gaussian->setEnabled(false);
    cvNamedWindow("Imagen");
    cvMoveWindow("Imagen",0,0);
    image_points = cvCreateMat( n_boards*board_n, 2, CV_32FC1 );
    object_points= cvCreateMat( n_boards*board_n, 3, CV_32FC1 );
    point_counts = cvCreateMat( n_boards, 1, CV_32SC1 );
    intrinsic_matrix= cvCreateMat( 3, 3, CV_32FC1 );
    distortion_coeffs	= cvCreateMat( 5, 1, CV_32FC1 );
    corners = new CvPoint2D32f[ board_n ];
    board_sz = cvSize( board_w, board_h );
    capture = cvCreateCameraCapture(0);

    image = cvQueryFrame( capture );
    gray_image = cvCreateImage(cvGetSize(image),8,1);

    timer1->start(15);
}

void Taller_5::update_info()
{
    image = cvQueryFrame( capture );
    // Find chessboard corners:
    count=0;
    found = cvFindChessboardCorners( image, board_sz, corners,
            &corner_count, CV_CALIB_CB_ADAPTIVE_THRESH |
            CV_CALIB_CB_FILTER_QUADS );

    // Get subpixel accuracy on those corners
    cvCvtColor( image, gray_image, CV_BGR2GRAY );
    cvFindCornerSubPix( gray_image, corners, corner_count,
    cvSize( 11, 11 ),cvSize( -1, -1 ),
    cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

    // Draw it
    cvDrawChessboardCorners( image, board_sz, corners,
                             corner_count, found );
    cvShowImage( "Imagen", image );
    //cvWaitKey(7);
}

void Taller_5::on_pushContinue_clicked()
{
    cvNamedWindow("Chessboard");
    cvMoveWindow("Chessboard",640,0);
    if( successes < n_boards ){

                    cvShowImage( "Chessboard", image );
                    cvWaitKey(10);
                    // If we got a good board, add it to our data
                    if( corner_count == board_n ){
                            step = successes*board_n;
                            for( int i=step, j=0; j < board_n; ++i, ++j ){
                                    CV_MAT_ELEM( *image_points, float, i, 0 ) =
                                            corners[j].x;
                                    CV_MAT_ELEM( *image_points, float, i, 1 ) =
                                            corners[j].y;
                                    CV_MAT_ELEM( *object_points, float, i, 0 ) =
                                            j/board_w;
                                    CV_MAT_ELEM( *object_points, float, i, 1 ) =
                                            j%board_w;
                                    CV_MAT_ELEM( *object_points, float, i, 2 ) = 0.0f;
                            }
                            CV_MAT_ELEM( *point_counts, int, successes, 0 ) = board_n;
                            successes++;
                            if(successes==n_boards)
                                ui->pushCalibrar->setEnabled(true);

                    }
                    ui->label_Success->setNum(successes);
                    cvWaitKey( 15 );
    }
}

void Taller_5::on_pushCalibrar_clicked()
{

    Cal = true;
    timer1->stop();
    cvDestroyWindow("Chessboard");
    cvDestroyWindow("Imagen");
    //Capture Corner views loop until we've got n_boards
    // succesful captures (all corners on the board are found)
    cvNamedWindow("Calibration");
    cvMoveWindow("Calibration",0,0);
    // Allocate matrices according to how many chessboards found
    object_points2 = cvCreateMat( successes*board_n, 3, CV_32FC1 );
    image_points2 = cvCreateMat( successes*board_n, 2, CV_32FC1 );
    point_counts2 = cvCreateMat( successes, 1, CV_32SC1 );

    // Transfer the points into the correct size matrices
    for( int i = 0; i < successes*board_n; ++i ){
            CV_MAT_ELEM( *image_points2, float, i, 0) = CV_MAT_ELEM( *image_points, float, i, 0 );
            CV_MAT_ELEM( *image_points2, float, i, 1) = CV_MAT_ELEM( *image_points, float, i, 1 );
            CV_MAT_ELEM( *object_points2, float, i, 0) = CV_MAT_ELEM( *object_points, float, i, 0 );
            CV_MAT_ELEM( *object_points2, float, i, 1) = CV_MAT_ELEM( *object_points, float, i, 1 );
            CV_MAT_ELEM( *object_points2, float, i, 2) = CV_MAT_ELEM( *object_points, float, i, 2 );
    }

    for( int i=0; i < successes; ++i ){
            CV_MAT_ELEM( *point_counts2, int, i, 0 ) = CV_MAT_ELEM( *point_counts, int, i, 0 );
    }
    cvReleaseMat( &object_points );
    cvReleaseMat( &image_points );
    cvReleaseMat( &point_counts );

    // At this point we have all the chessboard corners we need
    // Initiliazie the intrinsic matrix such that the two focal lengths
    // have a ratio of 1.0

    CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0;
    CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0;

    // Calibrate the camera
    cvCalibrateCamera2( object_points2, image_points2, point_counts2,
                        cvGetSize( image ),intrinsic_matrix, distortion_coeffs,
                        NULL, NULL, CV_CALIB_FIX_ASPECT_RATIO );

    // Save the intrinsics and distortions
    cvSave( "Intrinsics.xml", intrinsic_matrix );
    cvSave( "Distortion.xml", distortion_coeffs );

    // Example of loading these matrices back in
    intrinsic = (CvMat*)cvLoad( "Intrinsics.xml" );
    distortion = (CvMat*)cvLoad( "Distortion.xml" );

    // Build the undistort map that we will use for all subsequent frames
    mapx = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
    mapy = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
    cvInitUndistortMap( intrinsic, distortion, mapx, mapy );

    // Run the camera to the screen, now showing the raw and undistorted image
    cvNamedWindow( "Undistort" );
    cvMoveWindow("Undistort",640,0);
    timer2->start(18);
    ui->pushCalibrar->setEnabled(false);
}

void Taller_5::update_info2()
{
    IplImage *t = cvCloneImage( image );
    cvShowImage( "Calibration", image ); // Show raw image
    cvRemap( t, image, mapx, mapy ); // undistort image
    cvReleaseImage( &t );
    cvShowImage( "Undistort", image ); // Show corrected image
    cvWaitKey( 15 );
    image = cvQueryFrame( capture );
}

void Taller_5::on_push_Close_clicked()
{
    timer1->stop();
    cvReleaseCapture(&capture);
    cvDestroyAllWindows();
    if(timer2->isActive())
    {
    timer2->stop();
    cvReleaseMat ( &intrinsic_matrix);
    cvReleaseMat ( &distortion_coeffs);
    cvReleaseMat ( &object_points2);
    cvReleaseMat ( &image_points2);
    cvReleaseMat ( &point_counts2);
    cvReleaseMat ( &intrinsic);
    cvReleaseMat ( &distortion);
    cvReleaseImage( & mapx);
    cvReleaseImage( & mapy);
    }
    cvReleaseImage( &gray_image);
    successes=0;
    step=0;
    frame=0;
    ui->label_Success->setNum(successes);
    ui->pushContinue->setEnabled(false);
    ui->pushCalibrar->setEnabled(false);
    ui->push_Close->setEnabled(false);
    ui->push_Gaussian->setEnabled(true);
    Cal=false;
    Finish=false;
}
