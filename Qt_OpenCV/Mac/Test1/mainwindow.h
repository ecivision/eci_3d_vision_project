#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <cv.h>
#include <highgui.h>
#include <QDebug>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushStart_clicked();

    void on_pushCapture_clicked();

    void on_pushCalibrate_clicked();

    void on_pushReset_clicked();

    void update_info();

    void update_info2();

    void update_info3();

    void on_pushClose_clicked();

    void on_numHorCorners_textChanged(const QString &arg1);

    void on_numVertCorners_textChanged(const QString &arg1);

    void on_numBoards_textChanged(const QString &arg1);

    void on_numBoards_editingFinished();

private:
    Ui::MainWindow *ui;
        int board_w;
        int board_h;
        int numSquares;
        int numBoards;
        int count;
        QTimer *timer1;
        QTimer *timer2;
        QTimer *timer3;
    //new cpp interface
        cv::Size patternSize;
        cv::vector<cv::Point3f> obj;
        //imagen derecha
        cv::Mat src_right;
        cv::Mat dst_right;
        cv::Mat srcGray_right;
        cv::vector<cv::vector<cv::Point3f> > objPoints_right;
        cv::vector<cv::vector<cv::Point2f> > imgPoints_right;
        cv::vector<cv::Point2f> chessCorners_right;
        cv::VideoCapture capture_right;
        cv::Mat intrinsic_right;
        cv::Mat distCoeff_right;
        cv::vector<cv::Mat> rVecs_right;
        cv::vector<cv::Mat> tVecs_right;

        //imagen izquierda
        cv::Mat src_left;
        cv::Mat dst_left;
        cv::Mat srcGray_left;
        cv::vector<cv::vector<cv::Point3f> > objPoints_left;
        cv::vector<cv::vector<cv::Point2f> > imgPoints_left;
        cv::vector<cv::Point2f> chessCorners_left;
        cv::VideoCapture capture_left;
        cv::Mat intrinsic_left;
        cv::Mat distCoeff_left;
        cv::vector<cv::Mat> rVecs_left;
        cv::vector<cv::Mat> tVecs_left;

        cv::FileStorage calParamsFile_right;
        bool patternsfound_right;
        bool patternsfound_left;
        void test_save();
};

#endif // MAINWINDOW_H
