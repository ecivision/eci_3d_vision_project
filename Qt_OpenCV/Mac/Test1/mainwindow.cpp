#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QTimer>
#include <stdio.h>
#include <math.h>
#include <complex>
#include <time.h>

#define WINDOW_BORDER 30
//#define STEREO_CALIB

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QWidget::move(459,600);
    ui->pushCapture->setEnabled(false);
    ui->pushCalibrate->setEnabled(false);
    ui->pushReset->setEnabled(false);
    ui->pushStart->setEnabled(false);
    timer1 = new QTimer(this);
    timer2 = new QTimer(this);
    timer3 = new QTimer(this);
    patternsfound_right=0;
    patternsfound_left=0;
//    successes=0;
//    step=0;
//    frame=0;
    intrinsic_right = cv::Mat(3,3,CV_32FC1);
    intrinsic_left = cv::Mat(3,3,CV_32FC1);
    count=0;
    //board_dt=20;
    board_w = 0; // Board width in squares
    board_h = 0; // Board height
    numSquares = board_w * board_h;
    connect(timer1,SIGNAL(timeout()),this,SLOT(update_info()));
    connect(timer2,SIGNAL(timeout()),this,SLOT(update_info2()));
    connect(timer3,SIGNAL(timeout()),this,SLOT(update_info3()));
    cv::VideoCapture tempcam;
    bool errorNoCam=false;
    int i = 0;
    cv::Mat tempImage;
//    while(!errorNoCam){
//        try{
//        tempcam.open(i);
//        tempcam >> tempImage;
//        tempcam.release();
//        }
//        catch(cv::Exception e){
//            qDebug()<<"Error camera " + QString::number(i) + "Does not exist";
//            errorNoCam=true;
//        }
//        if(!errorNoCam){
//            i++;
//            qDebug()<<"Camera " + QString::number(i) + " exists";
//        }
//        else
//            i=0;
//    }
    test_save();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushStart_clicked()
{
    ui->pushReset->setEnabled(true);
        ui->numHorCorners->setEnabled(false);
        ui->numVertCorners->setEnabled(false);
        patternSize = cv::Size(board_w,board_h);
        //camara derecha
        cv::namedWindow("Imagen Derecha");


        capture_right.open(3);
        capture_right.set(CV_CAP_PROP_FRAME_WIDTH,640);
        capture_right.set(CV_CAP_PROP_FRAME_HEIGHT,320);
        capture_right>>src_right;
        cv::moveWindow("Imagen Derecha",src_right.cols,0);
        //cv::flip(src_right,src_right,1);
        cv::imshow("Imagen Derecha",src_right);

#ifdef STEREO_CALIB
        //camara izquierda
        cv::namedWindow("Imagen Izquierda");
        cv::moveWindow("Imagen Izquierda",0,0);

        capture_left.open(1);
        capture_left.set(CV_CAP_PROP_FRAME_WIDTH,640);
        capture_left.set(CV_CAP_PROP_FRAME_HEIGHT,320);
        capture_left >> src_left;
        //cv::flip(src_right,src_right,1);
        cv::imshow("Imagen Izquierda",src_left);
#endif
        timer1->start(15);
        ui->pushStart->setEnabled(false);
        ui->numBoards->setEnabled(false);
}

void MainWindow::on_pushCapture_clicked()
{
    timer3->stop();
    timer1->stop();
    count++;
#ifdef STEREO_CALIB
    imgPoints_left.push_back(chessCorners_left);
    objPoints_left.push_back(obj);
#endif
    if(count>numBoards){
        ui->pushCalibrate->setEnabled(true);
        ui->pushCapture->setEnabled(false);
        ui->pushStart->setEnabled(false);
    }
    else
    {
        ui->infoLabel->setText("Patron "+QString::number(count)+" capturado.");
        ui->label_captured->setNum(count);
        imgPoints_right.push_back(chessCorners_right);
        objPoints_right.push_back(obj);
        bitwise_not(src_right, src_right);
        cv::imshow("Imagen Derecha",src_right);
        qDebug()<< "Patron Capturado";
        timer1->start(15);
    }


}

void MainWindow::update_info3(){
MainWindow::on_pushCapture_clicked();
}

void MainWindow::on_pushCalibrate_clicked()
{
    calParamsFile_right = cv::FileStorage("/Volumes/Datos/felipe/Desktop/FelipeCalibrationTest.yml",cv::FileStorage::WRITE);
    intrinsic_right.ptr<float> (0)[0] = 1;
    intrinsic_right.ptr<float> (1)[1] = 1;
    double rms=0;
    rms = cv::calibrateCamera(objPoints_right,
                        imgPoints_right,
                        //cv::Size(src_right.cols/3,
                          //       src_right.rows/3),
                        src_right.size(),
                        intrinsic_right,
                        distCoeff_right,
                        rVecs_right,
                        tVecs_right);
    ui->infoLabel->setText("Error de calibracion "+ QString::number(rms));
#ifdef STEREO_CALIB
    intrinsic_left.ptr<float> (0)[0] = -34;
    intrinsic_left.ptr<float> (1)[1] = -34;

    cv::calibrateCamera(objPoints_left,
                        imgPoints_left,
                        cv::Size(src_left.cols/3,
                                 src_right.rows/3),
                        intrinsic_left,
                        distCoeff_left,
                        rVecs_left,
                        tVecs_left);
#endif
    calParamsFile_right << "intrinsicMatrix"<< intrinsic_right;
    calParamsFile_right << "distCoeffsMatrix"<< distCoeff_right;
    calParamsFile_right << "rotationVectors"<<rVecs_right;
    calParamsFile_right << "translationVectors" <<tVecs_right;
    calParamsFile_right.release();
    cv::namedWindow("Undistorted Right");
//    CvMat prueba = intrinsic_right;
//    cvSave("/Volumes/Datos/felipe/Desktop/hola.xml", &prueba);
    cv::moveWindow("Undistorted Right",
                   src_right.cols,
                   src_right.rows+WINDOW_BORDER);

#ifdef STEREO_CALIB
    cv::namedWindow("Undistorted Left");
    cv::moveWindow("Undistorted Left",
                   0,
                   capture_left.get(CV_CAP_PROP_FRAME_HEIGHT)+WINDOW_BORDER);
#endif
    qDebug()<<"So far so good";
    ui->pushCalibrate->setEnabled(false);
    timer2->start(15);
}

void MainWindow::on_pushReset_clicked()
{
    timer1->stop();
    timer2->stop();
    timer3->stop();
    ui->numHorCorners->setEnabled(true);
    ui->numVertCorners->setEnabled(true);
    ui->pushStart->setEnabled(false);
    ui->pushCalibrate->setEnabled(false);
    ui->pushCapture->setEnabled(false);
    ui->numBoards->setEnabled(false);
    if(capture_right.isOpened())
    capture_right.release();
    if(capture_left.isOpened())
    capture_left.release();
    try{
    cv::destroyAllWindows();
    }
    catch(cv::Exception e)
    {
        qDebug()<<"No highui windows opened";
    }
    src_right.release();
    srcGray_right.release();
    objPoints_right.clear();
    imgPoints_right.clear();
    intrinsic_right.release();
    distCoeff_right.release();
    rVecs_right.clear();
    tVecs_right.clear();
    ui->infoLabel->setText("");
    ui->numHorCorners->setText("");
    ui->numVertCorners->setText("");
    ui->label_captured->setText("");
    ui->numBoards->setText("");
    numBoards=0;
    board_h=0;
    board_w=0;
    count=0;
    obj.clear();

#ifdef STEREO_CALIB
    src_left.release();
    srcGray_left.release();
    objPoints_left.clear();
    imgPoints_left.clear();
    intrinsic_left.release();
    distCoeff_left.release();
    rVecs_left.clear();
    tVecs_left.clear();
#endif
}

void MainWindow::update_info()
{
    ui->infoLabel->setText("Capturando el patron " + QString::number(count));
    capture_right >> src_right;
#ifdef STEREO_CALIB
    capture_left >> src_left;
#endif
    cv::cvtColor(src_right,srcGray_right,CV_BGR2GRAY);

#ifdef STEREO_CALIB
    cv::cvtColor(src_left,srcGray_left,CV_BGR2GRAY);
#endif
    // Find chessboard corners:
    patternsfound_right = cv::findChessboardCorners(src_right,
                                                    patternSize,
                                                    chessCorners_right,
                                                    //CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
                                                    CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK |
                                                    CV_CALIB_CB_NORMALIZE_IMAGE);
#ifdef STEREO_CALIB
    patternsfound_left = cv::findChessboardCorners(srcGray_left,
                                                   patternSize,
                                                   chessCorners_left,
                                                   CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
#endif
    //patternsfound_right = cv::findCirclesGrid(srcGray_right,patternSize,chessCorners_right,cv::CALIB_CB_ASYMMETRIC_GRID);
    if(patternsfound_right
        #ifdef STEREO_CALIB
            & patternsfound_left
        #endif
            )
    {
        qDebug()<<"Chessboard found!";
        cornerSubPix(srcGray_right,
                     chessCorners_right,
                     cv::Size(11, 11), cv::Size(-1, -1),
                     cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
#ifdef STEREO_CALIB
        cornerSubPix(srcGray_left,
                     chessCorners_left,
                     cv::Size(11, 11),
                     cv::Size(-1, -1),
                     cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 40, 0.01));

        drawChessboardCorners(src_left, patternSize, chessCorners_left, patternsfound_left);
#endif
        drawChessboardCorners(src_right, patternSize, chessCorners_right, patternsfound_right);
        ui->pushCapture->setEnabled(true);
        if(!timer3->isActive())
        timer3->start(1500);
    }
    else{
        ui->pushCapture->setEnabled(false);
        timer3->stop();
    }
    //cv::flip(src_right,src_right,1);
    cv::imshow("Imagen Derecha",src_right);
    #ifdef STEREO_CALIB
   // cv::flip(src_right,src_right,1);
    cv::imshow("Imagen Izquierda",src_left);
#endif
}

void MainWindow::update_info2()
{
    capture_right >> src_right;

    cv::undistort(src_right,dst_right,intrinsic_right,distCoeff_right);
   // cv::flip(src_right,src_right,1);
    cv::imshow("Imagen Derecha",src_right);
    //cv::flip(dst_right,dst_right,1);
    cv::cvtColor(dst_right,src_left,CV_BGR2GRAY);
    cv::imshow("Undistorted Right",dst_right);

#ifdef STEREO_CALIB
    capture_left >> src_left;
    cv::undistort(src_left,dst_left,intrinsic_left,distCoeff_left);
    cv::imshow("Imagen Izquierda",src_left);
    cv::imshow("Undistorted Left",dst_left);
#endif
}

void MainWindow::on_pushClose_clicked()
{
 timer1->stop();
 timer2->stop();
 timer3->stop();
 ui->numHorCorners->setEnabled(true);
 ui->numVertCorners->setEnabled(true);
 if(capture_right.isOpened())
 capture_right.release();
 if(capture_left.isOpened())
 capture_left.release();
 try{
 cv::destroyAllWindows();
 }
 catch(cv::Exception e)
 {
     qDebug()<<"No highui windows opened";
 }
 QApplication::exit();
}

void MainWindow::on_numHorCorners_textChanged(const QString &numHorString)
{
    bool toIntOK=false;
    int numHor = numHorString.toInt(&toIntOK);
    if(numHorString!= NULL && toIntOK)
    {
        if(numHor<0)
            {
            ui->infoLabel->setText("Ingrese un entero positivo");
            board_w=0;
            ui->numHorCorners->setText("");
        }
        else{
            ui->infoLabel->setText("Numero de esquinas horizontales correcto");
            board_w = numHor;
        }
    }
    else{
        ui->infoLabel->setText("Ingrese un numero de esquinas horizontales valido");
        board_w=0;
        ui->numHorCorners->setText("");
    }

    if(board_h>0 && board_w>0){
        ui->pushStart->setEnabled(true);
        ui->numHorCorners->setEnabled(false);
        ui->numVertCorners->setEnabled(false);
        ui->pushReset->setEnabled(true);
        ui->numBoards->setEnabled(true);
        numSquares = board_w * board_h;
        for(int j=0;j<numSquares;j++){
            obj.push_back(cv::Point3f(j/board_w, j%board_w, 0.0f));
        }
        qDebug() << QString::number(obj.size());
    }
    else{
        ui->pushStart->setEnabled(false);
    }
}

void MainWindow::on_numVertCorners_textChanged(const QString &numVertString)
{
    bool toIntOK=false;
    int numVert = numVertString.toInt(&toIntOK);
    if(numVertString!= NULL && toIntOK)
    {
        if(numVert<0)
            {
            ui->infoLabel->setText("Ingrese un entero positivo");
            board_h=0;
            ui->numVertCorners->setText("");
        }
        else{
            ui->infoLabel->setText("Numero de esquinas verticales correcto");
            board_h = numVert;
        }
    }
    else{
        ui->infoLabel->setText("Ingrese un numero de esquinas horizontales valido");
        board_w=0;
        ui->numVertCorners->setText("");
    }

    if(board_h>0 && board_w>0){
        ui->numHorCorners->setEnabled(false);
        ui->numVertCorners->setEnabled(false);
        ui->pushReset->setEnabled(true);
        ui->numBoards->setEnabled(true);
        numSquares = board_w * board_h;
        for(int j=0;j<numSquares;j++){
               obj.push_back(cv::Point3f(j/board_w, j%board_w, 0.0f));
        }
    }
    else{
        ui->pushStart->setEnabled(false);
    }
}

void MainWindow::on_numBoards_textChanged(const QString &numBoardsString)
{

}

void MainWindow::on_numBoards_editingFinished()
{
    bool toIntOK=false;
    int nboards = ui->numBoards->text().toInt(&toIntOK);
    if(toIntOK && nboards>0)
    {
        numBoards=nboards;
        ui->pushStart->setEnabled(true);
        ui->numBoards->setEnabled(false);
    }
    else
    {
        numBoards=0;
        ui->infoLabel->setText("Elija un numero positivo de patrones a capturar.");
    }
}

void MainWindow::test_save(){


}
