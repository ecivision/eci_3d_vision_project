#-------------------------------------------------
#
# Project created by QtCreator 2013-03-15T13:52:46
#
#-------------------------------------------------

QT       += core gui

TARGET = Test1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH +='/opt/local/include/opencv'
INCLUDEPATH +='/opt/local/include/opencv2'
INCLUDEPATH +='/opt/include'
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann
