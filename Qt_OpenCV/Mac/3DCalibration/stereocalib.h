#ifndef STEREOCALIB_H
#define STEREOCALIB_H

#include <QMainWindow>

namespace Ui {
class StereoCalib;
}

class StereoCalib : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit StereoCalib(QWidget *parent = 0);
    ~StereoCalib();
    
private:
    Ui::StereoCalib *ui;
};

#endif // STEREOCALIB_H
