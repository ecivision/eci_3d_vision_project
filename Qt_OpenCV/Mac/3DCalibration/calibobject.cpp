#include "calibobject.h"
#include <opencv2/opencv.hpp>

CalibObject::CalibObject()
{
    public:
    cv::Size patternSize;
    cv::Mat image;
    cv::Mat gray_image;
    cv::vector<cv::vector<cv::Point3f> > objectPoints;
    cv::vector<cv::vector<cv::Point2f> > imagePoints;
    cv::vector<cv::Point2f> corners;
    cv::VideoCapture camera;
    cv::Mat cameraMatrix;
    cv::Mat distCoeff;
    double rmsError;
    double reprojectionError;
    cv::vector<cv::Mat> rotationVecs;
    cv::vector<cv::Mat> translationVecs;

    private:

}
