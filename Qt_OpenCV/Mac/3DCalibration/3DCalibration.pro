#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T14:11:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 3DCalibration
TEMPLATE = app


SOURCES += main.cpp\
        stereocalib.cpp \
    calibobject.cpp

HEADERS  += stereocalib.h \
    calibobject.h

FORMS    += stereocalib.ui
