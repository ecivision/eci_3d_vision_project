// Do not remove the include below
#include "Serial_Client_CDC.h"

unsigned char i[] = { 0, 0 };
//The setup function is called once at startup of the sketch

void SerialEvent() {
	while (Serial.available()) {
		*i =  Serial.read();
	}
}

void setup() {
	Serial.begin(57600);
	pinMode(LED_BUILTIN, OUTPUT);
}

// The loop function is called in an endless loop
void loop() {
	if ((i[0] == i[1]) && (i[0] > 0)) {
		digitalWrite(LED_BUILTIN, HIGH);
		delay(500);
	}
	digitalWrite(LED_BUILTIN, LOW);
}
