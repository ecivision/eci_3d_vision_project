// Do not remove the include below
#include "Microbridge_Shell.h"

#include <SPI.h>
#include <Adb.h>

Connection * shell;

// Event handler for the shell connection.
void adbEventHandler(Connection * connection, adb_eventType event,
		uint16_t length, uint8_t * data) {
	int i;

	switch (event) {
	case ADB_CONNECT:
		Serial.println("ADB_CONNECT");
		break;
	case ADB_DISCONNECT:
		Serial.println("ADB_DISCONNECT");
		break;
	case ADB_CONNECTION_OPEN:
		Serial.println("ADB_CONNECTION_OPEN");
		break;
	case ADB_CONNECTION_CLOSE:
		Serial.println("ADB_CONNECTION_CLOSE");
		break;
	case ADB_CONNECTION_FAILED:
		Serial.println("ADB_CONNECTION_FAILED");
		break;
	case ADB_CONNECTION_RECEIVE:
		Serial.println("ADB_CONNECTION_RECEIVE");
		digitalWrite(LED_BUILTIN, HIGH);
		for (i = 0; i < length; i++) {
			Serial.print(data[i]);
		}
		digitalWrite(LED_BUILTIN, LOW);
		break;
	}

}

void setup() {

// Initialise serial port
	Serial.begin(57600);
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);
// Initialise the ADB subsystem.
	ADB::init();
// Open an ADB stream to the phone's shell. Auto-reconnect
	shell = ADB::addConnection("shell:ls", false, adbEventHandler);
}

void loop() {
	ADB::closeAll();
	shell = ADB::addConnection("shell:ls", false, adbEventHandler);
	Serial.println(shell->remoteID);
	ADB::poll();
}
