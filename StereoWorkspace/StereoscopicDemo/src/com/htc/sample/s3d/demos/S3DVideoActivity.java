/*
 * Copyright (C) 2011 HTC Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.htc.sample.s3d.demos;

import com.htc.view.DisplaySetting;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

// S3D video example - touch to pause/play video with example text overlay
public class S3DVideoActivity extends Activity implements
        OnPreparedListener, OnVideoSizeChangedListener, SurfaceHolder.Callback {

    private static final String TAG = "S3DVideoActivity";
    private int width;
    private int height;
    private MediaPlayer mediaPlayer;
    private SurfaceView preview;
    private SurfaceHolder holder;
    private String fileName;
    private boolean isInitialized = false;
    private boolean isPrepared = false;
    private TextView text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);
        preview = (SurfaceView) findViewById(R.id.surface);
        text = (TextView) findViewById(R.id.text);
        holder = preview.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void playVideo() {
        release();
        fileName = "HTCDemo.mp4";
        try {
            mediaPlayer = new MediaPlayer();
            final AssetFileDescriptor afd = getAssets().openFd(fileName);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            mediaPlayer.setDisplay(holder);
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    public void onVideoSizeChanged(MediaPlayer mp, int aWidth, int aHeight) {
        if (aWidth == 0 || aHeight == 0) {
            return;
        }
        Log.i(TAG, "onVideoSizeChanged w=" + aWidth + " h=" + aHeight);
        isInitialized = true;
        width = aWidth;
        height = aHeight;
        if (isPrepared && isInitialized) {
            startVideoPlayback();
        }
    }

    public void onPrepared(MediaPlayer mediaplayer) {
        isPrepared = true;
        if (isPrepared && isInitialized) {
            startVideoPlayback();
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {
        holder = surfaceholder;
        enableS3D(true, holder.getSurface()); // note SEI FPA flag in content
                                              // overrides this
    }

    public void surfaceDestroyed(SurfaceHolder surfaceholder) {
        holder = surfaceholder;
        enableS3D(false, holder.getSurface());
    }

    public void surfaceCreated(SurfaceHolder holder) {
        playVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        release();
    }

    private void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        width = 0;
        height = 0;
        isPrepared = false;
        isInitialized = false;
        enableS3D(false, holder.getSurface());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                toggle();
                break;
            default:
                break;
        }
        return true;
    }

    private void toggle() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            text.setText("- paused -");
            text.setVisibility(View.VISIBLE);
        } else {
            mediaPlayer.start();
            text.setVisibility(View.INVISIBLE);
        }
    }

    private void startVideoPlayback() {
        holder.setFixedSize(width, height);
        mediaPlayer.start();
    }

    private void enableS3D(boolean enable, Surface surface) {
        text.setVisibility(View.INVISIBLE);
        Log.i(TAG, "enableS3D(" + enable + ")");
        int mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE;
        if (!enable) {
            mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF;
        }
        boolean formatResult = true;
        try {
            formatResult = DisplaySetting.setStereoscopic3DFormat(surface, mode);
        } catch (NoClassDefFoundError e) {
            text.setVisibility(View.VISIBLE);
            android.util.Log.i(TAG, "class not found - S3D display not available");
        }
        Log.i(TAG, "return value:" + formatResult);
        if (!formatResult) {
            android.util.Log.i(TAG, "S3D format not supported");
        }
    }
}
