/*
 * Copyright (C) 2011 HTC Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.htc.sample.s3d.demos;

import com.htc.view.DisplaySetting;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

// example S3D image drawn on canvas (touch to toggle S3D/2D)
// also illustrates red/cyan anaglyph fallback for non S3D devices/emulators
public class S3DImageActivity extends Activity {

    private Surface surface;

    class MySurfaceview extends SurfaceView implements SurfaceHolder.Callback {
        private Canvas canvas;
        private Bitmap bitmap;
        private SurfaceHolder holder;
        private final String TAG = MySurfaceview.class.getSimpleName();
        boolean s3DAvailable = true;
        private boolean display2D;
        private Bitmap bitmap2D;
        int width, height;

        public MySurfaceview(Context context) {
            super(context);

            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test);
            bitmap2D = BitmapUtils.create2DBitmap(bitmap);
            holder = this.getHolder();
            holder.setFormat(PixelFormat.RGBA_8888);
            holder.addCallback(this);
            surface = holder.getSurface();
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            boolean formatResult = true;
            this.width = width;
            this.height = height;

            try {
                formatResult = DisplaySetting.setStereoscopic3DFormat(surface,
                        DisplaySetting.STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE);
            } catch (NoClassDefFoundError e) {
                android.util.Log.i(TAG, "class not found - S3D display not available");
                s3DAvailable = false;
            } catch (UnsatisfiedLinkError e) {
                android.util.Log.i(TAG, "unsatisfied link error - S3D display not available");
                s3DAvailable = false;
            }

            android.util.Log.i(TAG, "return value:" + formatResult);

            if (!formatResult) {
                android.util.Log.i(TAG, "S3D format not supported");
                return;
            }

            canvas = holder.lockCanvas();
            if (!s3DAvailable) {
                // if DisplaySetting not found, create anaglyph version of 3D
                // image
                bitmap = BitmapUtils.createAnaglyphBitmap(bitmap);
            }
            canvas.drawBitmap(bitmap, null, new Rect(0, 0, width, height), null);
            holder.unlockCanvasAndPost(canvas);
        }

        public void surfaceCreated(SurfaceHolder holder) {
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            try {
                DisplaySetting.setStereoscopic3DFormat(surface,
                        DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF);
            } catch (NoClassDefFoundError e) {
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    display2D = !display2D;
                    canvas = holder.lockCanvas();
                    if (display2D) {
                        try {
                            DisplaySetting.setStereoscopic3DFormat(surface,
                                    DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF);
                        } catch (NoClassDefFoundError e) {
                        }
                        canvas.drawBitmap(bitmap2D, null, new Rect(0, 0, width, height), null);
                    } else {
                        try {
                            DisplaySetting.setStereoscopic3DFormat(surface,
                                    DisplaySetting.STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE);
                        } catch (NoClassDefFoundError e) {
                        }
                        canvas.drawBitmap(bitmap, null, new Rect(0, 0, width, height), null);
                    }
                    holder.unlockCanvasAndPost(canvas);
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new MySurfaceview(this));
    }

    protected void onDestroy() {
        super.onDestroy();
        if (surface != null) {
            try {
                DisplaySetting.setStereoscopic3DFormat(surface,
                        DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF);
            } catch (NoClassDefFoundError e) {
            }
        }
    }
}
