/*
 * Copyright (C) 2011 HTC Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.htc.sample.s3d.demos;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.htc.view.DisplaySetting;

//LG 3D supports rgb_565 (4) format, NV21 (17) and YCbCr - YUY2 YUYV (20)  

// S3D example of live camera preview (touch screen to toggle mode) and overlay text indicating 3D or 2D mode
public class S3DCameraActivity extends Activity implements SurfaceHolder.Callback {

	private final static String TAG = "S3DCameraActivity";
	/*
	 * CAMERA_STEREOSCOPIC Used to open S3D camera on devices running Android older than
	 * ICS, the id value is 2. For Android ICS devices, the id value is 100
	 * See how to check the device's Android version below in getS3DCamera()
	 */
	private final static int CAMERA_STEREOSCOPIC = 2;
	private final static int CAMERA_STEREOSCOPIC_ICS = 100;
	private boolean is3Denabled = true;
	private SurfaceHolder holder;
	private SurfaceView preview;
	private Camera camera;
	private TextView text;
	private int width, height;
	private Parameters parameters;
	private int focusModeNumber=0;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video);
		preview = (SurfaceView) findViewById(R.id.surface);
		text = (TextView) findViewById(R.id.text);
		holder = preview.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		camera = getS3DCamera();
		if (is3Denabled) {
			text.setText("S3D");
		} else {
			camera = get2DCamera();
			text.setText("2D");
		}
		text.setVisibility(View.VISIBLE);
	}

	public Camera get2DCamera() {
		Camera camera = null;
		try {
			camera = Camera.open();
			camera.setPreviewDisplay(holder);
		} catch (IOException ioe) {
			if (camera != null) {
				camera.release();
			}
			camera = null;
		} catch (RuntimeException rte) {
			if (camera != null) {
				camera.release();
			}
			camera = null;
		}
		return camera;
	}

	public Camera getS3DCamera() {
		Camera camera = null;
		int cameraID = Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH ?
				CAMERA_STEREOSCOPIC : CAMERA_STEREOSCOPIC_ICS;
		try {
			camera = Camera.open(cameraID);
			camera.setPreviewDisplay(holder);
			is3Denabled = true;
		} catch (IOException ioe) {
			if (camera != null) {
				camera.release();
			}
			camera = null;
		} catch (NoSuchMethodError nsme) {
			is3Denabled = false;
			text.setVisibility(View.VISIBLE);
			Log.w(TAG, Log.getStackTraceString(nsme));
		} catch (UnsatisfiedLinkError usle) {
			is3Denabled = false;
			text.setVisibility(View.VISIBLE);
			Log.w(TAG, Log.getStackTraceString(usle));
		} catch (RuntimeException re) {
			is3Denabled = false;
			text.setVisibility(View.VISIBLE);
			Log.w(TAG, Log.getStackTraceString(re));
			if (camera != null) {
				camera.release();
			}
			camera = null;
		}
		return camera;
	}

	public void surfaceDestroyed(SurfaceHolder surfaceholder) {
		stopPreview();
		holder = surfaceholder;
		enableS3D(false, surfaceholder.getSurface()); // to make sure it's off
	}

	private void stopPreview() {
		if (camera != null) {
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.05;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Size size : sizes) {
			Log.i(TAG, String.format("width=%d height=%d", size.width, size.height));
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	public void surfaceChanged(SurfaceHolder surfaceholder, int format, int w, int h) {
		holder = surfaceholder;
		width = w;
		height = h;
		startPreview(width, height);
	}

	private void startPreview(int w, int h) {
		if (camera != null) {
			parameters = camera.getParameters();
			List<Size> sizes = parameters.getSupportedPreviewSizes();
			Size optimalSize = getOptimalPreviewSize(sizes, w, h);
			parameters.setPreviewSize(optimalSize.width, optimalSize.height);
			Log.i(TAG, "optimalSize.width=" + optimalSize.width
					+ " optimalSize.height=" + optimalSize.height);
			camera.setParameters(parameters);
			camera.startPreview();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
//			toggle();
			List<Integer> focusModes = parameters.getSupportedPreviewFormats();
			++focusModeNumber;
//			if(focusModeNumber>(focusModes.size()-1)){
//			focusModeNumber=0;
//			}
			for(int i=0;i<focusModes.size();i++){
//			parameters.setPreviewFormat(focusModes.get(focusModeNumber));
			}
//			camera.setParameters(parameters);
			break;
		default:
			break;
		}
		return true;
	}

	public void toggle() {
		is3Denabled = !is3Denabled;
		stopPreview();
		if (is3Denabled) {
			camera = getS3DCamera();
		}
		if (!is3Denabled) {
			camera = get2DCamera();
		}
		startPreview(width, height);
		if (is3Denabled) {
			text.setText("S3D");
		} else {
			text.setText("2D");
		}
	}

	private void enableS3D(boolean enable, Surface surface) {
		Log.e(TAG, "enableS3D(" + enable + ")");
		int mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE;
		if (!enable) {
			mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF;
		} else {
			is3Denabled = true;
		}
		boolean formatResult = true;
		try {
			formatResult = DisplaySetting
					.setStereoscopic3DFormat(surface, mode);
		} catch (NoClassDefFoundError e) {
			android.util.Log.i(TAG,
					"class not found - S3D display not available");
			is3Denabled = false;
		}
		Log.i(TAG, "return value:" + formatResult);
		if (!formatResult) {
			android.util.Log.i(TAG, "S3D format not supported");
			is3Denabled = false;
		}
	}

}
