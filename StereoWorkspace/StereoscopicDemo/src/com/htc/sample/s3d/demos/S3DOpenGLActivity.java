/*
 * Copyright (C) 2011 HTC Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.htc.sample.s3d.demos;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

// S3D OpenGL demo of a spinning box
// touch screen to rotate box, use button to toggle S3D mode, slider to zoom in or out
// also illustrates red/cyan anaglyph fallback for non S3D devices/emulators
public class S3DOpenGLActivity extends Activity {

    private S3DGLSurfaceView glSurfaceView;
    final private static String TAG = "S3DOpenGLActivity";
    private Button s3dButton;
    private SeekBar myZoomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        glSurfaceView = (S3DGLSurfaceView) findViewById(R.id.glview);
        s3dButton = (Button) findViewById(R.id.button1);
        s3dButton.setText("S3D\nis  ON");
        myZoomBar = (SeekBar) findViewById(R.id.zoomBar);
        myZoomBar.setProgress(3);
        myZoomBar.setOnSeekBarChangeListener(myZoomBarOnSeekBarChangeListener);
        setZoomLevel();
    }

    public void onClickS3DButton(View view) {
        glSurfaceView.toggle();
        if (glSurfaceView.is3Denabled) {
            s3dButton.setText("S3D\nis  ON");
        } else {
            s3dButton.setText("S3D\nis OFF");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (glSurfaceView != null) {
            glSurfaceView.onResume();
        } else {
            Log.d(TAG, "onResume with null glSurfaceView");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (glSurfaceView != null) {
            glSurfaceView.onPause();
        } else {
            Log.d(TAG, "onPause with null glSurfaceView");
        }
    }

    private void setZoomLevel() {
        int myZoomLevel = myZoomBar.getProgress();
        glSurfaceView.slide(myZoomLevel);
    }

    private SeekBar.OnSeekBarChangeListener myZoomBarOnSeekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {

                public void onProgressChanged(SeekBar seekBar, int progress,
                        boolean fromUser) {
                    setZoomLevel();
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            };

}
