/*
 * Copyright (C) 2011 HTC Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.htc.sample.s3d.demos;

import com.htc.view.DisplaySetting;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

//surface view used in the S3DOpenGLExample
public class S3DGLSurfaceView extends GLSurfaceView implements Renderer {

    public boolean is3Denabled = true;
    public boolean is3Dsupported = true;

    private static final String TAG = "S3DGLSurfaceView";
    private static final int ZOOM = 1;
    private static final int DRAG = 2;

    private int width;
    private int height;

    private Box box;
    private float xrot;
    private float inityrot = 0.2f;
    private float yrot = inityrot;
    private float xspeed;
    private float inityspeed = 0.8f;
    private float yspeed = inityspeed;

    private float z = 0.0f;

    private float oldX;
    private float oldY;
    private final float scaleFactor = 0.4f;
    private Context context;
    private SurfaceHolder holder;
    private float oldDist;
    private int mode;

    public S3DGLSurfaceView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public S3DGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_GPU);
        this.setRenderer(this);
        this.requestFocus();
        this.setFocusableInTouchMode(true);
        box = new Box();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        box.loadGLTexture(gl, context);

        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
        gl.glClearDepthf(1.0f);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        Log.d(TAG, "w=" + w + " h=" + h);
    }

    public void draw(GL10 gl) {
        gl.glTranslatef(z, 0.0f, 0.0f);
        gl.glScalef(0.8f, 0.8f, 0.8f);

        gl.glRotatef(xrot, 0.0f, 0.0f, 1.0f);
        gl.glRotatef(yrot, 0.0f, 1.0f, 0.0f);

        box.draw(gl);

        xrot += xspeed;
        yrot += yspeed;
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        if (h == 0) {
            h = 1;
        }

        Log.d(TAG, "h=" + h + " h=" + h);

        width = w;
        height = h;

        initCameraProjection(width, height);

        enableS3D(is3Denabled);

        float ratio = (float) width / (float) height;
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glViewport(0, 0, width, height);

        gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
    }

    public void onDrawFrame(GL10 gl) {
        float left, right, top, bottom;

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glColorMask(true, true, true, true);

        if (is3Denabled) {
            // LEFT
            if (is3Dsupported) {
                gl.glViewport(0, 0, (int) width / 2, (int) height);
            } else {
                gl.glColorMask(true, false, false, true);
            }
        } else {
            gl.glViewport(0, 0, (int) width, (int) height);
        }
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        left = (float) (-ratio * wd2 + 0.5 * camera.eyeSeparation * ndfl);
        right = (float) (ratio * wd2 + 0.5 * camera.eyeSeparation * ndfl);
        top = wd2;
        bottom = -wd2;
        gl.glFrustumf(left, right, bottom, top, near, far);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        GLU.gluLookAt(gl, camera.viewPos[0] - r[0], camera.viewPos[1] - r[1], camera.viewPos[2]
                - r[2],
                camera.viewPos[0] - r[0] + camera.viewDirection[0],
                camera.viewPos[1] - r[1] + camera.viewDirection[1],
                camera.viewPos[2] - r[2] + camera.viewDirection[2],
                camera.viewUp[0], camera.viewUp[1], camera.viewUp[2]);
        draw(gl);

        if (is3Denabled) {
            // RIGHT
            if (is3Dsupported) {
                gl.glViewport((int) width / 2, 0, (int) width / 2, (int) height);
            } else {
                gl.glClear(GL10.GL_DEPTH_BUFFER_BIT);
                gl.glColorMask(true, true, true, true);
                gl.glColorMask(false, true, true, true);
                // gl.glEnable(GL10.GL_BLEND);
                // gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE);
            }

            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();
            left = (float) (-ratio * wd2 - 0.5 * camera.eyeSeparation * ndfl);
            right = (float) (ratio * wd2 - 0.5 * camera.eyeSeparation * ndfl);
            top = wd2;
            bottom = -wd2;
            gl.glFrustumf(left, right, bottom, top, near, far);

            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadIdentity();
            GLU.gluLookAt(gl, camera.viewPos[0] + r[0], camera.viewPos[1] + r[1], camera.viewPos[2]
                    + r[2],
                    camera.viewPos[0] + r[0] + camera.viewDirection[0],
                    camera.viewPos[1] + r[1] + camera.viewDirection[1],
                    camera.viewPos[2] + r[2] + camera.viewDirection[2],
                    camera.viewUp[0], camera.viewUp[1], camera.viewUp[2]);
            draw(gl);
        }
    }

    @Override
    public void onPause() {
        is3Denabled = false;
        enableS3D(is3Denabled);
    }

    private float pinch(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    protected void slide(int zoom) {
        Log.d(TAG, "ZOOM to " + zoom);

        float newZoom = zoom;

        float zfactor = (float) (newZoom * scaleFactor);

        if (zoom > 3)
        {
            // z -= zfactor;
            z = (float) (-zfactor + (3 * scaleFactor));
        }
        else if (zoom < 3)
        {
            // z += zfactor;
            z = (float) ((3 * scaleFactor) - zfactor);
        }
        else
            z = 0;

        Log.d(TAG, "zfactor = " + zfactor + ", z = " + z);

        // limit zoom
        Log.d(TAG, "z=" + z);
        if (z > 2.6f) {
            Log.d(TAG, "z is above threshold");
            z = 2.6f;
        } else if (z < -2.6f) {
            Log.d(TAG, "z is below threshold");
            z = -2.6f;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                mode = DRAG;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                mode = DRAG;
                oldDist = pinch(event);
                Log.d(TAG, "oldDist=" + oldDist);
                if (oldDist > 10f) {
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM");
                }
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = x - oldX;
                float dy = y - oldY;
                if (mode == ZOOM) {
                    float newDist = pinch(event);
                    if (newDist > 10f) {
                        float zfactor = dx * scaleFactor / 2;
                        z -= zfactor;
                        // limit zoom
                        Log.d(TAG, "z=" + z);
                        if (z > 2.6f) {
                            z = 2.6f;
                        } else if (z < -2.6f) {
                            z = -2.6f;
                        }
                    }
                } else {
                    xrot += dy * scaleFactor;
                    yrot += dx * scaleFactor;
                }
                break;
        }
        oldX = x;
        oldY = y;
        return true;
    }

    private void enableS3D(boolean enable) {

        int mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE;
        if (!enable) {
            mode = DisplaySetting.STEREOSCOPIC_3D_FORMAT_OFF;
        }

        boolean formatResult = true;
        Surface surface = holder.getSurface();
        try {
            formatResult = DisplaySetting.setStereoscopic3DFormat(surface, mode);
        } catch (NoClassDefFoundError e) {
            android.util.Log.i(TAG, "class not found - S3D display not available");
            is3Dsupported = false;
        } catch (UnsatisfiedLinkError usle) {
            android.util.Log.i(TAG, "unsatisfied link - S3D display not available");
            is3Dsupported = false;
        }

        android.util.Log.i(TAG, "return value:" + formatResult);
        if (!formatResult) {
            android.util.Log.i(TAG, "S3D format not supported");
        }
    }

    public void toggle() {
        is3Denabled = !is3Denabled;
        if (!is3Denabled) {
            if (is3Dsupported) {
                // compensate for difference (ideally time based animation)
                yspeed = (float) (inityspeed / 1.79);
            } else {
                yspeed = inityspeed;
            }
        } else {
            yspeed = inityspeed;
        }
        enableS3D(is3Denabled);
    }

    class Camera {
        float[] rotationPoint;
        float[] viewPos;
        float[] viewDirection;
        float[] viewUp;
        float aperture;
        float focallength; // along view direction
        float eyeSeparation; // = 0.325f;
        public int screenheight;
        public int screenwidth;

        public Camera() {
            focallength = 4;
            eyeSeparation = (float) (focallength / 30.0);
            aperture = 60;
            rotationPoint = new float[] {
                    0, 0, 0
            };
            viewDirection = new float[] {
                    1, 0, 0
            };
            viewUp = new float[] {
                    0, 1, 0
            };
            viewPos = new float[] {
                    -3, 0, 0
            };
        }
    };

    public static void cross(float[] v1, float[] v2, float[] r) {
        r[0] = v1[1] * v2[2] - v2[1] * v1[2];
        r[1] = v1[2] * v2[0] - v2[2] * v1[0];
        r[2] = v1[0] * v2[1] - v2[0] * v1[1];
    }

    public static void scalarMultiply(float[] v, float s) {
        for (int i = 0; i < v.length; i++) {
            v[i] *= s;
        }
    }

    public static float magnitude(float[] v) {
        return (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    }

    public static void normalize(float[] v) {
        scalarMultiply(v, 1 / magnitude(v));
    }

    float[] r = new float[3];
    Camera camera;
    float near = 0.01f;
    float far = 1000;
    float ratio, wd2, ndfl;

    public void initCameraProjection(int width, int height) {
        camera = new Camera();
        camera.screenwidth = width;
        camera.screenheight = height;
        near = camera.focallength / 5;
        cross(camera.viewDirection, camera.viewUp, r);
        normalize(r);
        r[0] *= camera.eyeSeparation / 2.0;
        r[1] *= camera.eyeSeparation / 2.0;
        r[2] *= camera.eyeSeparation / 2.0;
        ratio = camera.screenwidth / (float) camera.screenheight;
        float radians = (float) (0.0174532925 * camera.aperture / 2);
        wd2 = (float) (near * Math.tan(radians));
        ndfl = near / camera.focallength;
    }
}
