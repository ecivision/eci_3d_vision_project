package com.ecivision.stereocalibration;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ecivision.stereocalibration.CalibrationObject.CalibType;
import com.ecivision.stereocalibration.R;

public class StereoCalibrationActivity extends Activity implements
		OnClickListener, OnCheckedChangeListener {
	private static final String TAG = "Ecivision::StereoCal::Activity";

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				System.loadLibrary("ecivision-stereo-depth");
				undistordedImageLeft = new Mat();
				undistordedImageRight = new Mat();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	public native void NativeMatLoadProcess(long addrMat, String fileName,
			String matName);

	public native void NativeUndistortProcess(long addrSrc, long addrDst,
			long addrIntr, long addrDist);
	

	public native double NativeComputeStereoAvgReprojectionErr(
			long addrImagePointsLeftMat, long addrImagePointsRightMat,
			long addrIntrinsicMatLeft, long addrIntrinsicMatRigth,
			long addrDistCoeffLeft, long addrDistCoeffRight, long addrFMatrix);

	public native void NativeTest(long addrSrcMat);

	public native void NativeCameraFlip(long addrSrc, long addrDst);

	public native void NativeDepthMap(long addrImageLeft, long addrImageRight,
			long roiRectLeft, long roiRectRight, long addrDepthMap,
			long addrQMat);

	private enum StateMachine {
		IDLE, CAPTURE_STATE, PROCESS_STATE, UNDISTORT_STATE
	}

	static final int toggleCamera = Menu.FIRST;
	static final int toggleCalibType = Menu.FIRST + 1;
	private static final int CALIB_LEFT_OK = 0;
	private static final int CALIB_RIGHT_OK = 1;
	// private int imageWidth = 640;
	// private int imageHeight = 480;
	private int currentDisplayImageNumberLeft = 1;
	private int currentDisplayImageNumberRight = 1;
	private int numberImages = 0;
	private int group1Id = 1;
	private int squareSizeMM = 1;
	static int patternHeight = 0;
	static int patternWidth = 0;
	private int captureCount = 0;
	private List<Integer> correctLeft;
	private List<Integer> correctRight;
	private boolean calibratedLeftOK = false;
	private boolean calibratedRightOK = false;

	private CalibrationObject calibCameraLeft;
	private CalibrationObject calibCameraRight;

	private double errorLeft = 0, errorLeft2 = 0;
	private double errorRight = 0, errorRight2 = 0;
	private double errorStereo = 0;
	private StateMachine currentState;

	private Size patternSize;

	private CalibType calibType = CalibType.CALIB_CIRCLES;

	private boolean CameraNative = true;
	private boolean calibButtonOK = false;
	private boolean loadedMatrixes = false;
	private boolean flipImage = true;
	private boolean calibrated = false;
	private boolean saveImages = false;
	private boolean widthTextOk = false;
	private boolean heightTextOk = false;
	private boolean isUndistorted = false;
	private boolean numCapturesTextOk = false;
	private boolean invert = false;
	private boolean scale = false;

	private String imagePathBase;
	private String imageExtension;
	private String imageDirectoryBase;
	private String imageNameBase;
	private Mat loadedIntrinsicsMat;
	private Mat loadedDistCoeffsMat;
	private Mat image_left;
	private Mat image_right;
	private Mat undistordedImageLeft;
	private Mat undistordedImageRight;
	private Mat outputImage;
	private Mat rMat, tMat, eMat, fMat, rMatLeft, rMatRight, pMatLeft, pMatRight, QMat;
	private Mat map1Left, map2Left, map1Right, map2Right;
	private Mat rectifiedLeft, rectifiedRight;
	private Mat rectifiedLeftMod, rectifiedRightMod;
	private Mat intrinsicsScaledLeft, intrinsicsScaledRight;
	private Size scaledSize;

	private Rect validROILeft, validROIRight;

	private Bitmap image_bmp_left;
	private Bitmap image_bmp_right;

	private Button startButton;
	private Button calibButton;
	private Button resetButton;
	private Button stereoButton;
	private Button rectifyButton;
	private Button depthButton;

	private Spinner calibTypeSpinner;
	private EditText patternWidthEditText;
	private EditText patternHeightEditText;
	private EditText patternNumberEditText;
	private EditText patternSquareSize;
	private ArrayAdapter<String> calibTypeAdapter;
	private List<String> calibTypeList = new ArrayList<String>();
	private Context mContext;
	private CheckBox saveImageCheckBox;
	private CheckBox undistortImageBox;
	private ImageView image_view_left;
	private ImageView image_view_right;

	private Handler handler;

	public StereoCalibrationActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_stereo_depth);
		currentState = StateMachine.IDLE;
		mContext = this;
		Intent intent = new Intent(
				"com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity.intent.action.Launch");
		intent.setComponent(ComponentName
				.unflattenFromString("com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity"));
		intent.addCategory("android.intent.category.DEFAULT");
		startActivityForResult(intent, 1);
		startButton = (Button) findViewById(R.id.start_button);
		calibButton = (Button) findViewById(R.id.calibrate_button);
		resetButton = (Button) findViewById(R.id.reset_button);
		stereoButton = (Button) findViewById(R.id.stereo_button);
		rectifyButton = (Button) findViewById(R.id.test_button);
		depthButton = (Button) findViewById(R.id.depth_button);
		correctLeft = new ArrayList<Integer>();
		correctRight = new ArrayList<Integer>();
		calibTypeSpinner = (Spinner) findViewById(R.id.calibtype_spinner);

		patternWidthEditText = (EditText) findViewById(R.id.patternwidth_edittext);
		patternHeightEditText = (EditText) findViewById(R.id.patternheight_edittext);
		patternNumberEditText = (EditText) findViewById(R.id.patternNumber_edittext);
		patternSquareSize = (EditText) findViewById(R.id.squaresize_mm_edittext);

		saveImageCheckBox = (CheckBox) findViewById(R.id.save_checkbox);
		undistortImageBox = (CheckBox) findViewById(R.id.undistort_checkbox);

		image_view_left = (ImageView) findViewById(R.id.image_view_left);
		image_view_right = (ImageView) findViewById(R.id.image_view_right);

		resetButton.setEnabled(false);
		undistortImageBox.setEnabled(false);
		image_view_left.setEnabled(false);
		image_view_right.setEnabled(false);
		depthButton.setEnabled(false);

		calibTypeList.add("Chessboard");
		calibTypeList.add("Circles");

		patternSquareSize.setText("1");

		// imageExtension = ".PNG";
		// // imageExtension = ".JPG";

		// Code that creates the spinner for chessboard or circles selection
		calibTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, calibTypeList);
		calibTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calibTypeSpinner.setAdapter(calibTypeAdapter);

		// Thread Message Handler
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALIB_LEFT_OK:
					calibratedLeftOK = true;
					checkMonoCalibration();
					break;
				case CALIB_RIGHT_OK:
					calibratedRightOK = true;
					checkMonoCalibration();
					break;
				}
			}
		};

		calibTypeSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							calibType = CalibType.CALIB_CHESSBOARD;
						} else {
							if (id == 1) {
								calibType = CalibType.CALIB_CIRCLES;
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		// Setting up number of images, pattern widht and height edit texts.
		// Once a valid width
		// ,height and number of images are acquired the start button is
		// enabled.
		patternHeightEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Heigth",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						heightTextOk = false;
					} else {
						patternHeight = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Height: " + patternHeight,
								Toast.LENGTH_SHORT).show();
						heightTextOk = true;
						if (widthTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					heightTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternWidthEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Width",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						widthTextOk = false;
					} else {
						patternWidth = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Width: " + patternWidth,
								Toast.LENGTH_SHORT).show();
						widthTextOk = true;
						if (heightTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					widthTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternNumberEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 3) {
						Toast.makeText(
								mContext,
								"Please enter a valid positive number of Patterns",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						numCapturesTextOk = false;
					} else {
						numberImages = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Numero de Capturas: " + numberImages,
								Toast.LENGTH_SHORT).show();
						numCapturesTextOk = true;
						if (heightTextOk & widthTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					numCapturesTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternSquareSize.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					int squareSize = Integer.parseInt(s.toString());
					if (squareSize < 1)
						squareSizeMM = 1;
					else
						squareSizeMM = squareSize;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				} catch (Exception e) {
					squareSizeMM = 1;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		// Once enabled, the start button will start the stereo calibration
		// capture phase and once done it enables the calib button. It is
		// disabled once again until a restart is done. The calibration for each
		// camera takes place on i's own thread, to improve speed and avoid the
		// UI thread from hanging.
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				patternHeightEditText.setEnabled(false);
				patternWidthEditText.setEnabled(false);
				saveImageCheckBox.setEnabled(false);
				undistortImageBox.setEnabled(false);
				calibTypeSpinner.setEnabled(false);
				resetButton.setEnabled(true);
				depthButton.setEnabled(false);

				patternSize = new Size(patternWidth, patternHeight);
				calibCameraLeft = new CalibrationObject(patternSize, calibType,
						squareSizeMM, saveImages);
				calibCameraLeft.setTag("Calibration:Object:Left");
				calibCameraLeft.allocateImages();
				calibCameraRight = new CalibrationObject(patternSize,
						calibType, squareSizeMM, saveImages);
				calibCameraRight.setTag("Calibration:Object:Right");
				calibCameraRight.allocateImages();

				Thread leftThread = new Thread(new Runnable() {
					public void run() {
						int currentImageNumber = 1;
						image_left = new Mat();
						while (currentImageNumber <= numberImages) {
							image_left = Highgui.imread(imagePathBase
									+ String.valueOf(currentImageNumber) + "_L"
									+ imageExtension);
							if (!image_left.empty()) {
								int correctImageNumber = 0;
								correctImageNumber = calibCameraLeft
										.CalibFindCorners(image_left);
								correctLeft.add(correctImageNumber);
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										displayLoadedImage(calibCameraLeft
												.returnCornersImage(),
												image_view_left);
									}

								});
							}
							currentImageNumber++;
						}
						Message msg = Message.obtain();
						msg.what = CALIB_LEFT_OK;
						handler.sendMessage(msg);
						Log.e(TAG,
								"Total corners left: "
										+ String.valueOf(calibCameraLeft
												.getImagePoints().size()));
					}
				});
				leftThread.setPriority(Thread.MAX_PRIORITY);
				leftThread.start();

				Thread rightThread = new Thread(new Runnable() {
					public void run() {
						int currentImageNumber = 1;
						image_right = new Mat();
						while (currentImageNumber <= numberImages) {
							image_right = Highgui.imread(imagePathBase
									+ String.valueOf(currentImageNumber) + "_R"
									+ imageExtension);
							if (!image_right.empty()) {
								int correctImageNumber = 0;
								correctImageNumber = calibCameraRight
										.CalibFindCorners(image_right);
								correctRight.add(correctImageNumber);
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										displayLoadedImage(calibCameraRight
												.returnCornersImage(),
												image_view_right);
									}

								});
							}
							currentImageNumber++;
						}
						Message msg = Message.obtain();
						msg.what = CALIB_RIGHT_OK;
						handler.sendMessage(msg);
						Log.e(TAG,
								"Total corners right: "
										+ String.valueOf(calibCameraRight
												.getImagePoints().size()));
					}
				});
				rightThread.setPriority(Thread.MAX_PRIORITY);
				rightThread.start();

				startButton.setEnabled(false);
			}
		});

		resetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				calibCameraLeft.close();
				calibCameraRight.close();
				try {
					calibCameraLeft.finalize();
					calibCameraRight.finalize();
				} catch (Throwable e) {
					e.printStackTrace();
				}
				patternHeightEditText.setText("");
				patternWidthEditText.setText("");
				currentState = StateMachine.IDLE;
				patternHeight = 0;
				patternWidth = 0;
				numberImages = 0;
				loadedMatrixes = false;
				widthTextOk = false;
				heightTextOk = false;
				calibButtonOK = false;
				calibrated = false;
				isUndistorted = false;
				image_view_left.setEnabled(false);
				image_view_right.setEnabled(false);
				captureCount = 0;
				patternHeightEditText.setEnabled(true);
				patternWidthEditText.setEnabled(true);
				saveImageCheckBox.setEnabled(true);
				calibTypeSpinner.setEnabled(true);
				currentState = StateMachine.IDLE;
				startButton.setEnabled(false);
				calibButton.setEnabled(false);
				resetButton.setEnabled(false);
				undistortImageBox.setEnabled(false);
			}
		});

		calibButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				patternHeightEditText.setEnabled(false);
				patternWidthEditText.setEnabled(false);
				calibTypeSpinner.setEnabled(false);
				currentState = StateMachine.IDLE;
				startButton.setEnabled(false);
				calibButton.setEnabled(false);
				undistortImageBox.setEnabled(false);
				Log.e(TAG, "Starting monocular calibration for both cameras.");
				cleanCapturedPoints();
				Thread leftThread = new Thread(new Runnable() {
					public void run() {
						errorLeft = calibCameraLeft.RunCalibration();
						errorLeft2 = calibCameraLeft
								.computeReprojectionErrors();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Camera Left Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Left Calibration Error is: "
												+ String.valueOf(errorLeft),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Left Calibration Error is: "
										+ String.valueOf(errorLeft));
								Log.e(TAG, "Left Reprojection Error is: "
										+ String.valueOf(errorLeft2));
							}
						});

						calibrated = true;
					}
				});
				leftThread.setPriority(Thread.MAX_PRIORITY);
				leftThread.start();

				Thread rightThread = new Thread(new Runnable() {
					public void run() {
						errorRight = calibCameraRight.RunCalibration();
						errorRight2 = calibCameraRight
								.computeReprojectionErrors();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Camera Right Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Right Calibration Error is: "
												+ String.valueOf(errorRight),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Right Calibration Error is: "
										+ String.valueOf(errorRight));
								Log.e(TAG, "Right Reprojection Error is: "
										+ String.valueOf(errorRight2));
							}
						});

						calibrated = true;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								undistortImageBox.setEnabled(true);
								image_view_left.setEnabled(true);
								image_view_right.setEnabled(true);
								stereoButton.setEnabled(true);
							}

						});
					}
				});
				rightThread.setPriority(Thread.MAX_PRIORITY);
				rightThread.start();

			}
		});

		stereoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Thread stereoThread = new Thread(new Runnable() {
					public void run() {
						rMat = new Mat();
						tMat = new Mat();
						eMat = new Mat();
						fMat = new Mat();

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Stereo Camera Calibration Started",
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Stereo Calibration Started");
								runOnUiThread(new Runnable() {

									@Override
									public void run() {
										stereoButton.setEnabled(false);
									}
								});
							}
						});

						errorStereo = Calib3d.stereoCalibrate(calibCameraLeft
								.getObjectPoints(), calibCameraLeft
								.getImagePoints(), calibCameraRight
								.getImagePoints(), calibCameraLeft
								.getIntrinsicsMatrix(), calibCameraLeft
								.getDistCoeffsMatrix(), calibCameraRight
								.getIntrinsicsMatrix(), calibCameraRight
								.getDistCoeffsMatrix(), image_left.size(),
								rMat, tMat, eMat, fMat, new TermCriteria(
										TermCriteria.EPS
												+ TermCriteria.MAX_ITER, 200,
										1 * (10 ^ (-6))),
								Calib3d.CALIB_FIX_INTRINSIC
								// + Calib3d.CALIB_FIX_PRINCIPAL_POINT
										+ Calib3d.CALIB_RATIONAL_MODEL
										// + Calib3d.CALIB_FIX_K4
										+ Calib3d.CALIB_FIX_K5);

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(
										mContext,
										"Stereo Camera Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Stereo Calibration Error is: "
												+ String.valueOf(errorStereo),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Stereo Calibration Error is: "
										+ String.valueOf(errorStereo));
								rectifyButton.setEnabled(true);
							}
						});

						calibrated = true;
					}
				});
				stereoThread.setPriority(Thread.MAX_PRIORITY);
				stereoThread.start();
			}
		});

		rectifyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new Runnable() {
					public void run() {
						validROILeft = new Rect();
						validROIRight = new Rect();
						rMatLeft = new Mat();
						rMatRight = new Mat();
						pMatLeft = new Mat();
						pMatRight = new Mat();
						QMat = new Mat();
						if (scale) {
							intrinsicsScaledLeft = new Mat();
							intrinsicsScaledRight = new Mat();
							scaledSize = new Size(
									image_left.size().width / 2.0, image_left
											.size().height / 2.0);
							Core.divide(calibCameraLeft.getIntrinsicsMatrix(),
									new Scalar(2.0), intrinsicsScaledLeft);
							Core.divide(calibCameraRight.getIntrinsicsMatrix(),
									new Scalar(2.0), intrinsicsScaledRight);
							Calib3d.stereoRectify(intrinsicsScaledLeft,
									calibCameraLeft.getDistCoeffsMatrix(),
									intrinsicsScaledRight,
									calibCameraRight.getDistCoeffsMatrix(),
									scaledSize, rMat, tMat, rMatLeft, rMatRight,
									pMatLeft, pMatRight, QMat,
									Calib3d.CALIB_ZERO_DISPARITY, -1,
									scaledSize, validROILeft, validROIRight);
						} else {
							Calib3d.stereoRectify(
									calibCameraLeft.getIntrinsicsMatrix(),
									calibCameraLeft.getDistCoeffsMatrix(),
									calibCameraRight.getIntrinsicsMatrix(),
									calibCameraRight.getDistCoeffsMatrix(),
									image_left.size(), rMat, tMat, rMatLeft,
									rMatRight, pMatLeft, pMatRight, QMat,
									Calib3d.CALIB_ZERO_DISPARITY, -1,
									image_left.size(), validROILeft,
									validROIRight);
						}
						
						map1Left = new Mat();
						map2Left = new Mat();
						map1Right = new Mat();
						map2Right = new Mat();

						if (scale) {
							Imgproc.initUndistortRectifyMap(
									intrinsicsScaledLeft,
									calibCameraLeft.getDistCoeffsMatrix(),
									rMatLeft, pMatLeft, scaledSize, CvType.CV_16SC2,
									map1Left, map2Left);
							Imgproc.initUndistortRectifyMap(
									intrinsicsScaledRight,
									calibCameraRight.getDistCoeffsMatrix(),
									rMatRight, pMatRight, scaledSize, CvType.CV_16SC2,
									map1Right, map2Right);
						} else {
							Imgproc.initUndistortRectifyMap(
									calibCameraLeft.getIntrinsicsMatrix(),
									calibCameraLeft.getDistCoeffsMatrix(),
									rMatLeft, pMatLeft, image_left.size(),
									CvType.CV_16SC2, map1Left, map2Left);
							Imgproc.initUndistortRectifyMap(
									calibCameraRight.getIntrinsicsMatrix(),
									calibCameraRight.getDistCoeffsMatrix(),
									rMatRight, pMatRight, image_left.size(),
									CvType.CV_16SC2, map1Right, map2Right);
						}
						
						saveMatFiles();

						rectifiedLeft = new Mat();
						rectifiedRight = new Mat();
						rectifiedLeftMod = new Mat();
						rectifiedRightMod = new Mat();
						image_left = Highgui.imread(imagePathBase
								+ String.valueOf(1) + "_L" + imageExtension);
						image_right = Highgui.imread(imagePathBase
								+ String.valueOf(1) + "_R" + imageExtension);
						
						Imgproc.remap(image_left, rectifiedLeft, map1Left,
								map2Left, Imgproc.INTER_LANCZOS4);						
						Imgproc.remap(image_right, rectifiedRight, map1Right,
								map2Right, Imgproc.INTER_LANCZOS4);
						rectifiedLeftMod = rectifiedLeft.clone();
						rectifiedRightMod = rectifiedRight.clone();

						Core.rectangle(rectifiedLeftMod, validROILeft.tl(),
								validROILeft.br(), new Scalar(0, 255, 0));
						Core.rectangle(rectifiedRightMod, validROIRight.tl(),
								validROIRight.br(), new Scalar(0, 255, 0));
						for (int i = 0; i < image_left.height(); i += 16) {
							Core.line(rectifiedLeftMod, new Point(0, i),
									new Point(image_left.width(), i),
									new Scalar(0, 0, 255), 2);
							Core.line(rectifiedRightMod, new Point(0, i),
									new Point(image_left.width(), i),
									new Scalar(0, 0, 255), 2);
						}

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								displayLoadedImage(rectifiedLeftMod,
										image_view_left);
								displayLoadedImage(rectifiedRightMod,
										image_view_right);
								Toast.makeText(mContext, "Stereo Camera Remap",
										Toast.LENGTH_SHORT).show();
								depthButton.setEnabled(true);
								rectifyButton.setEnabled(false);
								stereoButton.setEnabled(false);
							}
						});

					}
				}).start();
			}
		});

		depthButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				rectifiedLeftMod.release();
				Mat depthMap = new Mat();
				long startnow = android.os.SystemClock.uptimeMillis();
				NativeDepthMap(rectifiedLeft.getNativeObjAddr(),
						rectifiedRight.getNativeObjAddr(),
						Rect2PointMat(validROILeft).getNativeObjAddr(),
						Rect2PointMat(validROIRight).getNativeObjAddr(),
						depthMap.getNativeObjAddr(), QMat.getNativeObjAddr());
				long endnow = android.os.SystemClock.uptimeMillis();
				Log.e(TAG, "Depth estimation execution time: "
						+ (endnow - startnow) + " ms");
				displayLoadedImage(depthMap, image_view_left);
			}
		});

		image_view_left.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (currentDisplayImageNumberLeft <= numberImages) {
						image_left = Highgui.imread(imagePathBase
								+ String.valueOf(currentDisplayImageNumberLeft)
								+ "_L" + imageExtension);
						if (image_left.empty()) {
							Toast.makeText(
									mContext,
									"Image #"
											+ String.valueOf(currentDisplayImageNumberLeft)
											+ " not found", Toast.LENGTH_SHORT)
									.show();
						} else {
							if (isUndistorted) {
								Mat imageSubstractLeft = new Mat();
								Mat imageInvertedLeft = new Mat();
								Imgproc.undistort(image_left,
										undistordedImageLeft,
										calibCameraLeft.getIntrinsicsMatrix(),
										calibCameraLeft.getDistCoeffsMatrix());
								if (invert) {
									Core.absdiff(undistordedImageLeft,
											image_left, imageSubstractLeft);
									Core.bitwise_not(imageSubstractLeft,
											imageInvertedLeft);
									displayLoadedImage(imageInvertedLeft,
											image_view_left);
								} else {
									displayLoadedImage(undistordedImageLeft,
											image_view_left);
								}
							} else {
								displayLoadedImage(image_left, image_view_left);
							}
						}
						currentDisplayImageNumberLeft++;
					} else {
						currentDisplayImageNumberLeft = 1;
					}
				}
				return true;
			}
		});

		image_view_right.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (currentDisplayImageNumberRight <= numberImages) {
						image_right = Highgui.imread(imagePathBase
								+ String.valueOf(currentDisplayImageNumberRight)
								+ "_R" + imageExtension);
						if (image_right.empty()) {
							Toast.makeText(
									mContext,
									"Image #"
											+ String.valueOf(currentDisplayImageNumberRight)
											+ " not found", Toast.LENGTH_SHORT)
									.show();
						} else {
							if (isUndistorted) {
								Mat imageSubstractRight = new Mat();
								Mat imageInvertedRight = new Mat();
								Imgproc.undistort(image_right,
										undistordedImageRight,
										calibCameraRight.getIntrinsicsMatrix(),
										calibCameraRight.getDistCoeffsMatrix());
								if (invert) {
									Core.absdiff(undistordedImageRight,
											image_right, imageSubstractRight);
									Core.bitwise_not(imageSubstractRight,
											imageInvertedRight);
									displayLoadedImage(imageInvertedRight,
											image_view_right);
								} else {
									displayLoadedImage(undistordedImageRight,
											image_view_right);
								}
							} else {
								displayLoadedImage(image_right,
										image_view_right);
							}
						}
						currentDisplayImageNumberRight++;
					} else {
						currentDisplayImageNumberRight = 1;
					}
				}
				return true;
			}
		});

		saveImageCheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						saveImages = isChecked;
					}
				});

		undistortImageBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						if (isChecked) {
							if (!isUndistorted) {
								Imgproc.undistort(image_left,
										undistordedImageLeft,
										calibCameraLeft.getIntrinsicsMatrix(),
										calibCameraLeft.getDistCoeffsMatrix());

								Imgproc.undistort(image_right,
										undistordedImageRight,
										calibCameraRight.getIntrinsicsMatrix(),
										calibCameraRight.getDistCoeffsMatrix());
								Mat imageSubstractLeft = new Mat();
								Mat imageSubstractRight = new Mat();

								if (invert) {

									Core.absdiff(undistordedImageLeft,
											image_left, imageSubstractLeft);
									Core.bitwise_not(imageSubstractLeft,
											imageSubstractLeft);

									Core.absdiff(undistordedImageRight,
											image_right, imageSubstractRight);
									Core.bitwise_not(imageSubstractRight,
											imageSubstractRight);

								} else {
									imageSubstractLeft = undistordedImageLeft;
									imageSubstractRight = undistordedImageRight;
								}

								displayLoadedImage(imageSubstractLeft,
										image_view_left);
								displayLoadedImage(imageSubstractRight,
										image_view_right);

							}
							isUndistorted = true;
						} else {
							if (isUndistorted) {
								displayLoadedImage(image_left, image_view_left);
								displayLoadedImage(image_right,
										image_view_right);
							}
							isUndistorted = false;
						}

					}
				});

	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		menu.add(group1Id, toggleCalibType, toggleCalibType,
				"Toggle Calibration Method");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case toggleCalibType:
			if (calibType == CalibType.CALIB_CHESSBOARD) {
				calibType = CalibType.CALIB_CIRCLES;
				Toast.makeText(this, "Changed to Circles Calibration method",
						Toast.LENGTH_SHORT).show();
			} else {
				calibType = CalibType.CALIB_CHESSBOARD;
				Toast.makeText(this,
						"Changed to Chessboard Calibration method",
						Toast.LENGTH_SHORT).show();
			}
		}
		return true;
	}

	private boolean displayLoadedImage(Mat src, ImageView imageSurface) {
		if (src.empty()) {
			Log.e(TAG, "La imagen esta vacia");
			return false;
		} else {
			try {
				Bitmap src_bmp = Bitmap.createBitmap(src.width(), src.height(),
						Bitmap.Config.ARGB_8888);
				Mat tmpImage = new Mat();
				if (src.channels() > 1) {
					if (src.channels() == 3) {
						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGR2RGBA);
					} else {

						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGRA2RGBA);
					}
				} else {
					Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_GRAY2RGBA);
				}
				Utils.matToBitmap(tmpImage, src_bmp);
				imageSurface.setImageBitmap(src_bmp);
				return true;
			} catch (RuntimeException e) {
				Log.e(TAG, "Error al intentar mostrar la imagen:");
				Log.e(TAG, e.toString());
				return false;
			}
		}
	}

	public MatOfPoint Rect2PointMat(Rect src) {
		MatOfPoint dst;
		dst = new MatOfPoint(src.tl(), src.br());
		Log.e(TAG, "Mat of points rect " + dst.dump());
		return dst;
	}

	public double computeStereoAvgReprojectionErr() {
		@SuppressWarnings("unused")
		double error = 0;
		@SuppressWarnings("unused")
		int npoints = 0;
		for (int i = 0; i < numberImages; i++) {
			@SuppressWarnings("unused")
			int ntp = (int) (calibCameraLeft.getImagePoints2f().get(0).width() * calibCameraLeft
					.getImagePoints2f().get(0).height());
			MatOfPoint2f imagePointsMatLeftTemp = new MatOfPoint2f();
			MatOfPoint2f imagePointsMatRightTemp = new MatOfPoint2f();
			imagePointsMatLeftTemp = calibCameraLeft.getImagePoints2f().get(i);
			imagePointsMatRightTemp = calibCameraRight.getImagePoints2f()
					.get(i);

			Imgproc.undistortPoints(imagePointsMatLeftTemp,
					imagePointsMatLeftTemp,
					calibCameraLeft.getIntrinsicsMatrix(),
					calibCameraLeft.getDistCoeffsMatrix(), new Mat(),
					calibCameraLeft.getIntrinsicsMatrix());

			Imgproc.undistortPoints(imagePointsMatRightTemp,
					imagePointsMatRightTemp,
					calibCameraRight.getIntrinsicsMatrix(),
					calibCameraRight.getDistCoeffsMatrix(), new Mat(),
					calibCameraRight.getIntrinsicsMatrix());

		}
		return 0;
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

	}

	private void checkMonoCalibration() {
		if (calibratedLeftOK & calibratedRightOK) {
			Log.e(TAG, "Left correct images: " + correctLeft.toString());
			Log.e(TAG, "Right correct images: " + correctRight.toString());
			int countCorrespondences = 0;
			for (int i = 0; i < correctLeft.size(); i++) {
				if ((correctLeft.get(i) != -1) && (correctRight.get(i) != -1)) {
					countCorrespondences++;
				}
			}
			Log.e(TAG,
					"Coincident images: "
							+ String.valueOf(countCorrespondences));
			calibButton.setEnabled(true);
			calibratedLeftOK = false;
			calibratedRightOK = false;
		}
	}

	private void cleanCapturedPoints() {
		List<Integer> correctCombined = new ArrayList<Integer>();
		for (int i = 0; i < correctLeft.size(); i++) {
			if ((correctLeft.get(i) == -1) || (correctRight.get(i) == -1)) {
			} else
				correctCombined.add(correctLeft.get(i).intValue());
		}
		for (int i = 0; i < correctLeft.size(); i++) {
			if (correctLeft.get(i) == -1) {
				correctLeft.remove(i);
				--i;
			}
		}
		for (int i = 0; i < correctRight.size(); i++) {
			if (correctRight.get(i) == -1) {
				correctRight.remove(i);
				--i;
			}
		}
		int i = 0;
		while (i < correctCombined.size()) {
			if (correctCombined.get(i).intValue() != correctLeft.get(i)
					.intValue()) {
				correctLeft.remove(i);
				calibCameraLeft.removeImagePoints(i);
			}
			if (correctCombined.get(i).intValue() != correctRight.get(i)
					.intValue()) {
				correctRight.remove(i);
				calibCameraRight.removeImagePoints(i);
			} else {
				++i;
			}
		}
		while (correctCombined.size() != correctLeft.size()) {
			correctLeft.remove(correctLeft.size() - 1);
			calibCameraLeft.removeImagePoints(correctLeft.size() - 1);
		}
		while (correctCombined.size() != correctRight.size()) {
			correctRight.remove(correctRight.size() - 1);
			calibCameraRight.removeImagePoints(correctRight.size() - 1);
		}
	}
	
	private void saveMatFiles()
	{
		NativeMatSaveProcess(calibCameraLeft
				.getIntrinsicsMatrix().getNativeObjAddr(),
				imageDirectoryBase + "CameraMatrixLeft.yml",
				"Camera_Matrix");
		NativeMatSaveProcess(calibCameraLeft
				.getDistCoeffsMatrix().getNativeObjAddr(),
				imageDirectoryBase + "DistCoeffsLeft.yml",
				"Dist_Coeffs");
		NativeMatSaveProcess(calibCameraRight
				.getIntrinsicsMatrix().getNativeObjAddr(),
				imageDirectoryBase + "CameraMatrixRight.yml",
				"Camera_Matrix");
		NativeMatSaveProcess(calibCameraRight
				.getDistCoeffsMatrix().getNativeObjAddr(),
				imageDirectoryBase + "DistCoeffsRight.yml",
				"Dist_Coeffs");
		NativeMatSaveProcess(eMat.getNativeObjAddr(),
				imageDirectoryBase + "eMatrix.yml", "E_Matrix");
		NativeMatSaveProcess(fMat.getNativeObjAddr(),
				imageDirectoryBase + "fMatrix.yml", "F_Matrix");
		NativeMatSaveProcess(rMat.getNativeObjAddr(),
				imageDirectoryBase + "rMatrix.yml", "R_Matrix");
		NativeMatSaveProcess(tMat.getNativeObjAddr(),
				imageDirectoryBase + "tMatrix.yml", "T_Matrix");
		NativeMatSaveProcess(rMatLeft.getNativeObjAddr(),
				imageDirectoryBase + "R1Matrix.yml",
				"R1_Matrix");
		NativeMatSaveProcess(rMatRight.getNativeObjAddr(),
				imageDirectoryBase + "R2Matrix.yml",
				"R2_Matrix");
		NativeMatSaveProcess(pMatLeft.getNativeObjAddr(),
				imageDirectoryBase + "P1Matrix.yml",
				"P1_Matrix");
		NativeMatSaveProcess(pMatRight.getNativeObjAddr(),
				imageDirectoryBase + "P2Matrix.yml",
				"P2_Matrix");
		NativeMatSaveProcess(QMat.getNativeObjAddr(),
				imageDirectoryBase + "QMatrix.yml", "Q_Matrix");
		NativeMatSaveProcess(map1Left.getNativeObjAddr(),
				imageDirectoryBase + "Remap1Left.yml",
				"Remap1_Matrix");
		NativeMatSaveProcess(map2Left.getNativeObjAddr(),
				imageDirectoryBase + "Remap2Left.yml",
				"Remap2_Matrix");
		NativeMatSaveProcess(map1Right.getNativeObjAddr(),
				imageDirectoryBase + "Remap1Right.yml",
				"Remap1_Matrix");
		NativeMatSaveProcess(map2Right.getNativeObjAddr(),
				imageDirectoryBase + "Remap2Right.yml",
				"Remap2_Matrix");
		NativeMatSaveProcess(Rect2PointMat(validROILeft)
				.getNativeObjAddr(), imageDirectoryBase
				+ "ValidROILeft.yml", "ROILeft_Matrix");
		NativeMatSaveProcess(Rect2PointMat(validROIRight)
				.getNativeObjAddr(), imageDirectoryBase
				+ "ValidROIRight.yml", "ROIRight_Matrix");
		sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
				Uri.parse("file://"
						+ Environment
								.getExternalStorageDirectory())));
	}
	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode,
			Intent aData) {

		super.onActivityResult(aRequestCode, aResultCode, aData);
		imagePathBase = Environment.getExternalStorageDirectory() + "/DCIM2/"
				+ aData.getStringExtra("pathbase");
		imageNameBase = aData.getStringExtra("namebase");
		imageDirectoryBase = Environment.getExternalStorageDirectory()
				+ "/DCIM2/" + aData.getStringExtra("directorybase");
		imageExtension = aData.getStringExtra("extention");
		numberImages = aData.getIntExtra("numberimages", 0);

		if (numberImages > 0) {
			patternNumberEditText.setText(String.valueOf(numberImages));
			numCapturesTextOk = true;
		}
		Log.e(TAG, "Image path is: " + imagePathBase);
		Log.e(TAG, "Image Extention is: " + imageExtension);
		Log.e(TAG, "Number of Images is: " + numberImages);
	}

}
