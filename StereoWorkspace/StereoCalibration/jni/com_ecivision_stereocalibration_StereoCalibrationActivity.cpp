#include "com_ecivision_stereocalibration_StereoCalibrationActivity.h"
#include <opencv2/opencv.hpp>
#include <android/log.h>
#include "ExtraNativeUtils.h"
#include <stdio.h>

using namespace cv;
using namespace std;

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeMatSaveProcess(
		JNIEnv * env, jobject object, jlong addrMat, jstring fileName,
		jstring matName) {
	cv::Mat& savedMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName, NULL);
	const char *_matName = env->GetStringUTFChars(matName, NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName, cv::FileStorage::WRITE);
	try {
		matrixFile << _matName << savedMat;
		matrixFile.release();
		__android_log_write(ANDROID_LOG_ERROR, "Saved", _fileName);
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
	} catch (int e) {
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
		__android_log_write(ANDROID_LOG_ERROR, "Ecivision-Stereo",
				"Error saving Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeMatLoadProcess(
		JNIEnv * env, jobject object, jlong addrMat, jstring fileName,
		jstring matName) {
	cv::Mat& toLoadMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName, NULL);
	const char *_matName = env->GetStringUTFChars(matName, NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName, cv::FileStorage::READ);
	try {
		matrixFile[_matName] >> toLoadMat;
		matrixFile.release();
		__android_log_write(ANDROID_LOG_ERROR, "Loaded", _fileName);
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
	} catch (int e) {
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
		__android_log_write(ANDROID_LOG_ERROR, "Ecivision-Stereo",
				"Error loading Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeUndistortProcess(
		JNIEnv * env, jobject object, jlong addrSrc, jlong addrDst,
		jlong addrIntr, jlong addrDist) {
	cv::Mat& imgSrc = *(cv::Mat*) addrSrc;
	cv::Mat& imgDst = *(cv::Mat*) addrDst;
	cv::Mat& intrMat = *(cv::Mat*) addrIntr;
	cv::Mat& distMat = *(cv::Mat*) addrDist;
	cv::undistort(imgSrc, imgDst, intrMat, distMat);
}

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeCameraFlip(
		JNIEnv *, jobject, jlong addrSrc, jlong addrDst) {
	cv::Mat& src = *(cv::Mat*) addrSrc;
	cv::Mat& dst = *(cv::Mat*) addrDst;
	cv::Point2f src_center(src.cols / 2.0F, src.rows / 2.0F);
	cv::Mat rot_mat = getRotationMatrix2D(src_center, 90, 1.0);
	cv::warpAffine(src, dst, rot_mat, src.size());
	cv::flip(dst, dst, 0);
	__android_log_print(ANDROID_LOG_DEBUG, "Ecivision-Stereo",
			"Images flipped");
}

JNIEXPORT jdouble JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeComputeStereoAvgReprojectionErr(
		JNIEnv *env, jobject javaObject, jlong addrImagePointsLeftMat,
		jlong addrImagePointsRightMat, jlong addrIntrinsicMatLeft,
		jlong addrIntrinsicMatRight, jlong addrDistCoeffsLeft,
		jlong addrDistCoeffsRight, jlong addrFMatrix) {

	cv::Mat& imagePointsLeftMat = *(cv::Mat*) addrImagePointsLeftMat;
	cv::Mat& imagePointsRightMat = *(cv::Mat*) addrImagePointsRightMat;
	cv::Mat& intrinsicMatLeft = *(cv::Mat*) addrIntrinsicMatLeft;
	cv::Mat& intrinsicMatRight = *(cv::Mat*) addrIntrinsicMatRight;
	cv::Mat& distCoeffsLeft = *(cv::Mat*) addrDistCoeffsLeft;
	cv::Mat& distCoeffsRight = *(cv::Mat*) addrDistCoeffsRight;
	cv::Mat& fMatrix = *(cv::Mat*) addrFMatrix;

	vector<vector<Point2f> > imagePointsLeftVV;
	vector<vector<Point2f> > imagePointsRightVV;
	Mat2DToVector2D(imagePointsLeftMat, imagePointsLeftVV);
	Mat2DToVector2D(imagePointsRightMat, imagePointsRightVV);

	return ComputeStereoAverageReprojectionError(imagePointsLeftVV,
			imagePointsRightVV, intrinsicMatLeft, intrinsicMatRight,
			distCoeffsLeft, distCoeffsRight, fMatrix);

}

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeTest(
		JNIEnv * env, jobject javaObject, jlong addrSrcMat) {

	cv::Mat& imgSrc = *(cv::Mat*) addrSrcMat;
	stringstream buffer;

	buffer << "Cols: " << imgSrc.cols << " Rows: " << imgSrc.rows;
	__android_log_write(ANDROID_LOG_ERROR, "Test Native Function ",
			buffer.str().c_str());
	buffer.str("");

	vector<vector<Point2f> > vector2DMat;
	Mat2DToVector2D(imgSrc, vector2DMat);
	PrintVectorOfVectors2D(vector2DMat);
	/*double avgError = ComputeStereoAverageReprojectionError(
	 vector<vector<Point2f> > imagePointsLeft,
	 vector<vector<Point2f> > imagePointsRight, Mat &intrinsicMatrixLeft,
	 Mat& intrinsicMatrixRight, Mat& distCoeffsMatrixLeft,
	 Mat& distCoeffsMatrixRight, Mat& fMatrix);*/
	/*const float* RowAddr = imgSrc.ptr<float>(3);
	 buffer << "X: " << RowAddr[106] << " Y: " << RowAddr[107] << " ";

	 buffer << endl;
	 __android_log_write(ANDROID_LOG_ERROR, "Test Mat", buffer.str().c_str());
	 buffer.str("");

	 */

}

JNIEXPORT void JNICALL Java_com_ecivision_stereocalibration_StereoCalibrationActivity_NativeDepthMap(
		JNIEnv *env, jobject javaObject, jlong addrImageLeft,
		jlong addrImageRight, jlong addrValidROILeftMat,
		jlong addrValidROIRightMat, jlong addrDepthMap,jlong addrQMat) {

	StereoBM BlockMatcher;
	StereoVar var;
	StereoSGBM SGBMMatcher;


	cv::Mat& validROILeftMat = *(cv::Mat*) addrValidROILeftMat;
	cv::Mat& validROIRightMat = *(cv::Mat*) addrValidROIRightMat;
	cv::Mat& imageLeft = *(cv::Mat*) addrImageLeft;
	cv::Mat& imageRight = *(cv::Mat*) addrImageRight;
	cv::Mat& depthMap = *(cv::Mat*) addrDepthMap;

	Mat imageLeftGray,imageRightGray;
	cvtColor(imageLeft,imageLeftGray,COLOR_BGR2GRAY);
	cvtColor(imageRight,imageRightGray,COLOR_BGR2GRAY);
//	Mat imageLeftGrayScaled, imageRightGrayScaled;
//	resize(imageLeftGray, imageLeftGrayScaled, Size(), 0.7, 0.7, INTER_AREA);
//	resize(imageRightGray, imageRightGrayScaled, Size(), 0.7, 0.7, INTER_AREA);
	stringstream buffer;

	int SADWindowSize = 15;
	int numberOfDisparities = 80;

	const Point* validROILeftPtr = validROILeftMat.ptr<Point>(0);
	const Point* validROIRightPtr = validROIRightMat.ptr<Point>(0);
	Rect validROILeft(*validROILeftPtr, *(validROILeftPtr + 1));
	Rect validROIRight(*validROIRightPtr, *(validROIRightPtr + 1));

	buffer<<"ImageRight ROI "<<validROIRight;
	__android_log_write(ANDROID_LOG_ERROR, "Test Native Function ",
			buffer.str().c_str());
	buffer.str("");

	buffer<<"Imageleft ROI "<<validROILeft;
	__android_log_write(ANDROID_LOG_ERROR, "Test Native Function ",
			buffer.str().c_str());
	buffer.str("");

	Mat disp;

	//BlockMatcher.state->roi1 = validROILeft;
	//BlockMatcher.state->roi2 = validROIRight;
	BlockMatcher.state->preFilterCap = 31;
	BlockMatcher.state->SADWindowSize = SADWindowSize > 0 ? SADWindowSize : 9;
	BlockMatcher.state->minDisparity = 0;
	BlockMatcher.state->numberOfDisparities = numberOfDisparities;
	BlockMatcher.state->textureThreshold = 10;
	BlockMatcher.state->uniquenessRatio = 15;
	BlockMatcher.state->speckleWindowSize = 100;
	BlockMatcher.state->speckleRange = 32;
	BlockMatcher.state->disp12MaxDiff = 1;

	BlockMatcher(imageLeftGray,imageRightGray,disp);
	disp.convertTo(depthMap, CV_8U, 255/(numberOfDisparities*16.));

}

