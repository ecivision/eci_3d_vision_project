#include "ExtraNativeUtils.h"
#include <android/log.h>
#include <stdio.h>
#include <math.h>

using namespace std;
using namespace cv;

void Mat2DToVector2D(Mat &src2DMat, vector<vector<Point2f> >& vector2DMat) {
	for (int i = 0; i < src2DMat.rows; i++) {
		const Point2f* srcMatRowAddr = src2DMat.ptr<Point2f>(i);
		cv::vector<Point2f> temp2DVector(srcMatRowAddr,
				srcMatRowAddr + src2DMat.cols);
		vector2DMat.push_back(temp2DVector);
	}
}

void PrintVectorOfVectors2D(vector<vector<Point2f> > &vector2DMat) {
	stringstream buffer;
	for (vector<vector<Point2f> >::iterator it = vector2DMat.begin();
			it != vector2DMat.end(); ++it) {
		buffer << *it;
		__android_log_write(ANDROID_LOG_ERROR, "Vector of Vectors 2D:",
				buffer.str().c_str());
		buffer.str("");
	}

}

double ComputeStereoAverageReprojectionError(
		vector<vector<Point2f> > imagePointsLeft,
		vector<vector<Point2f> > &imagePointsRight, Mat &intrinsicMatrixLeft,
		Mat& intrinsicMatrixRight, Mat& distCoeffsMatrixLeft,
		Mat& distCoeffsMatrixRight, Mat& fMatrix) {

	double err = 0;
	int npoints = 0;
	vector<Vec3f> lines[2];
	for (int i = 0; i < imagePointsLeft.size(); i++) {
		int npt = (int) imagePointsLeft[0].size();
		Mat imgptLeft = Mat(imagePointsLeft[i]);
		Mat imgptRight = Mat(imagePointsRight[i]);

		undistortPoints(imgptLeft, imgptLeft, intrinsicMatrixLeft,
				distCoeffsMatrixLeft, Mat(), intrinsicMatrixLeft);
		computeCorrespondEpilines(imgptLeft, 1, fMatrix, lines[0]);
		undistortPoints(imgptRight, imgptRight, intrinsicMatrixRight,
				distCoeffsMatrixRight, Mat(), intrinsicMatrixRight);
		computeCorrespondEpilines(imgptRight, 2, fMatrix, lines[1]);

		for (int j = 0; j < npt; j++) {
			double errij = fabs((double)(
						imagePointsLeft[i][j].x * lines[1][j][0]
							+ imagePointsLeft[i][j].y * lines[1][j][1]
							+ lines[1][j][2]))
					+ fabs((double)(
							imagePointsRight[i][j].x * lines[0][j][0]
									+ imagePointsRight[i][j].y * lines[0][j][1]
									+ lines[0][j][2]));
			err += errij;
		}
		npoints += npt;
	}
	return err/npoints;
}
