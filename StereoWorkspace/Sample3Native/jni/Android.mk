LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES:=on
include $(OPENCV_ANDROID_SDK)/sdk/native/jni/OpenCV.mk
LOCAL_MODULE    := native_sample
LOCAL_SRC_FILES := com_ecivision_sample2old_Sample3View.cpp
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)