package com.ecivision.sample2old;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public abstract class SampleViewBase extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private static final String TAG = "Sample::SurfaceView";

    private Camera              mCamera;
    private SurfaceHolder       mHolder;
    private int                 mFrameWidth;
    private int                 mFrameHeight;
    private byte[]              mFrame;
    private boolean             mThreadRun;

    public SampleViewBase(Context context) {
        super(context);
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        Log.i(TAG, "Instantiated new " + this.getClass());
    }
    
    public SampleViewBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        Log.i(TAG, "Instantiated new " + this.getClass());
    } 

    public int getFrameWidth() {
        return mFrameWidth;
    }

    public int getFrameHeight() {
        return mFrameHeight;
    }

    public void surfaceChanged(SurfaceHolder _holder, int format, int width, int height) {
        Log.i(TAG, "surfaceCreated");
        if (mCamera != null) {
            Camera.Parameters params = mCamera.getParameters();
            List<Camera.Size> sizes = params.getSupportedPreviewSizes();
            mFrameWidth = width;
            mFrameHeight = height;

            // selecting optimal camera preview size
            {
                double minDiff = Double.MAX_VALUE;
                for (Camera.Size size : sizes) {
                    if (Math.abs(size.height - height) < minDiff) {
                        mFrameWidth = size.width;
                        mFrameHeight = size.height;
                        minDiff = Math.abs(size.height - height);
                    }
                }
            }

            params.setPreviewSize(getFrameWidth(), getFrameHeight());
            mCamera.setParameters(params);
            Log.i(TAG, "crear fakeview");
            SurfaceView fakeview = (SurfaceView) ((View)getParent()).findViewById(R.id.fakeCameraView);
            //SurfaceView fakeview = (SurfaceView) (findViewById(R.id.fakeCameraView));
            fakeview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            Log.i(TAG, "fakeview creada");
            //SurfaceView trueview = (SurfaceView) ((View)getParent()).findViewById(R.id.cvsurface); 
            //_holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            try {
            	Log.i(TAG, "cam-null");
				//mCamera.setPreviewDisplay(null);
            	
				mCamera.setPreviewDisplay(fakeview.getHolder());
				
            	//mCamera.setPreviewDisplay(trueview.getHolder());
				Log.i(TAG, "cam-null2");
				//mCamera.setPreviewDisplay(_holder); 
			} catch (IOException e) {
				Log.e(TAG, "mCamera.setPreviewDisplay fails: " + e);
			}
            try{
            Log.i(TAG, "start preview");
            mCamera.startPreview();
            Log.i(TAG, "started preview");
           } catch (RuntimeException e){Log.e(TAG, "mCamera.startPreview fails: "+ e);}
            //_holder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
            //fakeview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
            //_holder.setType(SurfaceHolder.SURFACE_TYPE_GPU);
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
    	
        Log.i(TAG, "surfaceCreated");
        mCamera = Camera.open();
        Log.i(TAG, "Camera.open()");
        
        mCamera.setPreviewCallback(new PreviewCallback() {
        	
            public void onPreviewFrame(byte[] data, Camera camera) {
            	
            	Log.i(TAG, "synch");
                synchronized (SampleViewBase.this) {
                	Log.i(TAG, "mFrame=data");
                    mFrame = data;
                    SampleViewBase.this.notify();
                    Log.i(TAG, "notify");
                }
            }
        });
        (new Thread(this)).start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(TAG, "surfaceDestroyed");
        mThreadRun = false;
        if (mCamera != null) {
            synchronized (this) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mCamera.release();
                mCamera = null;
            }
        }
    }

    protected abstract Bitmap processFrame(byte[] data);

    public void run() {
    	SurfaceView trueview = (SurfaceView) ((View)getParent()).findViewById(R.id.cvsurface);
    	//SurfaceView trueview = (SurfaceView) ((View)getParent()).findViewById(R.id.fakeCameraView);
        
        mThreadRun = true;
        Log.i(TAG, "Starting processing thread");
        while (mThreadRun) {
            Bitmap bmp = null;
            Log.i(TAG, "Antes del sync");
            
            synchronized (this) {
                try {
                	Log.i(TAG, "Try this.wait();");
                    this.wait();
                    Log.i(TAG, "bmp = processFrame1");
                    bmp = processFrame(mFrame);
                    Log.i(TAG, "bmp = processFrame2");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
          	
            if (bmp != null) {
            	Log.i(TAG, "Antes del lock");
            	//mHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
                //Canvas canvas = mHolder.lockCanvas();
            	//trueview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
                Canvas canvas = trueview.getHolder().lockCanvas();
                Log.i(TAG, "Despues del lock");
                if (canvas != null) {
                	Log.i(TAG, "canvas no nulo");
                    canvas.drawBitmap(bmp, (canvas.getWidth() - getFrameWidth()) / 2, (canvas.getHeight() - getFrameHeight()) / 2, null);
                    //mHolder.unlockCanvasAndPost(canvas);
                    trueview.getHolder().unlockCanvasAndPost(canvas);
                }else {
                	Log.i(TAG, "canvas nulo");
                
                }
                bmp.recycle();
            }
           
        }
    }
}