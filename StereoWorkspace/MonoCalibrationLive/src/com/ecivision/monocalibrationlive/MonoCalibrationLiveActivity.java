package com.ecivision.monocalibrationlive;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract.CommonDataKinds.Identity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecivision.cvcustomcamera.CameraPreview;
import com.ecivision.cvcustomcamera.CameraPreview.CVProcessingRunnable;
import com.ecivision.monocalibrationlive.CalibrationObject.CalibType;

//Implementation of the Runnable routine is needed for CVCustomCamera to work, this will be the OpenCv processing part of the code 
public class MonoCalibrationLiveActivity extends Activity implements
		CVProcessingRunnable {

	private LinearLayout mainLineaLayout;

	// Variables needed for CVCustomCameraView
	private Mat Image;
	private SurfaceView camView;
	private CameraPreview camPreview;
	private ImageView mCameraImageView = null;
	private SurfaceHolder camSurfaceHolder;
	private static int PreviewSizeWidth = 640;
	private static int PreviewSizeHeight = 480;
	private byte[] mFrameData;
	private Bitmap bitmap = null;
	private Mat mRGBA, mRGBAClone, mapX, mapY, newIntrinsicMat,
			undistortedImage;
	private TextView mStatusEditText;

	// Interface Variables
	private Button startButton;
	private Button calibButton;
	private Button resetButton;
	private Button stereoButton;
	private Button rectifyButton;
	private Button depthButton;

	private Spinner calibTypeSpinner;
	private EditText patternWidthEditText;
	private EditText patternHeightEditText;
	private EditText patternNumberEditText;
	private EditText patternSquareSize;
	private ArrayAdapter<String> calibTypeAdapter;
	private List<String> calibTypeList = new ArrayList<String>();
	private CheckBox saveImageCheckBox;
	private CheckBox undistortImageBox;

	// Image selection intent data
	private String imagePathBase;
	private String imageExtension;
	private String imageDirectoryBase;
	private String imageNameBase;
	private int numberImages = 0;
	private int squareSizeMM = 1;
	static int patternHeight = 0;
	static int patternWidth = 0;
	private int captureCount = 0;
	private int correctCapture = -1;
	private int correctRight = -1;
	private int correctCaptures = 0;

	static final long delayTimePeriod = 50;
	static final long delayTime = 2000;

	private Size patternSize, imageSize;

	private CalibrationObject calibCamera;

	private boolean CameraNative = true;
	private boolean calibButtonOK = false;
	private boolean loadedMatrixes = false;
	private boolean flipImage = true;
	private boolean calibrated = false;
	private boolean saveImages = false;
	private boolean widthTextOk = false;
	private boolean heightTextOk = false;
	private boolean isUndistorted = false;
	private boolean numCapturesTextOk = false;
	private boolean invert = false;
	private boolean scale = false;

	private boolean calibratedLeftOK = false;
	private boolean calibratedRightOK = false;
	private boolean verbose = true;
	private boolean readyForCapture = true;
	private boolean saveImages2 = true;

	private CalibType calibType = CalibType.CALIB_CIRCLES;

	private Context mContext;

	private double error = 0, error2 = 0;

	private enum StateMachine {
		IDLE, CAPTURE_STATE, UNDISTORT_STATE
	}

	private StateMachine currentState = StateMachine.IDLE;

	private final String TAG = "ecivision::Main";

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV loaded successfully 2");
				System.loadLibrary("ecivision-mono-calibration-live");
				Image = new Mat();
			}
				break;
			default: {
				Log.e(TAG, "OpenCV not loaded successfully");
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mono_calibration_live);

		// CVCustomCamera routine, MUST BE DONE EXACTLy THIS WAY, OTHERWISE IT
		// WONT WORK
		camView = (SurfaceView) findViewById(R.id.camview_surface_view);
		camSurfaceHolder = camView.getHolder();
		camPreview = new CameraPreview(0, PreviewSizeWidth, PreviewSizeHeight,
				this);

		mContext = this;

		camSurfaceHolder.addCallback(camPreview);
		camSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mainLineaLayout = (LinearLayout) findViewById(R.id.linear_main_layout);
		mainLineaLayout.bringToFront();
		mainLineaLayout.setBackgroundColor(Color.WHITE);
		mCameraImageView = (ImageView) findViewById(R.id.camera_out_imageview);
		mCameraImageView.setBackgroundColor(Color.WHITE);
		bitmap = Bitmap.createBitmap(PreviewSizeWidth, PreviewSizeHeight,
				Bitmap.Config.ARGB_8888);

		// Interface setup has to be done after CVCustomCamera is correctly
		// setup
		startButton = (Button) findViewById(R.id.start_button);
		calibButton = (Button) findViewById(R.id.calibrate_button);
		resetButton = (Button) findViewById(R.id.reset_button);
		rectifyButton = (Button) findViewById(R.id.test_button);
		calibTypeSpinner = (Spinner) findViewById(R.id.calibtype_spinner);

		patternWidthEditText = (EditText) findViewById(R.id.patternwidth_edittext);
		patternHeightEditText = (EditText) findViewById(R.id.patternheight_edittext);
		patternNumberEditText = (EditText) findViewById(R.id.patternNumber_edittext);
		patternSquareSize = (EditText) findViewById(R.id.squaresize_mm_edittext);
		mStatusEditText = (TextView) findViewById(R.id.status_textview);

		saveImageCheckBox = (CheckBox) findViewById(R.id.save_checkbox);
		undistortImageBox = (CheckBox) findViewById(R.id.undistort_checkbox);
		undistortImageBox.setEnabled(false);
		saveImageCheckBox.setEnabled(false);

		// Call the image set selection intent which will define where to save
		// the images and the calibration files
		Intent intent = new Intent(
				"com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity.intent.action.Launch");
		intent.setComponent(ComponentName
				.unflattenFromString("com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity"));
		intent.addCategory("android.intent.category.DEFAULT");
		startActivityForResult(intent, 1);

		// Code that creates the spinner for chessboard or circles selection

		calibTypeList.add("Chessboard");
		calibTypeList.add("Circles");

		calibTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, calibTypeList);
		calibTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calibTypeSpinner.setAdapter(calibTypeAdapter);

		calibTypeSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							calibType = CalibType.CALIB_CHESSBOARD;
						} else {
							if (id == 1) {
								calibType = CalibType.CALIB_CIRCLES;
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		// Setting up number of images, pattern width and height edit texts.
		// Once a valid width
		// ,height and number of images are acquired the start button is
		// enabled.
		patternHeightEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Heigth",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						heightTextOk = false;
					} else {
						patternHeight = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Height: " + patternHeight,
								Toast.LENGTH_SHORT).show();
						heightTextOk = true;
						if (widthTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					heightTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternWidthEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Width",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						widthTextOk = false;
					} else {
						patternWidth = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Width: " + patternWidth,
								Toast.LENGTH_SHORT).show();
						widthTextOk = true;
						if (heightTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					widthTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternNumberEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 3) {
						Toast.makeText(
								mContext,
								"Please enter a valid positive number of Patterns",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						numCapturesTextOk = false;
					} else {
						numberImages = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Numero de Capturas: " + numberImages,
								Toast.LENGTH_SHORT).show();
						numCapturesTextOk = true;
						if (heightTextOk & widthTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					numCapturesTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternSquareSize.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					int squareSize = Integer.parseInt(s.toString());
					if (squareSize < 1)
						squareSizeMM = 1;
					else
						squareSizeMM = squareSize;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				} catch (Exception e) {
					squareSizeMM = 1;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		// Once enabled, the start button will start the stereo calibration
		// capture phase and once done it enables the calib button. It is
		// disabled once again until a restart is done. The calibration for each
		// camera takes place on i's own thread, to improve speed and avoid the
		// UI thread from hanging.
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				readyForCapture = true;
				patternHeightEditText.setEnabled(false);
				patternWidthEditText.setEnabled(false);
				// saveImageCheckBox.setEnabled(false);
				// undistortImageBox.setEnabled(false);
				calibTypeSpinner.setEnabled(false);
				// resetButton.setEnabled(true);
				// depthButton.setEnabled(false);
				patternSize = new Size(patternWidth, patternHeight);
				imageSize = new Size(PreviewSizeWidth, PreviewSizeHeight);
				calibCamera = new CalibrationObject(patternSize, imageSize,
						calibType, squareSizeMM, saveImages);
				calibCamera.setTag("Calibration:Object");
				calibCamera.allocateImages();
				currentState = StateMachine.CAPTURE_STATE;
				startButton.setEnabled(false);
			}
		});

		calibButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startButton.setEnabled(false);
				calibButton.setEnabled(false);
				Log.e(TAG, "Starting monocular calibration for both cameras.");
				Thread calibThread = new Thread(new Runnable() {
					public void run() {
						error = calibCamera.RunCalibration();
						error2 = calibCamera.computeReprojectionErrors();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Camera Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Calibration Error is: "
												+ String.valueOf(error),
										Toast.LENGTH_SHORT).show();
								mStatusEditText
										.setText("Calibration Error is: "
												+ String.valueOf(error));
								Log.e(TAG,
										"Calibration Error is: "
												+ String.valueOf(error));
								Log.e(TAG,
										"Reprojection Error is: "
												+ String.valueOf(error2));
							}
						});

						Log.e(TAG, "Here");
						saveMatFiles();

						calibrated = true;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								calibButton.setEnabled(false);
								undistortImageBox.setEnabled(true);
							}

						});
					}
				});
				calibThread.setPriority(Thread.MAX_PRIORITY);
				calibThread.start();

			}
		});

		// CheckBox
		undistortImageBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							currentState = StateMachine.UNDISTORT_STATE;
						} else {
							currentState = StateMachine.IDLE;
						}
					}
				});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mono_calibration_live, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
		Log.e(TAG, "OpenCV loaded successfully 1");
	}

	// This function sets the runnable for the CVCustomCamera3D,which can be
	// found below
	// it recieves the raw image data to be processed and saves it on a global
	// variable which the runnable will
	// use (mFrameData)
	@Override
	public Runnable MyImageProcessing(byte[] frameData) {
		mFrameData = frameData;
		return DoImageProcessing;
	}

	// Before processing the camera�s setProcessing flag is asserted so that no
	// more frames are received while the image is being
	// processed, after processing is done it must be deasserted with
	// clearProcessing so that new frames can be received.
	// The runnable receives the raw data from mFrameData and turns it into a
	// processable Mat (mRGBA) with the help of the function
	// rawImageData2MatRGBA, processing is done on mRGBA and then a bitmap is
	// created to be shown on screen.
	
	private final Runnable DoImageProcessing = new Runnable() {
		public void run() {
			processThread.setPriority(Thread.MAX_PRIORITY);
			processThread.run();
		}
	};
	
	private final Thread processThread = new Thread(new Runnable() {

		@Override
		public void run() {
			camPreview.setProcessing();
			if (currentState == StateMachine.IDLE) {
				if (mRGBA != null) {
					mRGBA.release();
					Image.release();
				}
				mRGBA = rawImageData2MatRGBA(mFrameData, false);
				Utils.matToBitmap(mRGBA, bitmap);
				mCameraImageView.setImageBitmap(bitmap);
			} else {
				if (currentState == StateMachine.CAPTURE_STATE) {
					if (mRGBA != null) {
						mRGBA.release();
						Image.release();
					}
					mStatusEditText.setText("Captures Remainig: "
							+ String.valueOf(numberImages
									- correctCaptures));

					mRGBA = rawImageData2MatRGBA(mFrameData, false);
					if (correctCaptures < numberImages) {
						if (saveImages2) {
							mRGBAClone = mRGBA.clone();
						}
						correctCapture = calibCamera
								.CalibFindCorners(mRGBA);
						if ((correctCapture > -1)) {
							correctCaptures++;
							readyForCapture = false;
							if (saveImages2) {
								Mat toSaveImage = new Mat();
								Imgproc.cvtColor(mRGBAClone,
										toSaveImage,
										Imgproc.COLOR_RGBA2BGR);
								Highgui.imwrite(
										imagePathBase
												+ String.valueOf(correctCaptures)
												+ imageExtension,
										toSaveImage);
							}
						}

					} else {
						calibButton.setEnabled(true);
						if (saveImages2) {
							sendBroadcast(new Intent(
									Intent.ACTION_MEDIA_MOUNTED,
									Uri.parse("file://"
											+ Environment
													.getExternalStorageDirectory())));
						}
						currentState = StateMachine.IDLE;
					}
					Utils.matToBitmap(mRGBA, bitmap);
					mCameraImageView.setImageBitmap(bitmap);
				} else {
					if (currentState == StateMachine.UNDISTORT_STATE) {
						if (mRGBA != null) {
							mRGBA.release();
							Image.release();
						}
						mRGBA = rawImageData2MatRGBA(mFrameData, false);
						Mat undistortedImage = new Mat(mRGBA.size(),
								mRGBA.type());
						Imgproc.undistort(mRGBA, undistortedImage,
								calibCamera.getIntrinsicsMatrix(),
								calibCamera.getDistCoeffsMatrix());
						Utils.matToBitmap(undistortedImage, bitmap);
						mCameraImageView.setImageBitmap(bitmap);
					}
				}
			}
			camPreview.clearProcessing();
		}
	});

	public Mat rawImageData2MatRGBA(byte[] frameData, boolean is3DCamera) {
		// This lines must be unchanged
		Mat mYuvTemp = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2,
				PreviewSizeWidth, CvType.CV_8UC1);
		mYuvTemp.put(0, 0, frameData);
		Mat mRgba = new Mat();

		Imgproc.cvtColor(mYuvTemp, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
		if (is3DCamera) {
			Mat mRgbaResized = new Mat();
			Imgproc.resize(mRgba, mRgbaResized,
					new Size(mRgba.width(), mRgba.height() / 2));
			return mRgbaResized;
		} else {
			return mRgba;
		}
	};

	protected void onPause() {
		super.onPause();
	}

	private void saveMatFiles() {
		NativeMatSaveProcess(calibCamera.getIntrinsicsMatrix()
				.getNativeObjAddr(), imageDirectoryBase
				+ "CameraMatrixMono.yml", "Camera_Matrix_Mono");
		NativeMatSaveProcess(calibCamera.getDistCoeffsMatrix()
				.getNativeObjAddr(), imageDirectoryBase + "DistCoeffsMono.yml",
				"Dist_Coeffs_Mono");
		sendBroadcast(new Intent(
				Intent.ACTION_MEDIA_MOUNTED,
				Uri.parse("file://" + Environment.getExternalStorageDirectory())));
	}

	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode,
			Intent aData) {

		super.onActivityResult(aRequestCode, aResultCode, aData);
		imagePathBase = Environment.getExternalStorageDirectory() + "/DCIM2/"
				+ aData.getStringExtra("pathbase");
		imageNameBase = aData.getStringExtra("namebase");
		imageDirectoryBase = Environment.getExternalStorageDirectory()
				+ "/DCIM2/" + aData.getStringExtra("directorybase");
		imageExtension = aData.getStringExtra("extention");
		numberImages = aData.getIntExtra("numberimages", 0);

		if (numberImages > 0) {
			patternNumberEditText.setText(String.valueOf(numberImages));
			numCapturesTextOk = true;
		}
		Log.e(TAG, "Image path is: " + imagePathBase);
		Log.e(TAG, "Image Extention is: " + imageExtension);
		Log.e(TAG, "Number of Images is: " + numberImages);
	}

}
