package com.example.usb_host;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

public class USBHostActivity extends Activity {
	final String TAG="USB Host Test";
	private Button buttonEqual,buttonDifferent;
	private TextView recievedTextView;
	private UsbManager mUsbManager;
	public UsbDevice device;
    public UsbSerialDriver driver;
    private UsbSerialProber prober;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_usbhost);
		buttonEqual= (Button) findViewById(R.id.button_equal);
		buttonDifferent = (Button) findViewById(R.id.button_different);
		recievedTextView = (TextView) findViewById(R.id.textview_recieved);
		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
		//Get Connected Devices List
	    final List<DeviceEntry> result = new ArrayList<DeviceEntry>();
	    for (final UsbDevice device : mUsbManager.getDeviceList().values()) {
	        final List<UsbSerialDriver> drivers = 
	        =   UsbSerialProber.
	        
	        Log.d(TAG, "Found usb device: " + device);
	        if (drivers.isEmpty()) {
	            Log.d(TAG, "  - No UsbSerialDriver available.");
	            result.add(new DeviceEntry(device, null));
	        } else {
	            for (UsbSerialDriver driver : drivers) {
	                Log.d(TAG, "  + " + driver);
	                result.add(new DeviceEntry(device, driver));
	            }
	        }
	    }
	}

    private static class DeviceEntry {
        public UsbDevice device;
        public UsbSerialDriver driver;

        DeviceEntry(UsbDevice device, UsbSerialDriver driver) {
            this.device = device;
            this.driver = driver;
        }
    }
}
