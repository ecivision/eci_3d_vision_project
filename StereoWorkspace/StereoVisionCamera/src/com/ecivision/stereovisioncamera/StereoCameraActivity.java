package com.ecivision.stereovisioncamera;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import com.ecivision.stereovisioncamera.StereoCameraBridgeViewBase.CvCameraViewListener;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class StereoCameraActivity extends Activity implements CvCameraViewListener{
	private StereoCameraBridgeViewBase stereoCam;
	private static final String TAG = "StereoCameraActivity";
	private Mat temp;
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.v(TAG, "OpenCV Library Loaded Successfully");
//				stereoCam.setMaxFrameSize(640, 480);
				stereoCam.enableView();
//				stereoCam.enableFpsMeter();
				break;
			}
			default: {
				Log.e(TAG, "ERROR Could not Load OpenCV Library");
				super.onManagerConnected(status);
				break;
			}
			}
		}
	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        stereoCam = new StereoCameraPreview(this);
        setContentView(R.layout.activity_stereo_camera);
        stereoCam = (StereoCameraBridgeViewBase) findViewById(R.id.stereo_cam_preview);
		stereoCam.setCvCameraViewListener(this);
    }

	@Override
	public void onCameraViewStarted(int width, int height) {	
	temp=new Mat();
	}
	
	
	@Override
	public void onPause() {
		if (stereoCam != null)
			stereoCam.disableView();
		super.onPause();
	}


	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}
	
	@Override
	public void onCameraViewStopped() {
		temp.release();
	}

	@Override
	public Mat onCameraFrame(Mat inputFrame) {
		Log.e(TAG,"Hello Callback");
		Core.bitwise_not(inputFrame, temp);
		return null;
	}


    
}
