package com.ecivision.stereovisioncamera;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.lge.real3d.Real3D;
import com.lge.real3d.Real3DInfo;

public class StereoCameraPreview extends SurfaceView implements
		SurfaceHolder.Callback {
	private static final String TAG = "StereoCameraPreview";
	private static final boolean DEBUGON = true;
	private final static int CAMERA_STEREOSCOPIC = 2;
	public static final String KEY_CAMERA_INDEX = "camera-index";
	public static final String KEY_S3D_SUPPORTED_STR = "s3d-supported";

	SurfaceHolder mHolder;
	private int width, height;
	Camera stereoCamera;
	private Real3D mReal3D;
	private Parameters cameraParameters;

	// Necessary Functions

	public StereoCameraPreview(Context context) {
		super(context);
		init(context);
	}

	public StereoCameraPreview(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		init(context);
	}

	@Override
	public void surfaceChanged(SurfaceHolder _surfaceHolder, int _imageFormat,
			int _width, int _height) {
		mHolder = _surfaceHolder;
		width = _width;
		height = _height;
		startPreview(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		stereoCamera = get3DCamera(holder);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
		stopPreview();
		mHolder = surfaceHolder;
	}

	// Auxiliar Functions
	// --Init surface preview
	private void init(Context context) {
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		// Initialize Real3D object
		mReal3D = new Real3D(mHolder);
		// initialize real3d info
		// mReal3DInfo = new Real3DInfo(true, Real3D.REAL3D_TYPE_SS,
		// Real3D.REAL3D_ORDER_LR);
		// Set type to Side by Side.
		mReal3D.setReal3DInfo(new Real3DInfo(true, Real3D.REAL3D_TYPE_SS,
				Real3D.REAL3D_ORDER_LR));
		if (DEBUGON) {
			Log.e(TAG, "Surface Inited Correctly");
		}
	}

	// --Get 3D camera
	public Camera get3DCamera(SurfaceHolder holder) {
		Camera camera = null;
		int cameraID = CAMERA_STEREOSCOPIC;
		try {
			camera = Camera.open(cameraID);
			camera.setPreviewDisplay(holder);
			if (DEBUGON)
				Log.e(TAG, "Got 3D camera correctly");
		} catch (IOException ioe) {
			if (camera != null) {
				camera.release();
				Log.e(TAG, "Could not get 3D camera correctly");
			}
		} catch (NoSuchMethodError nsme) {
			Log.w(TAG, Log.getStackTraceString(nsme));
			Log.e(TAG, "Could not get 3D camera correctly");
		} catch (UnsatisfiedLinkError usle) {
			Log.w(TAG, Log.getStackTraceString(usle));
			Log.e(TAG, "Could not get 3D camera correctly");
		} catch (RuntimeException re) {
			Log.w(TAG, Log.getStackTraceString(re));
			Log.e(TAG, "Could not get 3D camera correctly");
			if (camera != null) {
				camera.release();
				Log.e(TAG, "Could not get 3D camera correctly");
			}
			camera = null;
		}
		return camera;
	}

	// --Preview Start Routine
	private void startPreview(int w, int h) {
		cameraParameters = stereoCamera.getParameters();
		List<Size> sizes = cameraParameters.getSupportedPreviewSizes();
		Size optimalSize = getOptimalPreviewSize(sizes, w, h);
		cameraParameters.set(KEY_CAMERA_INDEX, 2);
		cameraParameters.set(KEY_S3D_SUPPORTED_STR, "true");
		cameraParameters.setPreviewSize(optimalSize.width, optimalSize.height);
		cameraParameters.setPictureSize(optimalSize.width, optimalSize.height);
		Log.i(TAG, "optimalSize.width=" + optimalSize.width
				+ " optimalSize.height=" + optimalSize.height);
		stereoCamera.setParameters(cameraParameters);
		stereoCamera.startPreview();
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.05;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Size size : sizes) {
			Log.i(TAG, String.format("width=%d height=%d", size.width,
					size.height));
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	// --Preview Atop Preview
	private void stopPreview() {
		if (stereoCamera != null) {
			stereoCamera.stopPreview();
			stereoCamera.release();
			stereoCamera = null;
		}
	}

}
