package com.ecivision.stereocalibrationlive;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ecivision.cvcustomcamera.CameraPreview;
import com.ecivision.cvcustomcamera.CameraPreview.CVProcessingRunnable;
import com.ecivision.stereocalibrationlive.CalibrationObject.CalibType;

public class StereoCalibrationActivity extends Activity implements
		CVProcessingRunnable {

	private SurfaceView camView;
	private LinearLayout mainLineaLayout;
//	 private int PreviewSizeWidth = 720;
//	 private int PreviewSizeHeight = 576;
	private int PreviewSizeWidth = 640;
	private int PreviewSizeHeight = 480;
	private CameraPreview camPreview;
	private ImageView mCameraImageView = null;
	private Mat mRGBA, mRGBAClone;
	private Mat leftImage, rightImage;
	private byte[] mFrameData;
	private Bitmap bitmap = null;
	private int numberImages = 0;
	private int group1Id = 1;
	private int squareSizeMM = 1;
	static int patternHeight = 0;
	static int patternWidth = 0;
	private int captureCount = 0;
	private int correctLeft = -1;
	private int correctRight = -1;
	private boolean calibratedLeftOK = false;
	private boolean calibratedRightOK = false;
	private boolean verbose = true;
	private boolean readyForCapture = true;
	private boolean saveImages2 = true;

	private Object SynchLeft, SynchRight;

	private CalibrationObject calibCameraLeft;
	private CalibrationObject calibCameraRight;

	private double errorLeft = 0, errorLeft2 = 0;
	private double errorRight = 0, errorRight2 = 0;
	private double errorStereo = 0;

	private CalibType calibType = CalibType.CALIB_CIRCLES;

	private boolean CameraNative = true;
	private boolean calibButtonOK = false;
	private boolean loadedMatrixes = false;
	private boolean flipImage = true;
	private boolean calibrated = false;
	private boolean saveImages = false;
	private boolean widthTextOk = false;
	private boolean heightTextOk = false;
	private boolean isUndistorted = false;
	private boolean numCapturesTextOk = false;
	private boolean invert = false;
	private boolean scale = false;

	static final long delayTimePeriod = 50;
	static final long delayTime = 2000;

	private Size patternSize, imageSize;

	private String imagePathBase;
	private String imageExtension;
	private String imageDirectoryBase;
	private String imageNameBase;
	private Mat loadedIntrinsicsMat;
	private Mat loadedDistCoeffsMat;
	private Mat image_left;
	private Mat image_right;
	private Mat undistordedImageLeft;
	private Mat undistordedImageRight;
	private Mat outputImage;
	private Mat rMat, tMat, eMat, fMat, rMatLeft, rMatRight, pMatLeft,
			pMatRight, QMat;
	private Mat map1Left, map2Left, map1Right, map2Right;
	private Mat rectifiedLeft, rectifiedRight;
	private Mat rectifiedLeftMod, rectifiedRightMod;
	private Mat intrinsicsScaledLeft, intrinsicsScaledRight;

	private Button startButton;
	private Button calibButton;
	private Button resetButton;
	private Button stereoButton;
	private Button rectifyButton;
	private Button depthButton;

	private Spinner calibTypeSpinner;
	private EditText patternWidthEditText;
	private EditText patternHeightEditText;
	private EditText patternNumberEditText;
	private EditText patternSquareSize;
	private ArrayAdapter<String> calibTypeAdapter;
	private List<String> calibTypeList = new ArrayList<String>();
	private Context mContext;
	private CheckBox saveImageCheckBox;
	private CheckBox undistortImageBox;
	
	private SurfaceHolder camSurfaceHolder;

	private Rect validROILeft, validROIRight;

	private int correctCaptures = 0;

	private Handler handler;

	private NextCaptureCount mNextCaptureCounter;

	private static final String TAG = "Ecivision::StereoLive::Activity";

	private enum StateMachine {
		IDLE, CAPTURE_STATE, RECTIFY_STATE
	}

	private StateMachine currentState = StateMachine.IDLE;

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV loaded successfully 2");
				System.loadLibrary("ecivision-calibration-live");
				leftImage = new Mat();
				rightImage = new Mat();
			}
				break;
			default: {
				Log.e(TAG, "OpenCV not loaded successfully");
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stereo_calibration);
		//CVCustomCamera routine, MUST BE DONE EXACTLy THIS WAY, OTHERWISE IT WONT WORK
		camView = (SurfaceView) findViewById(R.id.camview_surface_view);
		camSurfaceHolder = camView.getHolder();
		camPreview = new CameraPreview(PreviewSizeWidth, PreviewSizeHeight,
				this);

		mContext = this;

		camSurfaceHolder.addCallback(camPreview);
		camSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mainLineaLayout = (LinearLayout) findViewById(R.id.linear_main_layout);
		mainLineaLayout.bringToFront();
		mainLineaLayout.setBackgroundColor(Color.WHITE);
		mCameraImageView = (ImageView) findViewById(R.id.camera_out_imageview);
		mCameraImageView.setBackgroundColor(Color.WHITE);
		bitmap = Bitmap.createBitmap(PreviewSizeWidth, PreviewSizeHeight / 2,
				Bitmap.Config.ARGB_8888);

		startButton = (Button) findViewById(R.id.start_button);
		calibButton = (Button) findViewById(R.id.calibrate_button);
		resetButton = (Button) findViewById(R.id.reset_button);
		stereoButton = (Button) findViewById(R.id.stereo_button);
		rectifyButton = (Button) findViewById(R.id.test_button);
		depthButton = (Button) findViewById(R.id.depth_button);
		calibTypeSpinner = (Spinner) findViewById(R.id.calibtype_spinner);

		patternWidthEditText = (EditText) findViewById(R.id.patternwidth_edittext);
		patternHeightEditText = (EditText) findViewById(R.id.patternheight_edittext);
		patternNumberEditText = (EditText) findViewById(R.id.patternNumber_edittext);
		patternSquareSize = (EditText) findViewById(R.id.squaresize_mm_edittext);

		saveImageCheckBox = (CheckBox) findViewById(R.id.save_checkbox);
		undistortImageBox = (CheckBox) findViewById(R.id.undistort_checkbox);
		//
		// resetButton.setEnabled(false);
		// undistortImageBox.setEnabled(false);
		// depthButton.setEnabled(false);

		calibTypeList.add("Chessboard");
		calibTypeList.add("Circles");

		patternSquareSize.setText("1");
		
		// Call the image set selection intent which will define where to save
		// the images and the calibration files
		Intent intent = new Intent(
				"com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity.intent.action.Launch");
		intent.setComponent(ComponentName
				.unflattenFromString("com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity"));
		intent.addCategory("android.intent.category.DEFAULT");
		startActivityForResult(intent, 1);

		// Code that creates the spinner for chessboard or circles selection
		calibTypeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, calibTypeList);
		calibTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		calibTypeSpinner.setAdapter(calibTypeAdapter);

		calibTypeSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							calibType = CalibType.CALIB_CHESSBOARD;
						} else {
							if (id == 1) {
								calibType = CalibType.CALIB_CIRCLES;
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		// Setting up number of images, pattern width and height edit texts.
		// Once a valid width
		// ,height and number of images are acquired the start button is
		// enabled.
		patternHeightEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Heigth",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						heightTextOk = false;
					} else {
						patternHeight = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Height: " + patternHeight,
								Toast.LENGTH_SHORT).show();
						heightTextOk = true;
						if (widthTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					heightTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternWidthEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 2) {
						Toast.makeText(mContext,
								"Please enter a valid positive pattern Width",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						widthTextOk = false;
					} else {
						patternWidth = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Pattern Width: " + patternWidth,
								Toast.LENGTH_SHORT).show();
						widthTextOk = true;
						if (heightTextOk & numCapturesTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					widthTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternNumberEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				try {
					if (Integer.parseInt(arg0.toString()) < 3) {
						Toast.makeText(
								mContext,
								"Please enter a valid positive number of Patterns",
								Toast.LENGTH_SHORT).show();
						startButton.setEnabled(false);
						numCapturesTextOk = false;
					} else {
						numberImages = Integer.parseInt(arg0.toString());
						Toast.makeText(mContext,
								"Numero de Capturas: " + numberImages,
								Toast.LENGTH_SHORT).show();
						numCapturesTextOk = true;
						if (heightTextOk & widthTextOk) {
							startButton.setEnabled(true);
							Toast.makeText(mContext, "OK", Toast.LENGTH_SHORT)
									.show();
						}
					}
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					startButton.setEnabled(false);
					numCapturesTextOk = false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		patternSquareSize.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				try {
					int squareSize = Integer.parseInt(s.toString());
					if (squareSize < 1)
						squareSizeMM = 1;
					else
						squareSizeMM = squareSize;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				} catch (Exception e) {
					squareSizeMM = 1;
					Log.e(TAG, "Square Size: " + String.valueOf(squareSizeMM)
							+ " mm");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		// Once enabled, the start button will start the stereo calibration
		// capture phase and once done it enables the calib button. It is
		// disabled once again until a restart is done. The calibration for each
		// camera takes place on i's own thread, to improve speed and avoid the
		// UI thread from hanging.
		startButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				readyForCapture = true;
				mNextCaptureCounter = new NextCaptureCount(delayTime,
						delayTimePeriod);
				mNextCaptureCounter.cancel();
				patternHeightEditText.setEnabled(false);
				patternWidthEditText.setEnabled(false);
				// saveImageCheckBox.setEnabled(false);
				// undistortImageBox.setEnabled(false);
				calibTypeSpinner.setEnabled(false);
				// resetButton.setEnabled(true);
				// depthButton.setEnabled(false);
				patternSize = new Size(patternWidth, patternHeight);
				imageSize = new Size(PreviewSizeWidth / 2,
						PreviewSizeHeight / 2);
				calibCameraLeft = new CalibrationObject(patternSize, imageSize,
						calibType, squareSizeMM, saveImages);
				calibCameraLeft.setTag("Calibration:Object:Left");
				calibCameraLeft.allocateImages();
				calibCameraRight = new CalibrationObject(patternSize,
						imageSize, calibType, squareSizeMM, saveImages);
				calibCameraRight.setTag("Calibration:Object:Right");
				calibCameraRight.allocateImages();
				currentState = StateMachine.CAPTURE_STATE;
				startButton.setEnabled(false);
			}
		});

		calibButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startButton.setEnabled(false);
				calibButton.setEnabled(false);
				Log.e(TAG, "Starting monocular calibration for both cameras.");
				Thread leftThread = new Thread(new Runnable() {
					public void run() {
						errorLeft = calibCameraLeft.RunCalibration();
						errorLeft2 = calibCameraLeft
								.computeReprojectionErrors();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Camera Left Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Left Calibration Error is: "
												+ String.valueOf(errorLeft),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Left Calibration Error is: "
										+ String.valueOf(errorLeft));
								Log.e(TAG, "Left Reprojection Error is: "
										+ String.valueOf(errorLeft2));
							}
						});

						calibrated = true;
					}
				});
				leftThread.setPriority(Thread.MAX_PRIORITY);
				leftThread.start();

				Thread rightThread = new Thread(new Runnable() {
					public void run() {
						errorRight = calibCameraRight.RunCalibration();
						errorRight2 = calibCameraRight
								.computeReprojectionErrors();
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Camera Right Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Right Calibration Error is: "
												+ String.valueOf(errorRight),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Right Calibration Error is: "
										+ String.valueOf(errorRight));
								Log.e(TAG, "Right Reprojection Error is: "
										+ String.valueOf(errorRight2));
							}
						});

						calibrated = true;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								 stereoButton.setEnabled(true);
							}

						});
					}
				});
				rightThread.setPriority(Thread.MAX_PRIORITY);
				rightThread.start();

			}
		});

		stereoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Thread stereoThread = new Thread(new Runnable() {
					public void run() {
						rMat = new Mat();
						tMat = new Mat();
						eMat = new Mat();
						fMat = new Mat();

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext,
										"Stereo Camera Calibration Started",
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Stereo Calibration Started");
								runOnUiThread(new Runnable() {

									@Override
									public void run() {
										stereoButton.setEnabled(false);
									}
								});
							}
						});

						errorStereo = Calib3d.stereoCalibrate(calibCameraLeft
								.getObjectPoints(), calibCameraLeft
								.getImagePoints(), calibCameraRight
								.getImagePoints(), calibCameraLeft
								.getIntrinsicsMatrix(), calibCameraLeft
								.getDistCoeffsMatrix(), calibCameraRight
								.getIntrinsicsMatrix(), calibCameraRight
								.getDistCoeffsMatrix(), imageSize,
								rMat, tMat, eMat, fMat, new TermCriteria(
										TermCriteria.EPS
												+ TermCriteria.MAX_ITER, 200,
										1 * (10 ^ (-6))),
								Calib3d.CALIB_FIX_INTRINSIC
								// + Calib3d.CALIB_FIX_PRINCIPAL_POINT
										+ Calib3d.CALIB_RATIONAL_MODEL
										// + Calib3d.CALIB_FIX_K4
										+ Calib3d.CALIB_FIX_K5);

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(
										mContext,
										"Stereo Camera Calibrated Successfully",
										Toast.LENGTH_SHORT).show();
								Toast.makeText(
										mContext,
										"Stereo Calibration Error is: "
												+ String.valueOf(errorStereo),
										Toast.LENGTH_SHORT).show();
								Log.e(TAG, "Stereo Calibration Error is: "
										+ String.valueOf(errorStereo));
								rectifyButton.setEnabled(true);
							}
						});

						calibrated = true;
					}
				});
				stereoThread.setPriority(Thread.MAX_PRIORITY);
				stereoThread.start();
			}
		});

		rectifyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new Runnable() {
					public void run() {
						validROILeft = new Rect();
						validROIRight = new Rect();
						rMatLeft = new Mat();
						rMatRight = new Mat();
						pMatLeft = new Mat();
						pMatRight = new Mat();
						QMat = new Mat();
						Log.e(TAG,"Here");
						Calib3d.stereoRectify(
								calibCameraLeft.getIntrinsicsMatrix(),
								calibCameraLeft.getDistCoeffsMatrix(),
								calibCameraRight.getIntrinsicsMatrix(),
								calibCameraRight.getDistCoeffsMatrix(),
								imageSize, rMat, tMat, rMatLeft,
								rMatRight, pMatLeft, pMatRight, QMat,
								Calib3d.CALIB_ZERO_DISPARITY, 1,
								imageSize, validROILeft, validROIRight);

						map1Left = new Mat();
						map2Left = new Mat();
						map1Right = new Mat();
						map2Right = new Mat();

						Imgproc.initUndistortRectifyMap(
								calibCameraLeft.getIntrinsicsMatrix(),
								calibCameraLeft.getDistCoeffsMatrix(),
								rMatLeft, pMatLeft, imageSize,
								CvType.CV_16SC2, map1Left, map2Left);
						Imgproc.initUndistortRectifyMap(
								calibCameraRight.getIntrinsicsMatrix(),
								calibCameraRight.getDistCoeffsMatrix(),
								rMatRight, pMatRight,imageSize,
								CvType.CV_16SC2, map1Right, map2Right);
						
						

						saveMatFiles();
						currentState=StateMachine.RECTIFY_STATE;

					}
				}).start();
			}
		});

	}

	@Override
	public Runnable MyImageProcessing(byte[] frameData) {
		mFrameData = frameData;
		return DoImageProcessing;
	}

	private Runnable DoImageProcessing = new Runnable() {
		public void run() {
			// Log.i("MyRealTimeImageProcessing", "DoImageProcessing():");
			camPreview.setProcessing();
			if (currentState == StateMachine.IDLE) {
				if (mRGBA != null) {
					mRGBA.release();
					leftImage.release();
					rightImage.release();
				}
				mRGBA = rawImageData2MatRGBA(mFrameData);
				Utils.matToBitmap(mRGBA, bitmap);
				mCameraImageView.setImageBitmap(bitmap);
			} else {
				if (currentState == StateMachine.CAPTURE_STATE) {
					if (mRGBA != null) {
						mRGBA.release();
						leftImage.release();
						rightImage.release();
					}
					mRGBA = rawImageData2MatRGBA(mFrameData);
					if (correctCaptures < numberImages) {
						if (readyForCapture) {
							mNextCaptureCounter.cancel();
							if (saveImages2) {
								mRGBAClone = mRGBA.clone();
							}
							leftImage = new Mat(mRGBA, new Range(0,
									mRGBA.height()), new Range(0,
									mRGBA.width() / 2));

							rightImage = new Mat(mRGBA, new Range(0,
									mRGBA.height()), new Range(
									(mRGBA.width() + 1) / 2, mRGBA.width()));

							correctLeft = calibCameraLeft
									.CalibFindCorners(leftImage);
							correctRight = calibCameraRight
									.CalibFindCorners(rightImage);
							Log.e(TAG, "left: " + correctLeft + " right: "
									+ correctRight);
							if ((correctLeft > -1) && (correctRight > -1)
									&& (correctLeft == correctRight)) {
								correctCaptures++;
								readyForCapture = false;
								mNextCaptureCounter.start();
								if (saveImages2) {
									Mat toSaveImage = new Mat();
									Imgproc.cvtColor(mRGBAClone, toSaveImage,
											Imgproc.COLOR_RGBA2BGR);
									Highgui.imwrite(
											imagePathBase
													+ String.valueOf(correctCaptures)
													+ imageExtension,
											toSaveImage);
								}
							} else {
								if ((correctLeft > -1)
										&& (correctLeft > correctRight)) {
									calibCameraLeft.removeLastImagePoints();
									readyForCapture = false;
									mNextCaptureCounter.start();
								} else {
									if ((correctRight > -1)
											&& (correctLeft < correctRight))
										calibCameraRight
												.removeLastImagePoints();
									readyForCapture = false;
									mNextCaptureCounter.start();
								}
							}
						}
					} else {
						calibButton.setEnabled(true);
						if (saveImages2) {
							sendBroadcast(new Intent(
									Intent.ACTION_MEDIA_MOUNTED,
									Uri.parse("file://"
											+ Environment
													.getExternalStorageDirectory())));
						}
						currentState = StateMachine.IDLE;
					}
					Utils.matToBitmap(mRGBA, bitmap);
					mCameraImageView.setImageBitmap(bitmap);

				} else {
					if (currentState == StateMachine.RECTIFY_STATE) {

						if (mRGBA != null) {
							mRGBA.release();
							leftImage.release();
							rightImage.release();
							if(rectifiedLeft!=null)
							rectifiedLeft.release();
							if(rectifiedRight!=null)
							rectifiedRight.release();
						}
						mRGBA = rawImageData2MatRGBA(mFrameData);
						leftImage = new Mat(mRGBA,
								new Range(0, mRGBA.height()), new Range(0,
										mRGBA.width() / 2));

						rightImage = new Mat(mRGBA,
								new Range(0, mRGBA.height()), new Range(
										(mRGBA.width() + 1) / 2, mRGBA.width()));
						
						Mat newMRGBA = new Mat(mRGBA.size(), mRGBA.type());
						rectifiedLeft = new Mat(newMRGBA,
								new Range(0, newMRGBA.height()), new Range(0,
										newMRGBA.width() / 2));
						rectifiedRight = new Mat(newMRGBA,
								new Range(0, newMRGBA.height()), new Range(
										(newMRGBA.width() + 1) / 2, newMRGBA.width())); 
								
						Imgproc.remap(leftImage, rectifiedLeft, map1Left,
								map2Left, Imgproc.INTER_LANCZOS4);
						Imgproc.remap(rightImage, rectifiedRight, map1Right,
								map2Right, Imgproc.INTER_LANCZOS4);

						Core.rectangle(rectifiedLeft, validROILeft.tl(),
								validROILeft.br(), new Scalar(0, 255, 0));
						Core.rectangle(rectifiedRight, validROIRight.tl(),
								validROIRight.br(), new Scalar(0, 255, 0));
						
						for (int i = 0; i < leftImage.height(); i += 16) {
							Core.line(rectifiedLeft, new Point(0, i),
									new Point(leftImage.width(), i),
									new Scalar(0, 0, 255), 2);
							Core.line(rectifiedRight, new Point(0, i),
									new Point(rightImage.width(), i),
									new Scalar(0, 0, 255), 2);
						
							Log.e(TAG,"Rectified correctly");

						Utils.matToBitmap(newMRGBA, bitmap);
						mCameraImageView.setImageBitmap(bitmap);
						}

					}
				}
			}
			camPreview.clearProcessing();
		}
	};

	public Mat rawImageData2MatRGBA(byte[] frameData) {
		Mat mYuvTemp = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2,
				PreviewSizeWidth, CvType.CV_8UC1);
		mYuvTemp.put(0, 0, frameData);
		Mat mRgba = new Mat();
		Mat mRgbaResized = new Mat();
		Imgproc.cvtColor(mYuvTemp, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
		Imgproc.resize(mRgba, mRgbaResized,
				new Size(mRgba.width(), mRgba.height() / 2));
		return mRgbaResized;
	};

	public Mat rawImageData2MatGRAY(byte[] frameData) {
		Mat mYuvTemp = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2,
				PreviewSizeWidth, CvType.CV_8UC1);
		mYuvTemp.put(0, 0, frameData);
		Mat mGray = new Mat();
		Mat mGrayResized = new Mat();
		Imgproc.cvtColor(mYuvTemp, mGray, Imgproc.COLOR_YUV2GRAY_NV21, 1);
		Imgproc.resize(mGray, mGrayResized,
				new Size(mGray.width(), mGray.height() / 2));
		return mGrayResized;
	};

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
		Log.e(TAG, "OpenCV loaded successfully 1");
	}

	protected void onPause() {
		super.onPause();
	}

	private void saveMatFiles() {
		NativeMatSaveProcess(calibCameraLeft.getIntrinsicsMatrix()
				.getNativeObjAddr(), imageDirectoryBase
				+ "CameraMatrixLeft.yml", "Camera_Matrix");
		NativeMatSaveProcess(calibCameraLeft.getDistCoeffsMatrix()
				.getNativeObjAddr(), imageDirectoryBase + "DistCoeffsLeft.yml",
				"Dist_Coeffs");
		NativeMatSaveProcess(calibCameraRight.getIntrinsicsMatrix()
				.getNativeObjAddr(), imageDirectoryBase
				+ "CameraMatrixRight.yml", "Camera_Matrix");
		NativeMatSaveProcess(calibCameraRight.getDistCoeffsMatrix()
				.getNativeObjAddr(),
				imageDirectoryBase + "DistCoeffsRight.yml", "Dist_Coeffs");
		NativeMatSaveProcess(eMat.getNativeObjAddr(), imageDirectoryBase
				+ "eMatrix.yml", "E_Matrix");
		NativeMatSaveProcess(fMat.getNativeObjAddr(), imageDirectoryBase
				+ "fMatrix.yml", "F_Matrix");
		NativeMatSaveProcess(rMat.getNativeObjAddr(), imageDirectoryBase
				+ "rMatrix.yml", "R_Matrix");
		NativeMatSaveProcess(tMat.getNativeObjAddr(), imageDirectoryBase
				+ "tMatrix.yml", "T_Matrix");
		NativeMatSaveProcess(rMatLeft.getNativeObjAddr(), imageDirectoryBase
				+ "R1Matrix.yml", "R1_Matrix");
		NativeMatSaveProcess(rMatRight.getNativeObjAddr(), imageDirectoryBase
				+ "R2Matrix.yml", "R2_Matrix");
		NativeMatSaveProcess(pMatLeft.getNativeObjAddr(), imageDirectoryBase
				+ "P1Matrix.yml", "P1_Matrix");
		NativeMatSaveProcess(pMatRight.getNativeObjAddr(), imageDirectoryBase
				+ "P2Matrix.yml", "P2_Matrix");
		NativeMatSaveProcess(QMat.getNativeObjAddr(), imageDirectoryBase
				+ "QMatrix.yml", "Q_Matrix");
		NativeMatSaveProcess(map1Left.getNativeObjAddr(), imageDirectoryBase
				+ "Remap1Left.yml", "Remap1_Matrix");
		NativeMatSaveProcess(map2Left.getNativeObjAddr(), imageDirectoryBase
				+ "Remap2Left.yml", "Remap2_Matrix");
		NativeMatSaveProcess(map1Right.getNativeObjAddr(), imageDirectoryBase
				+ "Remap1Right.yml", "Remap1_Matrix");
		NativeMatSaveProcess(map2Right.getNativeObjAddr(), imageDirectoryBase
				+ "Remap2Right.yml", "Remap2_Matrix");
		NativeMatSaveProcess(Rect2PointMat(validROILeft).getNativeObjAddr(),
				imageDirectoryBase + "ValidROILeft.yml", "ROILeft_Matrix");
		NativeMatSaveProcess(Rect2PointMat(validROIRight).getNativeObjAddr(),
				imageDirectoryBase + "ValidROIRight.yml", "ROIRight_Matrix");
		sendBroadcast(new Intent(
				Intent.ACTION_MEDIA_MOUNTED,
				Uri.parse("file://" + Environment.getExternalStorageDirectory())));
	}

	public MatOfPoint Rect2PointMat(Rect src) {
		MatOfPoint dst;
		dst = new MatOfPoint(src.tl(), src.br());
		Log.e(TAG, "Mat of points rect " + dst.dump());
		return dst;
	}

	// Timer that delays the captures between identified patterns
	public class NextCaptureCount extends CountDownTimer {
		public NextCaptureCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			readyForCapture = true;
			if (verbose)
				Log.e(TAG, "Permission to capture conceded.");
		}

		@Override
		public void onTick(long arg0) {
			// if (verbose)
			// Log.e(TAG,
			// "Time Left for correct capture =" + Long.toString(arg0));
		}
	}

	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode,
			Intent aData) {

		super.onActivityResult(aRequestCode, aResultCode, aData);
		imagePathBase = Environment.getExternalStorageDirectory() + "/DCIM2/"
				+ aData.getStringExtra("pathbase");
		imageNameBase = aData.getStringExtra("namebase");
		imageDirectoryBase = Environment.getExternalStorageDirectory()
				+ "/DCIM2/" + aData.getStringExtra("directorybase");
		imageExtension = aData.getStringExtra("extention");
		numberImages = aData.getIntExtra("numberimages", 0);

		if (numberImages > 0) {
			patternNumberEditText.setText(String.valueOf(numberImages));
			numCapturesTextOk = true;
		}
		Log.e(TAG, "Image path is: " + imagePathBase);
		Log.e(TAG, "Image Extention is: " + imageExtension);
		Log.e(TAG, "Number of Images is: " + numberImages);
	}
}
