package com.ecivision.stereocalibrationlive;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;

public class CalibrationObject extends StereoCalibrationActivity {

	// Type of calibration to be performed either using asymmetric chessboards
	// or an asymmetric circles grid
	public enum CalibType {
		CALIB_CHESSBOARD, CALIB_CIRCLES
	}

	private String TAG = "Calibration::Object";
	static final long delayTimePeriod = 50;
	static final long delayTime = 2000;
	private CalibCount captureDelayCounter;
	private int numberOfCaptures = 0;
	private boolean captureCornersOK = false;
	private boolean isCounterActive = false;
	private double squareSize = 1;// = 50;// Distancia en mm de los circulos
	double rmsError = 0;
	private SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy_MM_dd_HH_mm_ss");
	Date now = new Date();
	// private int cornersCount = 0;

	private Mat tempGrayImage;
	private boolean capturedSignal = false;
	private Mat tempColorImage;
	private Mat intrinsicMat;
	private Mat toSaveImage;
	private List<Mat> rotationVectors;
	private List<Mat> translationVectors;
	private Mat distCoeffsMat;
	private Size patternSize;
	private Size imageSize;
	private boolean saveImages;
	private boolean positiveFound;
	private boolean verbose = false;
	private CalibType calibType = CalibType.CALIB_CIRCLES;
	private boolean isLiveFeed = true;

	private MatOfPoint2f imagePointsMat;
	private MatOfPoint3f objectPointsMat;
	private List<Mat> imagePoints;
	private List<Mat> objectPoints;
	private List<MatOfPoint2f> imagePoints2f = new ArrayList<MatOfPoint2f>();
	private List<MatOfPoint3f> objectPoints3f = new ArrayList<MatOfPoint3f>();

	// Default constructor
	public CalibrationObject(Size _patternSize, Size _imageSize,
			CalibType _calibType, int _squareSize, boolean _saveImages) {
		patternSize = _patternSize;
		imageSize = _imageSize;
		calibType = _calibType;
		captureDelayCounter = new CalibCount(delayTime, delayTimePeriod);
		imagePointsMat = new MatOfPoint2f();
		imagePoints = new ArrayList<Mat>();
		objectPoints = new ArrayList<Mat>();
		rotationVectors = new ArrayList<Mat>();
		translationVectors = new ArrayList<Mat>();
		saveImages = _saveImages;
		squareSize = (double) _squareSize;

	};

	// This method is responsible for finding the patterns and saving the points
	// in a vector and returns the number of captured corners.
	//
	// Points are captured when a pattern is recognized an maintained at least
	// 1.2s in order to give the user enough time to capture the next position
	public int CalibFindCorners(Mat _srcImage) {
		calcPatternObjectCorners();
		tempColorImage = _srcImage;
		if (!isLiveFeed)
			captureCornersOK = true;
		switch (calibType) {
		case CALIB_CHESSBOARD:
			try {
				imagePointsMat.release();
			} catch (Exception e) {

			}

			positiveFound = Calib3d.findChessboardCorners(tempColorImage,
					patternSize, imagePointsMat,
					Calib3d.CALIB_CB_ADAPTIVE_THRESH
							| Calib3d.CALIB_CB_NORMALIZE_IMAGE);

			if (positiveFound) {
				if (tempColorImage.channels() > 1) {
					Imgproc.cvtColor(tempColorImage, tempGrayImage,
							Imgproc.COLOR_BGR2GRAY);
				} else {
					tempGrayImage = tempColorImage;
				}
				Imgproc.cornerSubPix(tempGrayImage, imagePointsMat, new Size(
						11, 11), new Size(-1, -1), new TermCriteria(
						TermCriteria.EPS | TermCriteria.MAX_ITER, 30, 0.01));
				if (captureCornersOK) {
					numberOfCaptures++;
					imagePoints2f.add(new MatOfPoint2f(imagePointsMat));
					imagePoints.add(imagePointsMat.clone());
					objectPoints3f.add(new MatOfPoint3f(objectPointsMat));
					objectPoints.add(objectPointsMat.clone());
					Log.e(TAG, "Saved Chess Corners");
					if (saveImages) {
						Imgproc.cvtColor(tempColorImage, toSaveImage,
								Imgproc.COLOR_RGBA2BGR);
						Highgui.imwrite(
								Environment.getExternalStorageDirectory()
										+ "/DCIM/calib_chess_"
										+ formatter.format(now)
										+ String.valueOf(imagePoints2f.size())
										+ ".png", toSaveImage);
					}
					if (isLiveFeed) {
						Core.bitwise_not(tempColorImage, tempColorImage);
					}
					Log.e(TAG, "Captured chessboard corners location.");

					if (isLiveFeed) {
						captureCornersOK = false;
						isCounterActive = false;
					}
					// cornersCount++;
					Log.e(TAG,
							"Capturing corners #: "
									+ String.valueOf(imagePoints2f.size()));
					Calib3d.drawChessboardCorners(tempColorImage, patternSize,
							imagePointsMat, positiveFound);
				}

				if (!isCounterActive && isLiveFeed) {
					captureDelayCounter.start();
					isCounterActive = true;
				}
			} else {
				if (isCounterActive && isLiveFeed) {
					captureDelayCounter.cancel();
					captureCornersOK = false;
					isCounterActive = false;
				}
			}
			break;

		case CALIB_CIRCLES:
			if (tempColorImage.channels() > 1) {
				Imgproc.cvtColor(tempColorImage, tempGrayImage,
						Imgproc.COLOR_BGR2GRAY);
			} else {
				tempGrayImage = tempColorImage;
			}
			try {
				imagePointsMat.release();
			} catch (Exception e) {

			}
			positiveFound = Calib3d.findCirclesGridDefault(tempGrayImage,
					patternSize, imagePointsMat,
					Calib3d.CALIB_CB_ASYMMETRIC_GRID);
			if (!positiveFound)
				positiveFound = Calib3d.findCirclesGridDefault(tempGrayImage,
						patternSize, imagePointsMat,
						Calib3d.CALIB_CB_ASYMMETRIC_GRID
								| Calib3d.CALIB_CB_CLUSTERING);
			if (positiveFound) {
				if (verbose)
					Log.e(TAG, "Circles Pattern Found");
				if (captureCornersOK) {
					numberOfCaptures++;
					imagePoints2f.add(new MatOfPoint2f(imagePointsMat));
					imagePoints.add(imagePointsMat.clone());
					objectPoints3f.add(new MatOfPoint3f(objectPointsMat));
					objectPoints.add(objectPointsMat.clone());
					if (saveImages) {
						Imgproc.cvtColor(tempColorImage, toSaveImage,
								Imgproc.COLOR_RGBA2BGR);
						Highgui.imwrite(
								Environment.getExternalStorageDirectory()
										+ "/DCIM/Camera/calib_chess_"
										+ formatter.format(now)
										+ String.valueOf(imagePoints2f.size())
										+ ".png", toSaveImage);
					}
					if (isLiveFeed) {
						Core.bitwise_not(tempColorImage, tempColorImage);
					}
					if (verbose)
						Log.e(TAG, "Captured circles location.");

					if (isLiveFeed) {
						captureCornersOK = false;
						isCounterActive = false;
					}
					if (verbose)
						Log.e(TAG,
								"Capturing corners #: "
										+ String.valueOf(imagePoints2f.size()));
					capturedSignal = true;
				}

				Calib3d.drawChessboardCorners(tempColorImage, patternSize,
						imagePointsMat, positiveFound);

				if (!isCounterActive && isLiveFeed) {
					captureDelayCounter.start();
					isCounterActive = true;
				}

			} else {
				if (isCounterActive && isLiveFeed) {
					captureDelayCounter.cancel();
					captureCornersOK = false;
					isCounterActive = false;
				}
			}
			break;
		}
		if (capturedSignal) {
			capturedSignal = false;
			return numberOfCaptures;
		} else {
			return -1;
		}
	}

	public Mat returnCornersImage() {
		return tempColorImage;
	}

	public void allocateImages() {
		tempGrayImage = new Mat();
		tempColorImage = new Mat();
		toSaveImage = new Mat();
		intrinsicMat = Mat.zeros(3, 3, CvType.CV_32FC1);
		distCoeffsMat = Mat.zeros(1, 5, CvType.CV_32FC1);
	}

	private void calcPatternObjectCorners() {
		List<Point3> calculatedCornersList = new ArrayList<Point3>();
		switch (calibType) {
		case CALIB_CHESSBOARD:
			for (int i = 0; i < patternSize.height; i++)
				for (int j = 0; j < patternSize.width; j++) {
					calculatedCornersList.add(new Point3(
							(double) (j * squareSize),
							(double) (i * squareSize), 0));
				}
			break;
		case CALIB_CIRCLES:
			for (int i = 0; i < patternSize.height; i++)
				for (int j = 0; j < patternSize.width; j++) {
					calculatedCornersList.add(new Point3(
							(double) ((2 * j + i % 2) * squareSize),
							(double) (i * squareSize), 0));
				}
			break;
		}
		objectPointsMat = new MatOfPoint3f();
		objectPointsMat.fromList(calculatedCornersList);
	}

	public double RunCalibration() {
		if (objectPoints3f.size() < 3) {
			Log.e(TAG, "Not enough captured patterns to run calibration");
		} else {
			try {
				// For the calibration to work, objecPoints and imagePoints must
				// be the same size,
				// also objectPoints(n) and imagePoints(n) must be the same
				// size.
				double error = 0;
				error = Calib3d.calibrateCamera(objectPoints, imagePoints,
						imageSize, intrinsicMat, distCoeffsMat,
						rotationVectors, translationVectors,
						// Calib3d.CALIB_ZERO_TANGENT_DIST
						// + Calib3d.CALIB_SAME_FOCAL_LENGTH
						// + Calib3d.CALIB_FIX_ASPECT_RATIO
						// + Calib3d.CALIB_FIX_PRINCIPAL_POINT //NEW
						Calib3d.CALIB_RATIONAL_MODEL +
						// + Calib3d.CALIB_FIX_K3 + Calib3d.CALIB_FIX_K4
								Calib3d.CALIB_FIX_K5);
				Log.e(TAG, intrinsicMat.dump());
				Log.e(TAG, distCoeffsMat.dump());
				Log.e(TAG, "Calibrated Correctly");
				return error;
			} catch (Exception e) {
				Log.e(TAG, e.toString());
				Log.e(TAG, "Unable to calibrate");
				return -1;
			}
		}
		return -1;
	}

	// Getters for the important matrixes and parameters of calibration
	public Mat getIntrinsicsMatrix() {
		return intrinsicMat;
	}

	public Mat getIntrinsicsMatrixIdentity() {
		intrinsicMat = Mat.eye(3, 3, CvType.CV_64FC1);
		return intrinsicMat;
	}

	public Mat getDistCoeffsMatrix() {
		return distCoeffsMat;
	}

	public Mat getDistCoeffsMatrixZeros() {
		distCoeffsMat = Mat.zeros(1, 5, CvType.CV_64FC1);
		return distCoeffsMat;
	}

	public Mat getObjectMat() {
		return objectPointsMat;
	}

	public List<Mat> getObjectPoints() {
		return objectPoints;
	}

	public List<Mat> getImagePoints() {
		return imagePoints;
	}

	public List<MatOfPoint3f> getObjectPoints3f() {
		return objectPoints3f;
	}

	public List<MatOfPoint2f> getImagePoints2f() {
		return imagePoints2f;
	}

	public Mat getRotMat() {
		Mat tempMat = new Mat(rotationVectors.size(), rotationVectors.get(0)
				.height(), rotationVectors.get(0).type());
		;
		for (int i = 0; i < rotationVectors.size(); i++) {
			for (int j = 0; j < rotationVectors.get(0).height(); j++) {
				tempMat.put(i, j, rotationVectors.get(i).get(j, 0));
			}
		}
		return tempMat;
	}

	public Mat getTranslationMat() {
		Mat tempMat = new Mat(translationVectors.size(), translationVectors
				.get(0).height(), translationVectors.get(0).type());
		for (int i = 0; i < translationVectors.size(); i++) {
			for (int j = 0; j < translationVectors.get(0).height(); j++) {
				tempMat.put(i, j, translationVectors.get(i).get(j, 0));
			}
		}
		return tempMat;
	}

	public Mat getImagePointsMat() {
		Mat tempMat = new Mat(imagePoints.size(), imagePoints.get(0).height(),
				CvType.CV_32FC2);

		for (int i = 0; i < imagePoints.size(); i++) {
			for (int j = 0; j < imagePoints.get(0).height(); j++) {
				tempMat.put(i, j, imagePoints.get(i).get(j, 0));
			}
		}

		return tempMat;
	}

	public double getSquareSizeMM() {
		return squareSize;
	}

	public void dumpMat(Mat matrix) {
		Log.e(TAG, matrix.dump());
	}

	// Releases all matrixes
	public void close() {
		try {
			intrinsicMat.release();
			rotationVectors.clear();
			translationVectors.clear();
			distCoeffsMat.release();
			saveImages = false;
			positiveFound = false;
			imagePointsMat.release();
			objectPointsMat.release();
			imagePoints.clear();
			objectPoints.clear();
			imagePoints2f.clear();
			objectPoints3f.clear();
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	// Second Calculation of reprojection errors besides the value provided by
	// the calibrate function
	public double computeReprojectionErrors() {
		MatOfPoint2f imagePointsMat2;
		double error = 0, totalError = 0;
		int totalPoints = 0;
		MatOfDouble distCoeffsMatDouble = new MatOfDouble(distCoeffsMat);

		for (int i = 0; i < objectPoints3f.size(); i++) {
			imagePointsMat2 = new MatOfPoint2f();
			Calib3d.projectPoints(objectPoints3f.get(i),
					rotationVectors.get(i), translationVectors.get(i),
					intrinsicMat, distCoeffsMatDouble, imagePointsMat2);
			error = Core.norm(imagePoints2f.get(i), imagePointsMat2,
					Core.NORM_L2);
			int n = (int) (objectPoints3f.get(i).width() * objectPoints3f
					.get(i).height());
			totalError += error * error;
			totalPoints += n;
			imagePointsMat2.release();
		}

		Log.e(TAG, "Object points size: " + String.valueOf(totalPoints));
		Log.e(TAG, "Total Error: " + String.valueOf(totalError));
		return Math.sqrt(totalError / (double) totalPoints);
	}

	public void setTag(String _TAG) {
		TAG = _TAG;
	}

	// Timer that delays the captures between identified patterns
	@SuppressLint("SimpleDateFormat")
	public class CalibCount extends CountDownTimer {
		public CalibCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			captureCornersOK = true;
			if (verbose)
				Log.e(TAG, "Permission to capture conceded.");
		}

		@Override
		public void onTick(long arg0) {
			if (verbose)
				Log.e(TAG,
						"Time Left for correct capture =" + Long.toString(arg0));
		}

	}

	// Clear the captured points of the pattern at index idx
	public void removeImagePoints(int idx) {
		imagePoints2f.remove(idx);
		imagePoints.remove(idx);
		objectPoints3f.remove(idx);
		objectPoints.remove(idx);
	}

	public void removeLastImagePoints() {
		imagePoints2f.remove(imagePoints2f.size() - 1);
		imagePoints.remove(imagePoints.size() - 1);
		objectPoints3f.remove(objectPoints3f.size() - 1);
		objectPoints.remove(objectPoints.size() - 1);
		numberOfCaptures--;
	}

}