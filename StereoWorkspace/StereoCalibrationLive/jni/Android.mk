LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES:=on
include $(OPENCV_ANDROID_SDK)/sdk/native/jni/OpenCV.mk
LOCAL_MODULE    := ecivision-calibration-live
LOCAL_SRC_FILES := com_ecivision_stereocalibrationlive_StereoCalibrationActivity.cpp ExtraNAtiveUtils.cpp
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)