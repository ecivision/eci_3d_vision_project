#include "com_ecivision_stereocalibrationlive_StereoCalibrationActivity.h"

#include <opencv2/opencv.hpp>
#include <android/log.h>
#include "ExtraNativeUtils.h"
#include <stdio.h>

using namespace cv;
using namespace std;



JNIEXPORT void JNICALL Java_com_ecivision_stereocalibrationlive_StereoCalibrationActivity_NativeMatSaveProcess(
		JNIEnv * env, jobject object, jlong addrMat, jstring fileName,
		jstring matName) {
	cv::Mat& savedMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName, NULL);
	const char *_matName = env->GetStringUTFChars(matName, NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName, cv::FileStorage::WRITE);
	try {
		matrixFile << _matName << savedMat;
		matrixFile.release();
		__android_log_write(ANDROID_LOG_ERROR, "Saved", _fileName);
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
	} catch (int e) {
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName, _fileName);
		env->ReleaseStringUTFChars(matName, _matName);
		__android_log_write(ANDROID_LOG_ERROR, "Ecivision-Stereo",
				"Error saving Mat.");
	}
}
