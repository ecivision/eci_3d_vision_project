// Do not remove the include below

#include <Adb.h>
#include <Arduino.h>
#include <HardwareSerial.h>
#include <pins_arduino.h>
#include <Print.h>
#include <string.h>
//#include <SPI.h>
#include <stdint.h>
//#include "USB_Client_Sketch.h"

long lastTime;
Connection *connection;

void adbEventHandler(Connection * connection, adb_eventType event,
		uint16_t length, uint8_t * data) {
	uint16_t i;

	// Data packets contain two bytes, one for each servo, in the range of [0..180]
	if (event == ADB_CONNECTION_RECEIVE) {
		for (i = 0; i < length; i++) {
			Serial.print(data[0],HEX);
			Serial.print(" ");
			Serial.print(data[1],HEX);
			Serial.print("\n");
		}
	}
}

void setup() {

	// Initialise serial port
	Serial.begin(57600);

	//Save Start Time
	lastTime = millis();

	//Pinmode
	pinMode(LED_BUILTIN, OUTPUT);

	// Initialise the ADB subsystem.
	ADB::init();

	// Open an ADB stream to the phone's shell. Auto-reconnect
	connection = ADB::addConnection("tcp:4567", true, adbEventHandler);

}

void loop() {

	if ((millis() - lastTime) > 20) {
		lastTime = millis();
		char message []="OK!\n";
		connection->writeString(message);
	}
	// Poll the ADB subsystem.
	ADB::poll();
}
