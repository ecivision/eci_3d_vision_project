package com.ecivision.calibrationsample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;

public class CalibrationObject extends CalibrationActivity {

	// Type of calibration to be performed either using asymmetric chessboards
	// or an asymmetric circles grid
	public enum CalibType {
		CALIB_CHESSBOARD, CALIB_CIRCLES
	}

	private static final String TAG = "Calibration::Object";
	static final long delayTimePeriod = 50;
	static final long delayTime = 2400;
	private CalibCount captureDelayCounter;
	private boolean captureCornersOK = false;
	private boolean isCounterActive = false;
	private double squareSize = 1;
	double rmsError = 0;
	private SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy_MM_dd_HH_mm_ss");
	Date now = new Date();
	// private int cornersCount = 0;

	private Mat tempGrayImage;
	private Mat tempColorImage;
	private Mat intrinsicMat;
	private Mat toSaveImage;
	private List<Mat> rotationVectors;
	private List<Mat> translationVectors;
	private Mat distCoeffsMat;
	private Size patternSize;
	private boolean saveImages;
	private boolean positiveFound;
	private CalibType calibType = CalibType.CALIB_CIRCLES;

	private MatOfPoint2f imagePointsMat;
	private MatOfPoint3f objectPointsMat;
	private List<Mat> imagePoints;
	private List<Mat> objectPoints;
	private List<MatOfPoint2f> imagePointsTemp = new ArrayList<MatOfPoint2f>();
	private List<MatOfPoint3f> objectPointsTemp = new ArrayList<MatOfPoint3f>();

	// Default constructor
	public CalibrationObject(Size _patternSize, CalibType _calibType,
			boolean _saveImages) {
		patternSize = _patternSize;
		calibType = _calibType;
		captureDelayCounter = new CalibCount(delayTime, delayTimePeriod);
		imagePointsMat = new MatOfPoint2f();
		imagePoints = new ArrayList<Mat>();
		objectPoints = new ArrayList<Mat>();
		rotationVectors = new ArrayList<Mat>();
		translationVectors = new ArrayList<Mat>();
		saveImages = _saveImages;
	};

	// This method is responsible for finding the patterns and saving the points
	// in a vector and returns the number of captured corners.
	//
	// Points are captured when a pattern is recognized an maintained at least
	// 1.2s in order to give the user enough time to capture the next position
	public int CalibProcess(Mat _srcImage) {
		calcChessBoardCorners();
		tempColorImage = _srcImage;
		switch (calibType) {
		case CALIB_CHESSBOARD:
			positiveFound = Calib3d.findChessboardCorners(tempColorImage,
					patternSize, imagePointsMat,
					Calib3d.CALIB_CB_ADAPTIVE_THRESH
							| Calib3d.CALIB_CB_FAST_CHECK
							| Calib3d.CALIB_CB_NORMALIZE_IMAGE);
			if (positiveFound) {
				Imgproc.cvtColor(tempColorImage, tempGrayImage,
						Imgproc.COLOR_BGR2GRAY);
				Imgproc.cornerSubPix(tempGrayImage, imagePointsMat, new Size(
						11, 11), new Size(-1, -1), new TermCriteria(
						TermCriteria.EPS | TermCriteria.MAX_ITER, 30, 0.1));
				if (captureCornersOK) {
					imagePointsTemp.add(imagePointsMat);
					imagePoints.add((Mat) imagePointsMat);
					objectPointsTemp.add(objectPointsMat);
					objectPoints.add((Mat) objectPointsMat);
					imagePointsMat.release();
					if (saveImages) {
						Imgproc.cvtColor(tempColorImage, toSaveImage,
								Imgproc.COLOR_RGBA2BGR);
						Highgui.imwrite(
								Environment.getExternalStorageDirectory()
										+ "/DCIM/calib_chess_"
										+ formatter.format(now)
										+ String.valueOf(imagePointsTemp.size())
										+ ".png", toSaveImage);
					}
					Core.bitwise_not(tempColorImage, tempColorImage);
					Log.d(TAG, "Captured corners circles location.");
					captureCornersOK = false;
					isCounterActive = false;
					// cornersCount++;
					Log.d(TAG,
							"Capturing corners #: "
									+ String.valueOf(imagePointsTemp.size()));
				}

				if (!isCounterActive) {
					captureDelayCounter.start();
					isCounterActive = true;
				}

				Log.d(TAG,
						"Found correct corners and calculated subpixel accuracy.");
				Calib3d.drawChessboardCorners(tempColorImage, patternSize,
						imagePointsMat, positiveFound);
			} else {
				if (isCounterActive) {
					captureDelayCounter.cancel();
					captureCornersOK = false;
					isCounterActive = false;
				}
			}
			break;

		case CALIB_CIRCLES:
			Imgproc.cvtColor(tempColorImage, tempGrayImage,
					Imgproc.COLOR_BGR2GRAY);
			try {
				imagePointsMat.release();
			} catch (Exception e) {

			}
			positiveFound = Calib3d.findCirclesGridDefault(tempGrayImage,
					patternSize, imagePointsMat,
					Calib3d.CALIB_CB_ASYMMETRIC_GRID
							| Calib3d.CALIB_CB_CLUSTERING);
			if (positiveFound) {
				if (captureCornersOK) {
					imagePointsTemp.add(new MatOfPoint2f(imagePointsMat));
					imagePoints.add(imagePointsMat.clone());
					objectPointsTemp.add(new MatOfPoint3f(objectPointsMat));
					objectPoints.add(objectPointsMat.clone());
					if (saveImages) {
						Imgproc.cvtColor(tempColorImage, toSaveImage,
								Imgproc.COLOR_RGBA2BGR);
						Highgui.imwrite(
								Environment.getExternalStorageDirectory()
										+ "/DCIM/Camera/CalibCirclesTest_"
										+ formatter.format(now)
										+ String.valueOf(imagePointsTemp.size())
										+ ".png", toSaveImage);
					}
					Core.bitwise_not(tempColorImage, tempColorImage);
					Log.d(TAG, "Captured circles location.");
					captureCornersOK = false;
					isCounterActive = false;
					// cornersCount++;
					Log.d(TAG,
							"Capturing corners #: "
									+ String.valueOf(imagePointsTemp.size()));
				}

				Calib3d.drawChessboardCorners(tempColorImage, patternSize,
						imagePointsMat, positiveFound);

				if (!isCounterActive) {
					captureDelayCounter.start();
					isCounterActive = true;
				}

				Log.d(TAG, "Found correct circles location.");

			} else {
				if (isCounterActive) {
					captureDelayCounter.cancel();
					captureCornersOK = false;
					isCounterActive = false;
				}
			}
			break;
		}
		return imagePointsTemp.size();
	}

	public Mat returnCornersImage() {
		return tempColorImage;
	}

	public void allocateImages(int width, int height) {
		tempGrayImage = new Mat(height, width, CvType.CV_8UC1);
		tempColorImage = new Mat(height, width, CvType.CV_8UC4);
		toSaveImage = new Mat(height, width, CvType.CV_8UC3);
		intrinsicMat = new Mat(3, 3, CvType.CV_32FC1);
		distCoeffsMat = new Mat(1, 5, CvType.CV_32FC1);
	}

	private void calcChessBoardCorners() {
		List<Point3> calculatedCornersList = new ArrayList<Point3>();
		switch (calibType) {
		case CALIB_CHESSBOARD:
			for (int i = 0; i < patternSize.height; i++)
				for (int j = 0; j < patternSize.width; j++) {
					calculatedCornersList.add(new Point3(
							(double) (j * squareSize),
							(double) (i * squareSize), 0));
				}
			break;
		case CALIB_CIRCLES:
			for (int i = 0; i < patternSize.height; i++)
				for (int j = 0; j < patternSize.width; j++) {
					calculatedCornersList.add(new Point3(
							(double) ((2 * j + i % 2) * squareSize),
							(double) (i * squareSize), 0));
				}
			break;
		}
		objectPointsMat = new MatOfPoint3f();
		objectPointsMat.fromList(calculatedCornersList);
	}

	public double RunCalibration() {
		if (objectPointsTemp.size() < 3) {
			Log.e(TAG, "Not enough captured patterns to run calibration");
		} else {
			try {
				// For the calibration to work, objecPoints and imagePoints must
				// be the same size,
				// also objectPoints(n) and imagePoints(n) must be the same
				// size.
				double error = 0;
				error = Calib3d.calibrateCamera(
						objectPoints,
						imagePoints,
						new Size(tempColorImage.width(), tempColorImage
								.height()), intrinsicMat, distCoeffsMat,
						rotationVectors, translationVectors);
				Log.e(TAG, intrinsicMat.dump());
				Log.e(TAG, objectPointsMat.dump());
				Log.e(TAG, "Calibrated Correctly");
				return error;
			} catch (Exception e) {
				Log.e(TAG, e.toString());
				Log.e(TAG, "Unable to calibrate");
				return -1;
			}
		}
		return -1;
	}

	public Mat getIntrinsicsMatrix() {
		return intrinsicMat;
	}

	public Mat getDistCoeffsMatrix() {
		return distCoeffsMat;
	}

	public Mat getObjectMat() {
		return objectPointsMat;
	}

	public Mat getRotMat() {
		Log.e(TAG, String.valueOf(rotationVectors.size()));
		Log.e(TAG, String.valueOf(rotationVectors.get(0).height()));
		Mat tempMat = new Mat(rotationVectors.size(), rotationVectors.get(0)
				.height(), rotationVectors.get(0).type());
		for (int i = 0; i < rotationVectors.size(); i++) {
			Log.e(TAG, String.valueOf(rotationVectors.get(i).dump()));
		}
		for (int i = 0; i < rotationVectors.size(); i++) {
			for (int j = 0; j < rotationVectors.get(0).height(); j++) {
				tempMat.put(i, j, rotationVectors.get(i).get(j, 0));
			}
		}
		return tempMat;
	}

	public Mat getTranslationMat() {
		Log.e(TAG, String.valueOf(translationVectors.size()));
		Log.e(TAG, String.valueOf(translationVectors.get(0).height()));
		Mat tempMat = new Mat(translationVectors.size(), translationVectors
				.get(0).height(), translationVectors.get(0).type());
		for (int i = 0; i < translationVectors.size(); i++) {
			Log.e(TAG, String.valueOf(translationVectors.get(i).dump()));
		}
		for (int i = 0; i < translationVectors.size(); i++) {
			for (int j = 0; j < translationVectors.get(0).height(); j++) {
				tempMat.put(i, j, translationVectors.get(i).get(j, 0));
			}
		}
		return tempMat;
	}

	public void dumpMat(Mat matrix) {
		Log.i(TAG, matrix.dump());
	}

	public void close() {
		try {
			intrinsicMat.release();
			rotationVectors.clear();
			translationVectors.clear();
			distCoeffsMat.release();
			saveImages = false;
			positiveFound = false;
			imagePointsMat.release();
			objectPointsMat.release();
			imagePoints.clear();
			objectPoints.clear();
			imagePointsTemp.clear();
			objectPointsTemp.clear();
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	public double computeReprojectionErrors() {
		MatOfPoint2f imagePointsMat2 = new MatOfPoint2f();
		double error = 0, totalError = 0;
		int totalPoints = 0;
		MatOfDouble distCoeffsMatDouble = new MatOfDouble(distCoeffsMat);
		for (int i = 0; i < objectPointsTemp.size(); i++) {
			Calib3d.projectPoints(objectPointsTemp.get(i),
					rotationVectors.get(i), translationVectors.get(i),
					intrinsicMat, distCoeffsMatDouble, imagePointsMat2);
			Log.e(TAG, "Reprojected:\n" + imagePointsMat2.dump());
			Log.e(TAG, "Captured:\n" + imagePoints.get(i).dump());
			error = Core
					.norm(imagePoints.get(i), imagePointsMat2, Core.NORM_L2);
			int n = (int) (objectPointsTemp.get(i).width() * objectPointsTemp
					.get(i).height());
			totalError += error * error;
			totalPoints += n;
			Log.e(TAG, "Object points size: " + String.valueOf(n));
			Log.e(TAG, "Object points total: " + String.valueOf(totalPoints));
			Log.e(TAG, "Total Error: " + String.valueOf(totalError));
		}
		return Math.sqrt(totalError / (double) totalPoints);
	}

	public class CalibCount extends CountDownTimer {
		public CalibCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			captureCornersOK = true;
			Log.d(TAG, "Permission to capture image conceded.");
		}

		@Override
		public void onTick(long arg0) {
			Log.d(TAG, "Time Left for correct capture =" + Long.toString(arg0));
		}

	}

}