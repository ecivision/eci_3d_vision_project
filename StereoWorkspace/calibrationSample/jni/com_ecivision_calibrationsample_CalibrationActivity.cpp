#include "com_ecivision_calibrationsample_CalibrationActivity.h"
#include <android/log.h>
#include <opencv2/opencv.hpp>
#include <string.h>
#include <stdint.h>
#include <exception>
//FIX
using namespace cv;

JNIEXPORT void JNICALL Java_com_ecivision_calibrationsample_CalibrationActivity_NativeMatSaveProcess
  (JNIEnv * env, jobject object, jlong addrMat, jstring fileName, jstring matName){
	cv::Mat& savedMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName,NULL);
	const char *_matName = env->GetStringUTFChars(matName,NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName,cv::FileStorage::WRITE);
	try{
	matrixFile << _matName << savedMat;
	matrixFile.release();
	__android_log_write(ANDROID_LOG_ERROR, "Saved",_fileName);
	env->ReleaseStringUTFChars(fileName,_fileName);
	env->ReleaseStringUTFChars(matName,_matName);
	}
	catch(int e){
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName,_fileName);
		env->ReleaseStringUTFChars(matName,_matName);
	__android_log_write(ANDROID_LOG_ERROR, "Ecivision-calibration","Error saving Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_calibrationsample_CalibrationActivity_NativeMatLoadProcess
  (JNIEnv * env, jobject object, jlong addrMat, jstring fileName, jstring matName){
	cv::Mat& toLoadMat = *(cv::Mat*) addrMat;
	const char *_fileName = env->GetStringUTFChars(fileName,NULL);
	const char *_matName = env->GetStringUTFChars(matName,NULL);
	cv::FileStorage matrixFile;
	matrixFile = cv::FileStorage(_fileName,cv::FileStorage::READ);
	try{
	matrixFile[_matName] >> toLoadMat;
	matrixFile.release();
	__android_log_write(ANDROID_LOG_ERROR, "Loaded",_fileName);
	env->ReleaseStringUTFChars(fileName,_fileName);
	env->ReleaseStringUTFChars(matName,_matName);
	}
	catch(int e){
		matrixFile.release();
		env->ReleaseStringUTFChars(fileName,_fileName);
		env->ReleaseStringUTFChars(matName,_matName);
	__android_log_write(ANDROID_LOG_ERROR, "Ecivision-calibration","Error loading Mat.");
	}
}

JNIEXPORT void JNICALL Java_com_ecivision_calibrationsample_CalibrationActivity_NativeUndistortProcess
  (JNIEnv * env, jobject object, jlong addrSrc,jlong addrDst, jlong addrIntr, jlong addrDist){
	cv::Mat& imgSrc = *(cv::Mat*) addrSrc;
	cv::Mat& imgDst = *(cv::Mat*) addrDst;
	cv::Mat& intrMat = *(cv::Mat*) addrIntr;
	cv::Mat& distMat = *(cv::Mat*) addrDist;
	cv::undistort(imgSrc,imgDst,intrMat,distMat);
}

