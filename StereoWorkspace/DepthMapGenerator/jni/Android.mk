LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES:=on
include $(OPENCV_ANDROID_SDK)/sdk/native/jni/OpenCV.mk
LOCAL_MODULE    := ecivision-depth-generator
LOCAL_SRC_FILES := com_ecivision_depthmapgenerator_DepthGeneratorActivity.cpp ExtraNAtiveUtils.cpp
LOCAL_LDLIBS +=  -llog -ldl
include $(BUILD_SHARED_LIBRARY)