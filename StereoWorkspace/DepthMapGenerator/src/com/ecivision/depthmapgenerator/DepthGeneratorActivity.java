package com.ecivision.depthmapgenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DepthGeneratorActivity extends Activity {

	private static final String TAG = "Ecivision::DepthGen::Activity";
	private static final int REMAP_LEFT_OK = 0;
	private static final int REMAP_RIGHT_OK = 1;
	private int currentDisplayImageNumber = 1;
	private int numberImages = 96;
	// GUI elements BM
	private ImageView image_view_disparity;

	private TextView numberOfDisparitiesText;
	private SeekBar numberOfDisparitiesSeek;

	private TextView minDisparityText;
	private SeekBar minDisparitySeek;

	private TextView SADWindowSizeText;
	private SeekBar SADWindowSizeSeek;
	private String SADWindowSizeString;

	private TextView textureThreshText;
	private SeekBar textureThreshSeek;

	private TextView uniquenessRatioText;
	private SeekBar uniquenessRatioSeek;

	private TextView MaxDisparityCheckDiffText;
	private SeekBar MaxDisparityCheckDiffSeek;

	private TextView speckleWindowText;
	private SeekBar speckleWindowSeek;

	private TextView speckleRangeText;
	private SeekBar speckleRangeSeek;

	private TextView sgbmSmoothP1Text;
	private SeekBar sgbmSmoothP1Seek;

	private TextView sgbmsmoothP2Text;
	private SeekBar sgbmsmoothP2Seek;

	private TextView prefilterCapText;
	private SeekBar prefilterCapSeek;
	private MySeekBarListener mSeekBarListener;

	// Other UI elements
	private Button computeButton;
	private Button nextImageButton;
	private Button prevImageButton;
	private Button saveImageButton;
	private Spinner stereoMethodSpinner;
	private ArrayAdapter<String> stereoMethodAdapter;
	private EditText numberImagesEditText;
	private TextView pixValEditTextView;
	private CheckBox colorMapCheckBox;

	private enum StereoMethod {
		BM, SGBM, VAR
	}

	private List<String> stereoMethodList = new ArrayList<String>();
	private StereoMethod stereoMethod = StereoMethod.BM;
	private Handler handler;

	private DepthParamsObject ALGParams;
	private DepthParamsObject BMParams, SGBMParams;

	private long timerStart;
	private long timerStop;

	private IntentFilter ifilter;
	private Intent batteryStatus;

	// Objects required for image rectification and disparity map calculation.
	private Mat QMat;
	private Mat imageLeft, imageRight;
	private Mat rectifiedLeft, rectifiedRight;
	private Mat map1Left, map1Right, map2Left, map2Right;
	private Mat validROIRightMat, validROILeftMat;
	private Mat imageDisp, imageDepth, imageDispMapOR;
	private Mat leftUp, rightUp, dispUp, dispLow;
	private Mat leftDown, rightDown, depthUp, depthLow;

	private String imagePathBase;
	private String imageExtension;
	private String imageDirectoryBase;
	private String imageNameBase;

	private Context mContext;

	private boolean matsAllocated = false;
	private boolean remapLeftOK = false;
	private boolean remapRightOK = false;
	private boolean alreadyLoaded = false;
	private boolean doRemap = false;
	private boolean multipleThreads = false;
	private boolean coloredMap = true;

	private int start_battery_level = 0;
	private int end_battery_level = 0;

	Resources mResources;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_depth_generator);
		// Get image set parameters from image selection subsystem
		Intent intent = new Intent(
				"com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity.intent.action.Launch");
		intent.setComponent(ComponentName
				.unflattenFromString("com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity"));
		intent.addCategory("android.intent.category.DEFAULT");
		startActivityForResult(intent, 1);
		pixValEditTextView = (TextView) findViewById(R.id.pixel_val_textview);
		numberImagesEditText = (EditText) findViewById(R.id.number_images);
		
		
		mResources = getResources();
		mContext = this;
		// Setup UI elements
		// TextViews
		SADWindowSizeText = (TextView) findViewById(R.id.param_textview1);
		numberOfDisparitiesText = (TextView) findViewById(R.id.param_textview2);
		minDisparityText = (TextView) findViewById(R.id.param_textview3);
		textureThreshText = (TextView) findViewById(R.id.param_textview4);
		uniquenessRatioText = (TextView) findViewById(R.id.param_textview5);
		speckleWindowText = (TextView) findViewById(R.id.param_textview6);
		speckleRangeText = (TextView) findViewById(R.id.param_textview7);
		MaxDisparityCheckDiffText = (TextView) findViewById(R.id.param_textview8);
		prefilterCapText = (TextView) findViewById(R.id.param_textview9);
		// seekbars
		SADWindowSizeSeek = (SeekBar) findViewById(R.id.param_seekbar1);
		numberOfDisparitiesSeek = (SeekBar) findViewById(R.id.param_seekbar2);
		minDisparitySeek = (SeekBar) findViewById(R.id.param_seekbar3);
		textureThreshSeek = (SeekBar) findViewById(R.id.param_seekbar4);
		uniquenessRatioSeek = (SeekBar) findViewById(R.id.param_seekbar5);
		speckleWindowSeek = (SeekBar) findViewById(R.id.param_seekbar6);
		speckleRangeSeek = (SeekBar) findViewById(R.id.param_seekbar7);
		MaxDisparityCheckDiffSeek = (SeekBar) findViewById(R.id.param_seekbar8);
		prefilterCapSeek = (SeekBar) findViewById(R.id.param_seekbar9);
		
		BMParams = new DepthParamsObject();
		SGBMParams = new DepthParamsObject();
		ALGParams = new DepthParamsObject();
		ALGParams = BMParams;
		
		setupParamsScrollBars(BMParams);

		// CheckBox
		colorMapCheckBox = (CheckBox) findViewById(R.id.multi_check);
		colorMapCheckBox.setChecked(true);
		// Setting seekbar change listener
		mSeekBarListener = new MySeekBarListener();
		SADWindowSizeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		numberOfDisparitiesSeek.setOnSeekBarChangeListener(mSeekBarListener);
		minDisparitySeek.setOnSeekBarChangeListener(mSeekBarListener);
		textureThreshSeek.setOnSeekBarChangeListener(mSeekBarListener);
		uniquenessRatioSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleWindowSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleRangeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		MaxDisparityCheckDiffSeek.setOnSeekBarChangeListener(mSeekBarListener);
		prefilterCapSeek.setOnSeekBarChangeListener(mSeekBarListener);
		// Setting up the seekbars initial positions
		deactivateSeekbars();
		
		// Buttons
		computeButton = (Button) findViewById(R.id.compute_button);
		nextImageButton = (Button) findViewById(R.id.image_next_button);
		prevImageButton = (Button) findViewById(R.id.image_prev_button);
		saveImageButton = (Button) findViewById(R.id.image_save_button);

		// Spinner
		stereoMethodList.add("BM");
		stereoMethodList.add("SGBM");
		stereoMethodSpinner = (Spinner) findViewById(R.id.stereo_method_spinner);
		stereoMethodAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stereoMethodList);
		stereoMethodAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		stereoMethodSpinner.setAdapter(stereoMethodAdapter);

		stereoMethodSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							stereoMethod = StereoMethod.BM;
							ALGParams = BMParams;
							setupParamsScrollBars(BMParams);
						} else {
							if (id == 1) {
								stereoMethod = StereoMethod.SGBM;
								ALGParams = SGBMParams;
								setupParamsScrollBars(SGBMParams);
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		colorMapCheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						coloredMap = isChecked;
						doComputation();
						// multipleThreads = isChecked;
					}
				});

		// ImageView
		image_view_disparity = (ImageView) findViewById(R.id.image_view_disp);

		// Battery status intent
		ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		batteryStatus = mContext.registerReceiver(null, ifilter);
		// thread message handler

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case REMAP_LEFT_OK:
					// if(remapRightOK){
					// doComputation();
					// }
					break;
				case REMAP_RIGHT_OK:
					if (remapLeftOK) {
						if (multipleThreads)
							doComputation2();
						else
							doComputation();
					}
					break;
				}
			}
		};

		// Compute Button
		computeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!alreadyLoaded) {
					activateSeekbars();
					imageLeft = Highgui.imread(imagePathBase
							+ String.valueOf(01) + "_L" + imageExtension);
					imageRight = Highgui.imread(imagePathBase
							+ String.valueOf(01) + "_R" + imageExtension);
					alreadyLoaded = true;
				}
				timerStart = android.os.SystemClock.uptimeMillis();
				start_battery_level = batteryStatus.getIntExtra(
						BatteryManager.EXTRA_LEVEL, -1);
				if (doRemap) {
					new Thread(new Runnable() {
						public void run() {
							Imgproc.remap(imageLeft, rectifiedLeft, map1Left,
									map2Left, Imgproc.INTER_LINEAR);
							Message msg = Message.obtain();
							msg.what = REMAP_LEFT_OK;
							remapLeftOK = true;
							handler.sendMessage(msg);
						}
					}).start();
					new Thread(new Runnable() {
						public void run() {
							Imgproc.remap(imageRight, rectifiedRight,
									map1Right, map2Right, Imgproc.INTER_LINEAR);
							Message msg = Message.obtain();
							msg.what = REMAP_RIGHT_OK;
							remapRightOK = true;
							handler.sendMessage(msg);
						}
					}).start();
				} else {
					if (multipleThreads)
						doComputation2();
					else
						doComputation();
				}
			}
		});

		nextImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentDisplayImageNumber <= numberImages) {
					++currentDisplayImageNumber;
					imageLeft = Highgui.imread(imagePathBase
							+ String.valueOf(currentDisplayImageNumber) + "_L"
							+ imageExtension);
					imageRight = Highgui.imread(imagePathBase
							+ String.valueOf(currentDisplayImageNumber) + "_R"
							+ imageExtension);
					if (imageLeft.empty() | imageRight.empty()) {
						Toast.makeText(
								mContext,
								"Image #"
										+ String.valueOf(currentDisplayImageNumber)
										+ " not found", Toast.LENGTH_SHORT)
								.show();
					} else {
						timerStart = android.os.SystemClock.uptimeMillis();
						start_battery_level = batteryStatus.getIntExtra(
								BatteryManager.EXTRA_LEVEL, -1);
						if (doRemap) {
							new Thread(new Runnable() {
								public void run() {
									Imgproc.remap(imageLeft, rectifiedLeft,
											map1Left, map2Left,
											Imgproc.INTER_LINEAR);
									Message msg = Message.obtain();
									msg.what = REMAP_LEFT_OK;
									remapLeftOK = true;
									handler.sendMessage(msg);
								}
							}).start();
							new Thread(new Runnable() {
								public void run() {
									Imgproc.remap(imageRight, rectifiedRight,
											map1Right, map2Right,
											Imgproc.INTER_LINEAR);
									Message msg = Message.obtain();
									msg.what = REMAP_RIGHT_OK;
									remapRightOK = true;
									handler.sendMessage(msg);
								}
							}).start();
						} else {
							if (multipleThreads)
								doComputation2();
							else
								doComputation();
						}
					}
				} else {
					currentDisplayImageNumber = 1;
				}
			}
		});

		prevImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentDisplayImageNumber > 1) {
					currentDisplayImageNumber--;
					imageLeft = Highgui.imread(imagePathBase
							+ String.valueOf(currentDisplayImageNumber) + "_L"
							+ imageExtension);
					imageRight = Highgui.imread(imagePathBase
							+ String.valueOf(currentDisplayImageNumber) + "_R"
							+ imageExtension);
					if (imageLeft.empty() | imageRight.empty()) {
						Toast.makeText(
								mContext,
								"Image #"
										+ String.valueOf(currentDisplayImageNumber)
										+ " not found", Toast.LENGTH_SHORT)
								.show();
					} else {
						timerStart = android.os.SystemClock.uptimeMillis();
						start_battery_level = batteryStatus.getIntExtra(
								BatteryManager.EXTRA_LEVEL, -1);
						if (doRemap) {
							new Thread(new Runnable() {
								public void run() {
									Imgproc.remap(imageLeft, rectifiedLeft,
											map1Left, map2Left,
											Imgproc.INTER_LINEAR);
									Message msg = Message.obtain();
									msg.what = REMAP_LEFT_OK;
									remapLeftOK = true;
									handler.sendMessage(msg);
								}
							}).start();
							new Thread(new Runnable() {
								public void run() {
									Imgproc.remap(imageRight, rectifiedRight,
											map1Right, map2Right,
											Imgproc.INTER_LINEAR);
									Message msg = Message.obtain();
									msg.what = REMAP_RIGHT_OK;
									remapRightOK = true;
									handler.sendMessage(msg);
								}
							}).start();
						} else {
							if (multipleThreads)
								doComputation2();
							else
								doComputation();
						}
					}
				} else {
					currentDisplayImageNumber = numberImages;
				}
			}
		});

		saveImageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Highgui.imwrite(
						imagePathBase
								+ "Disparity_"
								+ DateFormat.format("MM-dd-yy_hh-mm-ss",
										new Date().getTime()) + ".png",
						imageDisp);
				sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri
						.parse("file://"
								+ Environment.getExternalStorageDirectory())));
			}
		});
		


		image_view_disparity.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if ((event.getAction() == MotionEvent.ACTION_MOVE)
						|| (event.getAction() == MotionEvent.ACTION_UP)) {
					float[] touchPoint = new float[] { event.getX(),
							event.getY() };
					Matrix inverse = new Matrix();
					image_view_disparity.getImageMatrix().invert(inverse);
					inverse.mapPoints(touchPoint);
					if ((int) touchPoint[0] > imageDisp.width()
							|| (int) touchPoint[1] > imageDisp.height()
							|| (int) touchPoint[0] < 0) {
						pixValEditTextView.setText("Out of bounds from: "
								+ imageDisp.width() + "x" + imageDisp.height());
					} else {
						// Log.e(TAG, "Pixel X: " + (int) touchPoint[0] + " Y: "
						// + (int) touchPoint[1]);
						double pixelValue[] = imageDisp.get(
								(int) touchPoint[1], (int) touchPoint[0]);
						double dispValue[] = imageDispMapOR.get(
								(int) touchPoint[1], (int) touchPoint[0]);
						 double depthValue[] = imageDepth.get(
						 (int) touchPoint[1], (int) touchPoint[0]);
						if (doRemap) {
							pixValEditTextView.setText(
							 "Distance Repro: "
							 + String.valueOf(depthValue[2]) + "\n"+
							 //+ "Distance Fit: " + "\n" + 
							// + String.valueOf((int) distanceCalc) + "mm"
							// + "\n" + "Pixel X: " + (int) touchPoint[0]
							// + " Y: " + (int) touchPoint[1] + "\n"+
									"Disp Original: "
											+ String.valueOf(dispValue[0])
											+ "\n" + "Disp Norm: "
											+ String.valueOf(pixelValue[0])
									/*
									 * + "Distance Calc: " + String.valueOf(Z) +
									 * "\n"
									 */
									);
						} else {
							pixValEditTextView.setText("Pixel X: "
									+ (int) touchPoint[0] + " Y: "
									+ (int) touchPoint[1] + "\n" + "Disp: "
									+ String.valueOf(pixelValue[0]) + "\n"
									+ " Disp Original: "
									+ String.valueOf(dispValue[0]) + "\n");
						}
					}
				}
				return true;
			}
		});

		numberImagesEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence input, int arg1, int arg2,
					int arg3) {
				try {
					int inNumberImages = Integer.parseInt(input.toString());
					if (inNumberImages > 0)
						numberImages = inNumberImages;
					else
						numberImages = 1;
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					numberImages = 1;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

	}

	public class MatImageView extends ImageView {
		private Paint currentPaint;
		public boolean drawCross = false;
		public float currentX;
		public float currentY;

		public MatImageView(Context context) {
			super(context);
			currentPaint = new Paint();
			currentPaint.setDither(true);
			currentPaint.setColor(0xFF00CC00); // alpha.r.g.b
			currentPaint.setStyle(Paint.Style.STROKE);
			currentPaint.setStrokeJoin(Paint.Join.ROUND);
			currentPaint.setStrokeCap(Paint.Cap.ROUND);
			currentPaint.setStrokeWidth(2);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			if (drawCross) {
				canvas.drawLine(0, currentY, this.getWidth(), currentY,
						currentPaint);
				canvas.drawLine(currentX, 0, currentX, this.getHeight(),
						currentPaint);
			}
		}
	}

	private void doComputation() {
		new Thread(new Runnable() {
			public void run() {
				imageDisp = new Mat();
				timerStart = android.os.SystemClock.uptimeMillis();
				if (stereoMethod == StereoMethod.BM) {
					if (ALGParams.SADWindowSize < 5) {
						ALGParams.SADWindowSize = 5;
						SADWindowSizeSeek.setProgress(5);
					}
					if (doRemap) {
						Log.e(TAG, "Remaping");
						imageDisp = computeDepthMapBM(rectifiedLeft,
								rectifiedRight, validROILeftMat,
								validROIRightMat, QMat, ALGParams, doRemap);
					} else
						imageDisp = computeDepthMapBM(imageLeft, imageRight,
								validROILeftMat, validROIRightMat, QMat,
								ALGParams, doRemap);
				} else {
					if (stereoMethod == StereoMethod.SGBM)
						if (doRemap)
							imageDisp = computeDepthMapSGBM(rectifiedLeft,
									rectifiedRight, validROILeftMat,
									validROIRightMat, QMat, ALGParams, doRemap);
						else
							imageDisp = computeDepthMapSGBM(imageLeft,
									imageRight, validROILeftMat,
									validROIRightMat, QMat, ALGParams, doRemap);
				}

				timerStop = android.os.SystemClock.uptimeMillis();

				Log.e(TAG,
						"Execution Time: "
								+ String.valueOf(timerStop - timerStart) + "ms");

				runOnUiThread(new Runnable() {
					public void run() {
						displayLoadedImage(imageDisp, image_view_disparity);

					}
				});
				// }
				end_battery_level = batteryStatus.getIntExtra(
						BatteryManager.EXTRA_LEVEL, -1);
				Log.e(TAG,
						"Battery level difference: "
								+ String.valueOf(end_battery_level
										- start_battery_level));
			}
		}).start();
	}

	private boolean displayLoadedImage(Mat src, ImageView imageSurface) {
		if (src.empty()) {
			Log.e(TAG, "Image structure is empty.");
			return false;
		} else {
			try {
				Bitmap src_bmp = Bitmap.createBitmap(src.width(), src.height(),
						Bitmap.Config.ARGB_8888);
				Mat tmpImage = new Mat();
				if (src.channels() > 1) {
					if (src.channels() == 3) {
						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGR2RGBA);
					} else {

						Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_BGRA2RGBA);
					}
				} else {
					Imgproc.cvtColor(src, tmpImage, Imgproc.COLOR_GRAY2RGBA);
				}
				Utils.matToBitmap(tmpImage, src_bmp);
				imageSurface.setImageBitmap(src_bmp);
				return true;
			} catch (RuntimeException e) {
				Log.e(TAG, "Error while trying to show the image: ");
				Log.e(TAG, e.toString());
				return false;
			}
		}
	}

	private boolean allocateMats() {
		QMat = new Mat();
		imageLeft = new Mat();
		imageRight = new Mat();
		rectifiedLeft = new Mat();
		rectifiedRight = new Mat();
		map1Left = new Mat();
		map1Right = new Mat();
		map2Left = new Mat();
		map2Right = new Mat();
		imageDisp = new Mat();
		validROIRightMat = new Mat();
		validROILeftMat = new Mat();
		imageDepth = new Mat();
		imageDispMapOR = new Mat();
		leftUp = new Mat();
		leftDown = new Mat();
		rightUp = new Mat();
		rightDown = new Mat();
		return true;
	}

	private void loadMats() {
		if (doRemap) {
			NativeMatLoadProcess(QMat.getNativeObjAddr(), imageDirectoryBase
					+ "QMatrix.yml", "Q_Matrix");
			NativeMatLoadProcess(map1Left.getNativeObjAddr(),
					imageDirectoryBase + "Remap1Left.yml", "Remap1_Matrix");
			NativeMatLoadProcess(map2Left.getNativeObjAddr(),
					imageDirectoryBase + "Remap2Left.yml", "Remap2_Matrix");
			NativeMatLoadProcess(map1Right.getNativeObjAddr(),
					imageDirectoryBase + "Remap1Right.yml", "Remap1_Matrix");
			NativeMatLoadProcess(map2Right.getNativeObjAddr(),
					imageDirectoryBase + "Remap2Right.yml", "Remap2_Matrix");
			NativeMatLoadProcess(validROILeftMat.getNativeObjAddr(),
					imageDirectoryBase + "ValidROILeft.yml", "ROILeft_Matrix");
			NativeMatLoadProcess(validROIRightMat.getNativeObjAddr(),
					imageDirectoryBase + "ValidROIRight.yml", "ROIRight_Matrix");
		}
	}

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV loaded successfully");
				System.loadLibrary("ecivision-depth-generator");
				matsAllocated = allocateMats();
				Log.e(TAG, "Mats allocated correctly");
				if (doRemap)
					loadMats();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	// Extremely important line, loads OpenCV Library
	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	// Auxiliar functions
	private Mat computeDepthMapBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params, boolean useROI) {

		Mat dispMap = new Mat();
		NativeDepthMapBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), valiROILeft.getNativeObjAddr(),
				validROIRight.getNativeObjAddr(), dispMap.getNativeObjAddr(),
				imageDepth.getNativeObjAddr(),
				imageDispMapOR.getNativeObjAddr(), qMat.getNativeObjAddr(),
				params.SADWindowSize, params.minDisparity,
				params.numberOfDisparities, params.textureThresh,
				params.uniquenessRatio, params.speckeWidowSize,
				params.speckleRange, params.maxDisparityCheckDiff,
				params.prefilterCap, useROI, coloredMap);
		return dispMap;
	}

	private Mat computeDepthMapSGBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params, boolean useROI) {

		Mat dispMap = new Mat();
		NativeDepthMapSGBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), dispMap.getNativeObjAddr(),
				imageDepth.getNativeObjAddr(),
				imageDispMapOR.getNativeObjAddr(), qMat.getNativeObjAddr(),
				params.SADWindowSize, params.minDisparity,
				params.numberOfDisparities, params.textureThresh,
				params.uniquenessRatio, params.speckeWidowSize,
				params.speckleRange, params.maxDisparityCheckDiff,
				params.prefilterCap, useROI, coloredMap);
		return dispMap;
	}

	// Native functions
	public native void NativeMatLoadProcess(long addrMat, String fileName,
			String matName);

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	public native void NativeDepthMapBM(long addrImageLeft,
			long addrImageRight, long addrValidROILeftMat,
			long addrValidROIRightMat, long addrDispMap, long addrDepthMap,
			long addrDispMapOr, long addrQMat, int SADWindow, int minDisparity,
			int numberDisparities, int textureThreshold, int uniquenessRatio,
			int speckleWindowSize, int speckleRange, int disp12MaxDiff,
			int prefilterCap, boolean doRemap, boolean coloredMap);

	public native void NativeDepthMapSGBM(long addrImageLeft,
			long addrImageRight, long addrDispMap, long addrDepthMap,
			long addrDispMapOr, long addrQMat, int SADWindow, int minDisparity,
			int numberDisparities, int textureThreshold, int uniquenessRatio,
			int speckleWindowSize, int speckleRange, int disp12MaxDiff,
			int prefilterCap, boolean doRemap, boolean coloredMap);

	private class MySeekBarListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			switch (seekBar.getId()) {
			case R.id.param_seekbar1:
				if (stereoMethod == StereoMethod.BM) {
					if (progress < 2)
						ALGParams.SADWindowSize = 5;
					else
						ALGParams.SADWindowSize = (2 * progress + 1);
				} else {
					if (progress < 1)
						ALGParams.SADWindowSize = 3;
					else
						ALGParams.SADWindowSize = (2 * progress + 1);
				}
				SADWindowSizeText.setText(mResources
						.getString(R.string.param_block_size)
						+ String.valueOf(ALGParams.SADWindowSize));
				break;
			case R.id.param_seekbar2:
				if (progress < 1) {
					ALGParams.numberOfDisparities = 16;
				} else {
					ALGParams.numberOfDisparities = progress * 16;
				}
				numberOfDisparitiesText.setText(mResources
						.getString(R.string.param_number_disparities)
						+ String.valueOf(ALGParams.numberOfDisparities));
				break;
			case R.id.param_seekbar3:
				ALGParams.minDisparity = (progress - 10) * 16;
				minDisparityText.setText(mResources
						.getString(R.string.param_min_disparity)
						+ String.valueOf(ALGParams.minDisparity));
				break;
			case R.id.param_seekbar4:
				ALGParams.textureThresh = progress * 16;
				textureThreshText.setText(mResources
						.getString(R.string.param_texture_thresh)
						+ String.valueOf(ALGParams.textureThresh));
				break;

			case R.id.param_seekbar5:
				if (progress <= 0)
					ALGParams.uniquenessRatio = 1;
				else
					ALGParams.uniquenessRatio = progress;
				uniquenessRatioText.setText(mResources
						.getString(R.string.param_uniqueness)
						+ String.valueOf(ALGParams.uniquenessRatio));
				break;
			case R.id.param_seekbar6:
				ALGParams.speckeWidowSize = progress;
				speckleWindowText.setText(mResources
						.getString(R.string.param_speckle_window)
						+ String.valueOf(ALGParams.speckeWidowSize));
				break;
			case R.id.param_seekbar7:
				ALGParams.speckleRange = progress;
				speckleRangeText.setText(mResources
						.getString(R.string.param_speckle_range)
						+ String.valueOf(ALGParams.speckleRange));
				break;
			case R.id.param_seekbar8:
				ALGParams.maxDisparityCheckDiff = progress;
				MaxDisparityCheckDiffText.setText(mResources
						.getString(R.string.param_disp12maxdiff)
						+ String.valueOf(ALGParams.maxDisparityCheckDiff));
				break;
			case R.id.param_seekbar9:
				if (progress < 1)
					ALGParams.prefilterCap = 1;
				else
					ALGParams.prefilterCap = progress;

				prefilterCapText.setText(mResources
						.getString(R.string.param_prefilter_cap)
						+ String.valueOf(ALGParams.prefilterCap));
				break;
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			timerStart = android.os.SystemClock.uptimeMillis();
			start_battery_level = batteryStatus.getIntExtra(
					BatteryManager.EXTRA_LEVEL, -1);
			if (multipleThreads)
				doComputation2();
			else
				doComputation();
			Log.e(TAG, "released Seekbar");
		}
	}

	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode,
			Intent aData) {

		super.onActivityResult(aRequestCode, aResultCode, aData);
		imagePathBase = Environment.getExternalStorageDirectory() + "/DCIM2/"
				+ aData.getStringExtra("pathbase");
		imageExtension = aData.getStringExtra("extention");
		numberImages = aData.getIntExtra("numberimages", 0);
		doRemap = aData.getBooleanExtra("doremap", false);
		imageNameBase = aData.getStringExtra("namebase");
		imageDirectoryBase = Environment.getExternalStorageDirectory()
				+ "/DCIM2/" + aData.getStringExtra("directorybase");

		if (numberImages > 0) {
			numberImagesEditText.setText(String.valueOf(numberImages));
		} else {
			numberImages = 1;
			numberImagesEditText.setText(String.valueOf(numberImages));
		}
		Log.e(TAG, "Image path is: " + imagePathBase);
		Log.e(TAG, "Image Extention is: " + imageExtension);
		Log.e(TAG, "Number of Images is: " + numberImages);
		Log.e(TAG, "Image remap is: " + doRemap);
	}

	private void doComputation2() {
		leftUp = rectifiedLeft.submat(0, (rectifiedLeft.height()) / 2, 0,
				rectifiedLeft.width());
		leftDown = rectifiedLeft.submat((rectifiedLeft.height()) / 2,
				rectifiedLeft.height(), 0, rectifiedLeft.width());
		rightUp = rectifiedRight.submat(0, (rectifiedRight.height()) / 2, 0,
				rectifiedRight.width());
		rightDown = rectifiedRight.submat((rectifiedRight.height()) / 2,
				rectifiedLeft.height(), 0, rectifiedRight.width());

		final Thread upperThread = new Thread(new Runnable() {
			public void run() {
				dispUp = new Mat();
				timerStart = android.os.SystemClock.uptimeMillis();
				if (stereoMethod == StereoMethod.BM)
					dispUp = computeDepthMapBM(leftUp, rightUp,
							validROILeftMat, validROIRightMat, QMat, ALGParams,
							false);
				if (stereoMethod == StereoMethod.SGBM)
					dispUp = computeDepthMapSGBM(leftUp, rightUp,
							validROILeftMat, validROIRightMat, QMat, ALGParams,
							false);
			}
		});
		final Thread lowerThread = new Thread(new Runnable() {
			public void run() {
				dispUp = new Mat();
				if (stereoMethod == StereoMethod.BM)
					dispLow = computeDepthMapBM(leftDown, rightDown,
							validROILeftMat, validROIRightMat, QMat, ALGParams,
							false);
				if (stereoMethod == StereoMethod.SGBM)
					dispLow = computeDepthMapSGBM(leftDown, rightDown,
							validROILeftMat, validROIRightMat, QMat, ALGParams,
							false);
			}
		});

		final Thread SynchThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Log.e(TAG, "SynchThread Started");
				try {
					lowerThread.join();
					upperThread.join();
					Log.e(TAG, "Threads Synchronized");
				} catch (InterruptedException ie) {
					Log.e(TAG, "Threads Not Synchronized");
				}

				timerStop = android.os.SystemClock.uptimeMillis();

				Log.e(TAG,
						"Execution Time: "
								+ String.valueOf(timerStop - timerStart) + "ms");
				imageDisp = new Mat(360, 640, dispUp.type());
				Log.e(TAG, "height :" + String.valueOf(rectifiedLeft.height()));
				Log.e(TAG, "height :" + String.valueOf(leftDown.height()));
				Log.e(TAG, "channels :" + String.valueOf(imageDisp.channels()));
				Mat imageDispUp = imageDisp.submat(0, (imageDisp.height()) / 2,
						0, imageDisp.width());
				Mat imageDispLow = imageDisp.submat((imageDisp.height()) / 2,
						imageDisp.height(), 0, imageDisp.width());
				Log.e(TAG, "channels :" + String.valueOf(imageDisp.depth()));
				try {
					imageDispUp.push_back(dispUp);
					imageDispLow.push_back(dispLow);
					Log.e(TAG, "height :" + String.valueOf(imageDisp.height()));
					Log.e(TAG, "Worked");
				} catch (Exception e) {
					Log.e(TAG, e.toString());
					Log.e(TAG, "Did not work");
				}
				runOnUiThread(new Runnable() {
					public void run() {
						displayLoadedImage(imageDisp, image_view_disparity);

					}
				});
				// // }
				// end_battery_level = batteryStatus.getIntExtra(
				// BatteryManager.EXTRA_LEVEL, -1);
				// Log.e(TAG,
				// "Battery level difference: "
				// + String.valueOf(end_battery_level
				// - start_battery_level));
			}
		});
		upperThread.setPriority(Thread.MAX_PRIORITY);
		lowerThread.setPriority(Thread.MAX_PRIORITY);
		SynchThread.setPriority(Thread.MAX_PRIORITY);
		upperThread.start();
		lowerThread.start();
		SynchThread.start();
	}

	private void setupParamsScrollBars(DepthParamsObject Params) {
		numberOfDisparitiesSeek.setProgress(Params.numberOfDisparities/16);
		minDisparitySeek.setProgress(Params.minDisparity/16 + 10);
		SADWindowSizeSeek.setProgress((Params.SADWindowSize - 1)/2);
		textureThreshSeek.setProgress(Params.textureThresh/16);
		uniquenessRatioSeek.setProgress(Params.uniquenessRatio);
		MaxDisparityCheckDiffSeek.setProgress(Params.maxDisparityCheckDiff);
		speckleWindowSeek.setProgress(Params.speckeWidowSize);
		speckleRangeSeek.setProgress(Params.speckleRange);
		//sgbmSmoothP1Seek.setProgress(Params.sgbmSmoothP1);
		//sgbmsmoothP2Seek.setProgress(Params.sgbmSmoothP2);
		prefilterCapSeek.setProgress(Params.prefilterCap);

		SADWindowSizeText.setText(mResources
				.getString(R.string.param_block_size)
				+ String.valueOf(ALGParams.SADWindowSize));
		numberOfDisparitiesText.setText(mResources
				.getString(R.string.param_number_disparities)
				+ String.valueOf(ALGParams.numberOfDisparities));
		minDisparityText.setText(mResources
				.getString(R.string.param_min_disparity)
				+ String.valueOf(ALGParams.minDisparity));
		textureThreshText.setText(mResources
				.getString(R.string.param_texture_thresh)
				+ String.valueOf(ALGParams.textureThresh));
		uniquenessRatioText.setText(mResources
				.getString(R.string.param_uniqueness)
				+ String.valueOf(ALGParams.uniquenessRatio));
		speckleWindowText.setText(mResources
				.getString(R.string.param_speckle_window)
				+ String.valueOf(ALGParams.speckeWidowSize));
		speckleRangeText.setText(mResources
				.getString(R.string.param_speckle_range)
				+ String.valueOf(ALGParams.speckleRange));
		MaxDisparityCheckDiffText.setText(mResources
				.getString(R.string.param_disp12maxdiff)
				+ String.valueOf(ALGParams.maxDisparityCheckDiff));
		prefilterCapText.setText(mResources
				.getString(R.string.param_prefilter_cap)
				+ String.valueOf(ALGParams.prefilterCap));
	}
	
	private void deactivateSeekbars(){
		SADWindowSizeSeek.setEnabled(false);
		numberOfDisparitiesSeek.setEnabled(false);
		minDisparitySeek.setEnabled(false);
		textureThreshSeek.setEnabled(false);
		uniquenessRatioSeek.setEnabled(false);
		speckleWindowSeek.setEnabled(false);
		speckleRangeSeek.setEnabled(false);
		MaxDisparityCheckDiffSeek.setEnabled(false);
		prefilterCapSeek.setEnabled(false);
	}
	
	private void activateSeekbars(){
		SADWindowSizeSeek.setEnabled(true);
		numberOfDisparitiesSeek.setEnabled(true);
		minDisparitySeek.setEnabled(true);
		textureThreshSeek.setEnabled(true);
		uniquenessRatioSeek.setEnabled(true);
		speckleWindowSeek.setEnabled(true);
		speckleRangeSeek.setEnabled(true);
		MaxDisparityCheckDiffSeek.setEnabled(true);
		prefilterCapSeek.setEnabled(true);
	}

}
