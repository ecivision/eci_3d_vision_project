package com.ecivision.motortest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.immersion.uhl.Launcher;

public class TMotorTestActivity extends Activity {

	private final String TAG = "TMotorTest";

	public String datatosend = "";

	private CheckBox leftMotorCheckBox;
	private CheckBox centerMotorCheckBox;
	private CheckBox rightMotorCheckBox;
	
	private MyCheckBoxChangeListener mCheckBoxChangeListener;

	private SeekBar leftMotorSeekBar;
	private SeekBar centerMotorSeekBar;
	private SeekBar rightMotorSeekBar;

	private MySeekBarListener mSeekBarChangeListener;

	private int leftPWMVal = 0;
	private int centerPWMVal = 0;
	private int rightPWMVal = 0;
	
	private final boolean right= true;
	private final boolean left = false;
	
	
	private Launcher mLauncher;
	private int hapticEffect= Launcher.WEAPON1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		AudioSerialOutMono.activate();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tmotor_test);
		
		// Setting up the CheckBoxes
		leftMotorCheckBox = (CheckBox) findViewById(R.id.leftMotorCheckBox);
		centerMotorCheckBox = (CheckBox) findViewById(R.id.centerMotorCheckBox);
		rightMotorCheckBox = (CheckBox) findViewById(R.id.rightMotorCheckBox);
		
		mCheckBoxChangeListener = new MyCheckBoxChangeListener();
		
		leftMotorCheckBox.setOnCheckedChangeListener(mCheckBoxChangeListener);
		centerMotorCheckBox.setOnCheckedChangeListener(mCheckBoxChangeListener);
		rightMotorCheckBox.setOnCheckedChangeListener(mCheckBoxChangeListener);

		// Setting up the seekbars
		leftMotorSeekBar = (SeekBar) findViewById(R.id.leftMotorSeekBar);
		centerMotorSeekBar = (SeekBar) findViewById(R.id.centerMotorSeekBar);
		rightMotorSeekBar = (SeekBar) findViewById(R.id.rightMotorSeekBar);
		
		leftMotorSeekBar.setEnabled(false);
		centerMotorSeekBar.setEnabled(false);
		rightMotorSeekBar.setEnabled(false);
		
		mSeekBarChangeListener = new MySeekBarListener();

		leftMotorSeekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);
		centerMotorSeekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);
		rightMotorSeekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);

		// Setting up the serial communication parameters
		AudioSerialOutMono.new_baudRate = 9200;
		AudioSerialOutMono.UpdateParameters(true);


//		 Setting up the central vibrator
		try {

			mLauncher = new Launcher(this);

		} catch (Exception e) {

			Log.e(TAG, "Exception!: " + e.getMessage());
		}



	}

	private class MySeekBarListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			switch (seekBar.getId()) {
			case R.id.leftMotorSeekBar:
				leftPWMVal = progress * 17;
				break;
			case R.id.centerMotorSeekBar:
				centerPWMVal = progress;
				break;
			case R.id.rightMotorSeekBar:
				rightPWMVal = progress * 17;
				break;

			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			switch (seekBar.getId()) {
			case R.id.leftMotorSeekBar:
				Log.e("Sent: ",String.valueOf(leftPWMVal));
				SendAudio(leftPWMVal, left);
				break;
			case R.id.centerMotorSeekBar:
				Log.e("Sent: ",String.valueOf(centerPWMVal));
				mLauncher.play(hapticEffect + centerPWMVal);
				break;
			case R.id.rightMotorSeekBar:
				Log.e("Sent: ",String.valueOf(rightPWMVal));
				SendAudio(rightPWMVal, right);
				break;
			}

		}

	}
	
	private class MyCheckBoxChangeListener implements OnCheckedChangeListener{

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			switch(buttonView.getId()){
			case R.id.leftMotorCheckBox:
				if(isChecked){
					leftMotorSeekBar.setEnabled(true);
					SendAudio(leftPWMVal, left);
				}
				else{
					leftMotorSeekBar.setEnabled(false);
					SendAudio((char) 0, left);
				}
				break;
			case R.id.centerMotorCheckBox:
				if(isChecked){
					centerMotorSeekBar.setEnabled(true);
					mLauncher.play(hapticEffect + centerPWMVal);
				}
				else{
					centerMotorSeekBar.setEnabled(false);
					mLauncher.stop();
				}
				break;
			case R.id.rightMotorCheckBox:
				if(isChecked){
				rightMotorSeekBar.setEnabled(true);
				SendAudio(rightPWMVal, right);
				}
				else{
					rightMotorSeekBar.setEnabled(false);
					SendAudio((char)0, right);
				}
				break;
			}			
		}
		
	}
	
	private void SendAudio(int valueToSend, boolean Channel){
		AudioSerialOutMono.new_baudRate = 9200;
		AudioSerialOutMono.UpdateParameters(true);
		Log.e("Sent: ",String.valueOf(valueToSend));
		AudioSerialOutMono.output(String.valueOf(valueToSend));
	}
}
