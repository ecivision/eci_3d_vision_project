/*
*  CameraPreview.java
*/
package com.ecivision.cvcustomcamera;

import java.io.IOException;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;

public class CameraPreview implements SurfaceHolder.Callback, Camera.PreviewCallback
{
	private final String TAG ="CameraPreview";
	private Camera mCamera = null;
	private int imageFormat;
	private int PreviewSizeWidth;
 	private int PreviewSizeHeight;
 	private int cameraID;
 	private boolean bProcessing = false;

	
 	private Handler mHandler;
 	private CVProcessingRunnable myProcessingCallback;
 	
	public CameraPreview(int _cameraID,int PreviewlayoutWidth, int PreviewlayoutHeight, CVProcessingRunnable mClass)
    {
		PreviewSizeWidth = PreviewlayoutWidth;
    	PreviewSizeHeight = PreviewlayoutHeight;
    	myProcessingCallback = mClass;
    	cameraID=_cameraID;
    	mHandler = new Handler(Looper.getMainLooper());
    }
	
	public CameraPreview(int PreviewlayoutWidth, int PreviewlayoutHeight, CVProcessingRunnable mClass)
    {
		PreviewSizeWidth = PreviewlayoutWidth;
    	PreviewSizeHeight = PreviewlayoutHeight;
    	myProcessingCallback = mClass;
    	cameraID=2;
    	mHandler = new Handler(Looper.getMainLooper());
    }
	

	@Override
	public void onPreviewFrame(byte[] frameData, Camera _camera) 
	{
		// At preview mode, the frame data will push to here.
		if (imageFormat == ImageFormat.NV21)
        {
			//We only accept the NV21(YUV420) format.
			if ( !bProcessing )
			{
				mHandler.post(myProcessingCallback.MyImageProcessing(frameData));
			}
        }
	}
	
	
	public void onPause()
    {
    	mCamera.stopPreview();
    }
	
   
	@SuppressWarnings("deprecation")
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) 
	{
	    Parameters parameters;
		
	    parameters = mCamera.getParameters();
		// Set the camera preview size
		parameters.setPreviewSize(PreviewSizeWidth, PreviewSizeHeight);
		if(parameters.getFocusMode().contains(Parameters.FOCUS_MODE_INFINITY))
		parameters.setFocusMode(Parameters.FOCUS_MODE_INFINITY);
		else{
			if(parameters.getFocusMode().contains(Parameters.FOCUS_MODE_FIXED))
				parameters.setFocusMode(Parameters.FOCUS_MODE_FIXED);		
		}
		if(parameters.get("preview-frame-rate")!=null)
		parameters.set("preview-frame-rate",10);
		if(parameters.get("max-zoom")!=null)
		parameters.set("max-zoom",0);
		if(parameters.get("auto-convergence")!=null)
		parameters.set("auto-convergence","mode-disable");
//		if(parameters.get("whitebalance")!=null);
//		parameters.set("whitebalance","cloudy-daylight");
		if(parameters.get("s3d2d-preview")!=null)
		parameters.set("s3d2d-preview","on");
		Log.e(TAG, parameters.flatten());
		imageFormat = parameters.getPreviewFormat();
		
		mCamera.setParameters(parameters);
		
		mCamera.startPreview();
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) 
	{
		mCamera = Camera.open(cameraID);
		try
		{
			// If did not set the SurfaceHolder, the preview area will be black.
			mCamera.setPreviewDisplay(arg0);
			mCamera.setPreviewCallback(this);
		} 
		catch (IOException e)
		{
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) 
	{
    	mCamera.setPreviewCallback(null);
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}


  
  public interface CVProcessingRunnable{
	  public Runnable MyImageProcessing(byte[] frameData);
  }
  
  public void setProcessing(){
	  bProcessing=true;
  }
  
  public void clearProcessing(){
	  bProcessing=false;
  }
   }
