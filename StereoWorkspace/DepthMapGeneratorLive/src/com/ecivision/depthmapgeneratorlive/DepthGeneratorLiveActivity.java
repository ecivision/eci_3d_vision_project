package com.ecivision.depthmapgeneratorlive;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.ecivision.cvcustomcamera.CameraPreview;
import com.ecivision.cvcustomcamera.CameraPreview.CVProcessingRunnable;

public class DepthGeneratorLiveActivity extends Activity implements
		CVProcessingRunnable {

	private static final String TAG = "Ecivision::DepthGen::Activity";
	private static final int REMAP_LEFT_OK = 0;
	private static final int REMAP_RIGHT_OK = 1;
	private int currentDisplayImageNumber = 1;
	// GUI elements BM
	private ImageView image_view_disparity;

	private TextView numberOfDisparitiesText;
	private SeekBar numberOfDisparitiesSeek;

	private TextView minDisparityText;
	private SeekBar minDisparitySeek;

	private TextView SADWindowSizeText;
	private SeekBar SADWindowSizeSeek;
	private String SADWindowSizeString;

	private TextView textureThreshText;
	private SeekBar textureThreshSeek;

	private TextView uniquenessRatioText;
	private SeekBar uniquenessRatioSeek;

	private TextView MaxDisparityCheckDiffText;
	private SeekBar MaxDisparityCheckDiffSeek;

	private TextView speckleWindowText;
	private SeekBar speckleWindowSeek;

	private TextView speckleRangeText;
	private SeekBar speckleRangeSeek;

	private TextView sgbmSmoothP1Text;
	private SeekBar sgbmSmoothP1Seek;

	private TextView sgbmsmoothP2Text;
	private SeekBar sgbmsmoothP2Seek;

	private TextView prefilterCapText;
	private SeekBar prefilterCapSeek;
	private MySeekBarListener mSeekBarListener;

	// Other UI elements
	private Button computeButton;
	private Button nextImageButton;
	private Button prevImageButton;
	private Button saveImageButton;
	private Spinner stereoMethodSpinner;
	private ArrayAdapter<String> stereoMethodAdapter;
	private EditText numberImagesEditText;
	private TextView pixValEditTextView;
	private CheckBox colorMapCheckBox;

	private enum StereoMethod {
		BM, SGBM, VAR
	}

	private List<String> stereoMethodList = new ArrayList<String>();
	private StereoMethod stereoMethod = StereoMethod.BM;
	private Handler handler;

	private DepthParamsObject ALGParams, ALGParamsSynch;
	private DepthParamsObject BMParams, SGBMParams;

	private long timerStart;
	private long timerStop;

	private IntentFilter ifilter;
	private Intent batteryStatus;

	// Objects required for image rectification and disparity map calculation.
	private Mat QMat;
	private Mat imageLeft, imageRight, mRGBA;
	private Mat rectifiedLeft, rectifiedRight;
	private Mat map1Left, map1Right, map2Left, map2Right;
	private Mat validROIRightMat, validROILeftMat;
	private Mat imageDisp, imageDepth, imageDispMapOR;
	private Mat leftUp, rightUp, dispUp, dispLow;
	private Mat leftDown, rightDown, depthUp, depthLow;

	private String imagePathBase;
	private String imageExtension;
	private String imageDirectoryBase;
	private String imageNameBase;

	private boolean matsAllocated = false;
	private boolean remapLeftOK = false;
	private boolean remapRightOK = false;
	private boolean alreadyLoaded = false;
	private boolean doRemap = false;
	private boolean multipleThreads = false;
	private boolean coloredMap = true;

	private int start_battery_level = 0;
	private int end_battery_level = 0;

	Resources mResources;

	private SurfaceHolder camSurfaceHolder;
	private SurfaceView camView;
	private int PreviewSizeWidth = 640;
	private int PreviewSizeHeight = 480;
	private Size imageSize;
	private CameraPreview camPreview;
	private ImageView mCameraImageView = null;
	private byte[] mFrameData;
	private Bitmap bitmap = null;
	private Context mContext;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_depth_generator_live);
		camView = (SurfaceView) findViewById(R.id.camview_surface_view);
		camSurfaceHolder = camView.getHolder();
		camPreview = new CameraPreview(PreviewSizeWidth, PreviewSizeHeight,
				this);

		mContext = this;

		camSurfaceHolder.addCallback(camPreview);
		camSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mCameraImageView = (ImageView) findViewById(R.id.camera_out_imageview);
		mCameraImageView.setBackgroundColor(Color.WHITE);
		bitmap = Bitmap.createBitmap(PreviewSizeWidth / 2,
				PreviewSizeHeight / 2, Bitmap.Config.ARGB_8888);

		pixValEditTextView = (TextView) findViewById(R.id.pixel_val_textview);

		mResources = getResources();
		mContext = this;
		// Setup UI elements
		// TextViews
		SADWindowSizeText = (TextView) findViewById(R.id.param_textview1);
		numberOfDisparitiesText = (TextView) findViewById(R.id.param_textview2);
		minDisparityText = (TextView) findViewById(R.id.param_textview3);
		textureThreshText = (TextView) findViewById(R.id.param_textview4);
		uniquenessRatioText = (TextView) findViewById(R.id.param_textview5);
		speckleWindowText = (TextView) findViewById(R.id.param_textview6);
		speckleRangeText = (TextView) findViewById(R.id.param_textview7);
		MaxDisparityCheckDiffText = (TextView) findViewById(R.id.param_textview8);
		prefilterCapText = (TextView) findViewById(R.id.param_textview9);
		// seekbars
		SADWindowSizeSeek = (SeekBar) findViewById(R.id.param_seekbar1);
		numberOfDisparitiesSeek = (SeekBar) findViewById(R.id.param_seekbar2);
		minDisparitySeek = (SeekBar) findViewById(R.id.param_seekbar3);
		textureThreshSeek = (SeekBar) findViewById(R.id.param_seekbar4);
		uniquenessRatioSeek = (SeekBar) findViewById(R.id.param_seekbar5);
		speckleWindowSeek = (SeekBar) findViewById(R.id.param_seekbar6);
		speckleRangeSeek = (SeekBar) findViewById(R.id.param_seekbar7);
		MaxDisparityCheckDiffSeek = (SeekBar) findViewById(R.id.param_seekbar8);
		prefilterCapSeek = (SeekBar) findViewById(R.id.param_seekbar9);

		BMParams = new DepthParamsObject();
		SGBMParams = new DepthParamsObject();
		ALGParams = new DepthParamsObject();
		ALGParamsSynch = new DepthParamsObject();
		ALGParams = BMParams;
		ALGParamsSynch = ALGParams;

		setupParamsScrollBars(BMParams);

		// CheckBox
		colorMapCheckBox = (CheckBox) findViewById(R.id.multi_check);
		colorMapCheckBox.setChecked(true);
		// Setting seekbar change listener
		mSeekBarListener = new MySeekBarListener();
		SADWindowSizeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		numberOfDisparitiesSeek.setOnSeekBarChangeListener(mSeekBarListener);
		minDisparitySeek.setOnSeekBarChangeListener(mSeekBarListener);
		textureThreshSeek.setOnSeekBarChangeListener(mSeekBarListener);
		uniquenessRatioSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleWindowSeek.setOnSeekBarChangeListener(mSeekBarListener);
		speckleRangeSeek.setOnSeekBarChangeListener(mSeekBarListener);
		MaxDisparityCheckDiffSeek.setOnSeekBarChangeListener(mSeekBarListener);
		prefilterCapSeek.setOnSeekBarChangeListener(mSeekBarListener);

		// Spinner
		stereoMethodList.add("BM");
		stereoMethodList.add("SGBM");
		stereoMethodSpinner = (Spinner) findViewById(R.id.stereo_method_spinner);
		stereoMethodAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stereoMethodList);
		stereoMethodAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		stereoMethodSpinner.setAdapter(stereoMethodAdapter);

		stereoMethodSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long id) {
						if (id == 0) {
							stereoMethod = StereoMethod.BM;
							ALGParams = BMParams;
							setupParamsScrollBars(BMParams);
							synchronized(ALGParamsSynch){
								ALGParamsSynch=ALGParams;
							}
						} else {
							if (id == 1) {
								stereoMethod = StereoMethod.SGBM;
								ALGParams = SGBMParams;
								setupParamsScrollBars(SGBMParams);
								synchronized(ALGParamsSynch){
									ALGParamsSynch=ALGParams;
								}
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});

		colorMapCheckBox
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						coloredMap = isChecked;
					}
				});
		
		
		mCameraImageView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if ((event.getAction() == MotionEvent.ACTION_MOVE)
						|| (event.getAction() == MotionEvent.ACTION_UP)) {
					float[] touchPoint = new float[] { event.getX(),
							event.getY() };
					Matrix inverse = new Matrix();
					mCameraImageView.getImageMatrix().invert(inverse);
					inverse.mapPoints(touchPoint);
					if ((int) touchPoint[0] > imageDisp.width()
							|| (int) touchPoint[1] > imageDisp.height()
							|| (int) touchPoint[0] < 0) {
						pixValEditTextView.setText("Out of bounds from: "
								+ imageDisp.width() + "x" + imageDisp.height());
					} else {
						// Log.e(TAG, "Pixel X: " + (int) touchPoint[0] + " Y: "
						// + (int) touchPoint[1]);
						double pixelValue[] = imageDisp.get(
								(int) touchPoint[1], (int) touchPoint[0]);
						double dispValue[] = imageDispMapOR.get(
								(int) touchPoint[1], (int) touchPoint[0]);
						 double depthValue[] = imageDepth.get(
						 (int) touchPoint[1], (int) touchPoint[0]);
						if (doRemap) {
							pixValEditTextView.setText(
							 "Distance Repro: "
							 + String.valueOf((int)depthValue[2]) + " mm\n"+
							 //+ "Distance Fit: " + "\n" + 
							// + String.valueOf((int) distanceCalc) + "mm"
							// + "\n" + "Pixel X: " + (int) touchPoint[0]
							// + " Y: " + (int) touchPoint[1] + "\n"+
									"Disp Original: "
											+ String.valueOf(dispValue[0])
											+ "\n" + "Disp Norm: "
											+ String.valueOf(pixelValue[0])
									/*
									 * + "Distance Calc: " + String.valueOf(Z) +
									 * "\n"
									 */
									);
						} else {
							pixValEditTextView.setText("Pixel X: "
									+ (int) touchPoint[0] + " Y: "
									+ (int) touchPoint[1] + "\n" + "Disp: "
									+ String.valueOf(pixelValue[0]) + "\n"
									+ " Disp Original: "
									+ String.valueOf(dispValue[0]) + "\n");
						}
					}
				}
				return true;
			}
		});

		// Call the image set selection intent which will define where to save
		// the images and the calibration files
		Intent intent = new Intent(
				"com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity.intent.action.Launch");
		intent.setComponent(ComponentName
				.unflattenFromString("com.ecivision.imagesetselectionsubsystem.ImageSelectionActivity"));
		intent.addCategory("android.intent.category.DEFAULT");
		startActivityForResult(intent, 1);
	}

	@Override
	public Runnable MyImageProcessing(byte[] frameData) {
		mFrameData = frameData;
		return DoImageProcessing;
	}

	private Thread leftThread, rightThread, nativeDepthThread;

	private Runnable DoImageProcessing = new Runnable() {
		public void run() {
			camPreview.setProcessing();
			if (mRGBA != null) {
				mRGBA.release();
				imageLeft.release();
				imageRight.release();
			}
			timerStart = android.os.SystemClock.uptimeMillis();
			mRGBA = rawImageData2MatGRAY(mFrameData);
			imageLeft = new Mat(mRGBA, new Range(0, mRGBA.height()), new Range(
					0, mRGBA.width() / 2));
			imageRight = new Mat(mRGBA, new Range(0, mRGBA.height()),
					new Range((mRGBA.width() + 1) / 2, mRGBA.width()));

			leftThread = new Thread(new Runnable() {
				@Override
				public void run() {
					Imgproc.remap(imageLeft, rectifiedLeft, map1Left, map2Left,
							Imgproc.INTER_LINEAR);
				}
			});
			rightThread = new Thread(new Runnable() {
				@Override
				public void run() {
					Imgproc.remap(imageRight, rectifiedRight, map1Right,
							map2Right, Imgproc.INTER_LINEAR);
				}
			});
			leftThread.setPriority(Thread.MAX_PRIORITY);
			rightThread.setPriority(Thread.MAX_PRIORITY);
			leftThread.run();
			rightThread.run();
			while (rightThread.isAlive() && leftThread.isAlive())
				;

			nativeDepthThread = new Thread(new Runnable() {
				public void run() {
					imageDisp = new Mat();
					synchronized (ALGParamsSynch) {
						if (stereoMethod == StereoMethod.BM) {
							if (ALGParamsSynch.SADWindowSize < 5) {
								ALGParamsSynch.SADWindowSize = 5;
								SADWindowSizeSeek.setProgress(5);
							}
							if (doRemap) {
								imageDisp = computeDepthMapBM(rectifiedLeft,
										rectifiedRight, validROILeftMat,
										validROIRightMat, QMat, ALGParamsSynch,
										doRemap);
							} else
								imageDisp = computeDepthMapBM(imageLeft,
										imageRight, validROILeftMat,
										validROIRightMat, QMat, ALGParamsSynch,
										doRemap);
						} else {
							if (stereoMethod == StereoMethod.SGBM)
								if (doRemap)
									imageDisp = computeDepthMapSGBM(
											rectifiedLeft, rectifiedRight,
											validROILeftMat, validROIRightMat,
											QMat, ALGParamsSynch, doRemap);
								else
									imageDisp = computeDepthMapSGBM(imageLeft,
											imageRight, validROILeftMat,
											validROIRightMat, QMat,
											ALGParamsSynch, doRemap);
						}
					}
				}
			});
			nativeDepthThread.setPriority(Thread.MAX_PRIORITY);
			nativeDepthThread.run();
			while (nativeDepthThread.isAlive())
				;
			timerStop = android.os.SystemClock.uptimeMillis();
			Utils.matToBitmap(imageDisp, bitmap);
			mCameraImageView.setImageBitmap(bitmap);
			// Log.e(TAG,"Threads ended time: "+String.valueOf(timerStop-timerStart)+
			// "ms");
			camPreview.clearProcessing();
		}
	};

	public Mat rawImageData2MatRGBA(byte[] frameData) {
		Mat mYuvTemp = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2,
				PreviewSizeWidth, CvType.CV_8UC1);
		mYuvTemp.put(0, 0, frameData);
		Mat mRgba = new Mat();
		Mat mRgbaResized = new Mat();
		Imgproc.cvtColor(mYuvTemp, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
		Imgproc.resize(mRgba, mRgbaResized,
				new Size(mRgba.width(), mRgba.height() / 2));
		return mRgbaResized;
	};

	public Mat rawImageData2MatGRAY(byte[] frameData) {
		Mat mYuvTemp = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2,
				PreviewSizeWidth, CvType.CV_8UC1);
		mYuvTemp.put(0, 0, frameData);
		Mat mGray = new Mat();
		Mat mGrayResized = new Mat();
		Imgproc.cvtColor(mYuvTemp, mGray, Imgproc.COLOR_YUV2GRAY_NV21, 1);
		Imgproc.resize(mGray, mGrayResized,
				new Size(mGray.width(), mGray.height() / 2));
		return mGrayResized;
	};

	// Auxiliar functions
	private Mat computeDepthMapBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params, boolean useROI) {

		Mat dispMap = new Mat();
		NativeDepthMapBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), valiROILeft.getNativeObjAddr(),
				validROIRight.getNativeObjAddr(), dispMap.getNativeObjAddr(),
				imageDepth.getNativeObjAddr(),
				imageDispMapOR.getNativeObjAddr(), qMat.getNativeObjAddr(),
				params.SADWindowSize, params.minDisparity,
				params.numberOfDisparities, params.textureThresh,
				params.uniquenessRatio, params.speckeWidowSize,
				params.speckleRange, params.maxDisparityCheckDiff,
				params.prefilterCap, useROI, coloredMap);
		return dispMap;
	}

	private Mat computeDepthMapSGBM(Mat imageLeft, Mat imageRight,
			Mat valiROILeft, Mat validROIRight, Mat qMat,
			DepthParamsObject params, boolean useROI) {

		Mat dispMap = new Mat();
		NativeDepthMapSGBM(imageLeft.getNativeObjAddr(),
				imageRight.getNativeObjAddr(), dispMap.getNativeObjAddr(),
				imageDepth.getNativeObjAddr(),
				imageDispMapOR.getNativeObjAddr(), qMat.getNativeObjAddr(),
				params.SADWindowSize, params.minDisparity,
				params.numberOfDisparities, params.textureThresh,
				params.uniquenessRatio, params.speckeWidowSize,
				params.speckleRange, params.maxDisparityCheckDiff,
				params.prefilterCap, useROI, coloredMap);
		return dispMap;
	}

	// Native functions
	public native void NativeMatLoadProcess(long addrMat, String fileName,
			String matName);

	public native void NativeMatSaveProcess(long addrMat, String fileName,
			String matName);

	public native void NativeDepthMapBM(long addrImageLeft,
			long addrImageRight, long addrValidROILeftMat,
			long addrValidROIRightMat, long addrDispMap, long addrDepthMap,
			long addrDispMapOr, long addrQMat, int SADWindow, int minDisparity,
			int numberDisparities, int textureThreshold, int uniquenessRatio,
			int speckleWindowSize, int speckleRange, int disp12MaxDiff,
			int prefilterCap, boolean doRemap, boolean coloredMap);

	public native void NativeDepthMapSGBM(long addrImageLeft,
			long addrImageRight, long addrDispMap, long addrDepthMap,
			long addrDispMapOr, long addrQMat, int SADWindow, int minDisparity,
			int numberDisparities, int textureThreshold, int uniquenessRatio,
			int speckleWindowSize, int speckleRange, int disp12MaxDiff,
			int prefilterCap, boolean doRemap, boolean coloredMap);

	private void setupParamsScrollBars(DepthParamsObject Params) {
		numberOfDisparitiesSeek.setProgress(Params.numberOfDisparities / 16);
		minDisparitySeek.setProgress(Params.minDisparity / 16 + 10);
		SADWindowSizeSeek.setProgress((Params.SADWindowSize - 1) / 2);
		textureThreshSeek.setProgress(Params.textureThresh / 16);
		uniquenessRatioSeek.setProgress(Params.uniquenessRatio);
		MaxDisparityCheckDiffSeek.setProgress(Params.maxDisparityCheckDiff);
		speckleWindowSeek.setProgress(Params.speckeWidowSize);
		speckleRangeSeek.setProgress(Params.speckleRange);
		prefilterCapSeek.setProgress(Params.prefilterCap);

		SADWindowSizeText.setText(mResources
				.getString(R.string.param_block_size)
				+ String.valueOf(ALGParams.SADWindowSize));
		numberOfDisparitiesText.setText(mResources
				.getString(R.string.param_number_disparities)
				+ String.valueOf(ALGParams.numberOfDisparities));
		minDisparityText.setText(mResources
				.getString(R.string.param_min_disparity)
				+ String.valueOf(ALGParams.minDisparity));
		textureThreshText.setText(mResources
				.getString(R.string.param_texture_thresh)
				+ String.valueOf(ALGParams.textureThresh));
		uniquenessRatioText.setText(mResources
				.getString(R.string.param_uniqueness)
				+ String.valueOf(ALGParams.uniquenessRatio));
		speckleWindowText.setText(mResources
				.getString(R.string.param_speckle_window)
				+ String.valueOf(ALGParams.speckeWidowSize));
		speckleRangeText.setText(mResources
				.getString(R.string.param_speckle_range)
				+ String.valueOf(ALGParams.speckleRange));
		MaxDisparityCheckDiffText.setText(mResources
				.getString(R.string.param_disp12maxdiff)
				+ String.valueOf(ALGParams.maxDisparityCheckDiff));
		prefilterCapText.setText(mResources
				.getString(R.string.param_prefilter_cap)
				+ String.valueOf(ALGParams.prefilterCap));
	}

	private void deactivateSeekbars() {
		SADWindowSizeSeek.setEnabled(false);
		numberOfDisparitiesSeek.setEnabled(false);
		minDisparitySeek.setEnabled(false);
		textureThreshSeek.setEnabled(false);
		uniquenessRatioSeek.setEnabled(false);
		speckleWindowSeek.setEnabled(false);
		speckleRangeSeek.setEnabled(false);
		MaxDisparityCheckDiffSeek.setEnabled(false);
		prefilterCapSeek.setEnabled(false);
	}

	private void activateSeekbars() {
		SADWindowSizeSeek.setEnabled(true);
		numberOfDisparitiesSeek.setEnabled(true);
		minDisparitySeek.setEnabled(true);
		textureThreshSeek.setEnabled(true);
		uniquenessRatioSeek.setEnabled(true);
		speckleWindowSeek.setEnabled(true);
		speckleRangeSeek.setEnabled(true);
		MaxDisparityCheckDiffSeek.setEnabled(true);
		prefilterCapSeek.setEnabled(true);
	}

	private class MySeekBarListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			switch (seekBar.getId()) {
			case R.id.param_seekbar1:
				if (stereoMethod == StereoMethod.BM) {
					if (progress < 2)
						ALGParams.SADWindowSize = 5;
					else
						ALGParams.SADWindowSize = (2 * progress + 1);
				} else {
					if (progress < 1)
						ALGParams.SADWindowSize = 3;
					else
						ALGParams.SADWindowSize = (2 * progress + 1);
				}
				SADWindowSizeText.setText(mResources
						.getString(R.string.param_block_size)
						+ String.valueOf(ALGParams.SADWindowSize));
				break;
			case R.id.param_seekbar2:
				if (progress < 1) {
					ALGParams.numberOfDisparities = 16;
				} else {
					ALGParams.numberOfDisparities = progress * 16;
				}
				numberOfDisparitiesText.setText(mResources
						.getString(R.string.param_number_disparities)
						+ String.valueOf(ALGParams.numberOfDisparities));
				break;
			case R.id.param_seekbar3:
				ALGParams.minDisparity = (progress - 10) * 16;
				minDisparityText.setText(mResources
						.getString(R.string.param_min_disparity)
						+ String.valueOf(ALGParams.minDisparity));
				break;
			case R.id.param_seekbar4:
				ALGParams.textureThresh = progress * 16;
				textureThreshText.setText(mResources
						.getString(R.string.param_texture_thresh)
						+ String.valueOf(ALGParams.textureThresh));
				break;

			case R.id.param_seekbar5:
				if (progress <= 0)
					ALGParams.uniquenessRatio = 1;
				else
					ALGParams.uniquenessRatio = progress;
				uniquenessRatioText.setText(mResources
						.getString(R.string.param_uniqueness)
						+ String.valueOf(ALGParams.uniquenessRatio));
				break;
			case R.id.param_seekbar6:
				ALGParams.speckeWidowSize = progress;
				speckleWindowText.setText(mResources
						.getString(R.string.param_speckle_window)
						+ String.valueOf(ALGParams.speckeWidowSize));
				break;
			case R.id.param_seekbar7:
				ALGParams.speckleRange = progress;
				speckleRangeText.setText(mResources
						.getString(R.string.param_speckle_range)
						+ String.valueOf(ALGParams.speckleRange));
				break;
			case R.id.param_seekbar8:
				ALGParams.maxDisparityCheckDiff = progress;
				MaxDisparityCheckDiffText.setText(mResources
						.getString(R.string.param_disp12maxdiff)
						+ String.valueOf(ALGParams.maxDisparityCheckDiff));
				break;
			case R.id.param_seekbar9:
				if (progress < 1)
					ALGParams.prefilterCap = 1;
				else
					ALGParams.prefilterCap = progress;

				prefilterCapText.setText(mResources
						.getString(R.string.param_prefilter_cap)
						+ String.valueOf(ALGParams.prefilterCap));
				break;
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			synchronized (ALGParamsSynch) {
				ALGParamsSynch = ALGParams;
			}
		}
	}

	private boolean allocateMats() {
		QMat = new Mat();
		imageLeft = new Mat();
		imageRight = new Mat();
		rectifiedLeft = new Mat();
		rectifiedRight = new Mat();
		map1Left = new Mat();
		map1Right = new Mat();
		map2Left = new Mat();
		map2Right = new Mat();
		imageDisp = new Mat();
		validROIRightMat = new Mat();
		validROILeftMat = new Mat();
		imageDepth = new Mat();
		imageDispMapOR = new Mat();
		leftUp = new Mat();
		leftDown = new Mat();
		rightUp = new Mat();
		rightDown = new Mat();
		return true;
	}

	private void loadMats() {
		NativeMatLoadProcess(QMat.getNativeObjAddr(), imageDirectoryBase
				+ "QMatrix.yml", "Q_Matrix");
		NativeMatLoadProcess(map1Left.getNativeObjAddr(), imageDirectoryBase
				+ "Remap1Left.yml", "Remap1_Matrix");
		NativeMatLoadProcess(map2Left.getNativeObjAddr(), imageDirectoryBase
				+ "Remap2Left.yml", "Remap2_Matrix");
		NativeMatLoadProcess(map1Right.getNativeObjAddr(), imageDirectoryBase
				+ "Remap1Right.yml", "Remap1_Matrix");
		NativeMatLoadProcess(map2Right.getNativeObjAddr(), imageDirectoryBase
				+ "Remap2Right.yml", "Remap2_Matrix");
		NativeMatLoadProcess(validROILeftMat.getNativeObjAddr(),
				imageDirectoryBase + "ValidROILeft.yml", "ROILeft_Matrix");
		NativeMatLoadProcess(validROIRightMat.getNativeObjAddr(),
				imageDirectoryBase + "ValidROIRight.yml", "ROIRight_Matrix");
	}

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV loaded successfully");
				System.loadLibrary("ecivision-depth-generator-live");
				matsAllocated = allocateMats();
				Log.e(TAG, "Mats allocated correctly");
				if (doRemap)
					loadMats();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	// Extremely important line, loads OpenCV Library
	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}

	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onActivityResult(int aRequestCode, int aResultCode,
			Intent aData) {

		super.onActivityResult(aRequestCode, aResultCode, aData);
		imagePathBase = Environment.getExternalStorageDirectory() + "/DCIM2/"
				+ aData.getStringExtra("pathbase");
		imageExtension = aData.getStringExtra("extention");
		doRemap = aData.getBooleanExtra("doremap", false);
		imageNameBase = aData.getStringExtra("namebase");
		imageDirectoryBase = Environment.getExternalStorageDirectory()
				+ "/DCIM2/" + aData.getStringExtra("directorybase");

		Log.e(TAG, "Image path is: " + imagePathBase);
		Log.e(TAG, "Image Extention is: " + imageExtension);
		Log.e(TAG, "Image remap is: " + doRemap);
	}

}
