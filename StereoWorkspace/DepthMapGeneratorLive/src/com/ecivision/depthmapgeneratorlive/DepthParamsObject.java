package com.ecivision.depthmapgeneratorlive;

import java.io.Serializable;

public class DepthParamsObject implements Serializable {

	private static final long serialVersionUID = 0L;
	public int numberOfDisparities = 0;
	public int minDisparity = 0;
	public int SADWindowSize = 9;
	public int textureThresh = 0;


	// Margin in percentage by which the best (minimum) computed cost function
	// value should “win” the second best value to consider the found match
	// correct. Normally, a value within the 5-15 range is good enough.
	// Basically defines if it is only permitted one or various matches.
	public int uniquenessRatio = 5;

	// Maximum allowed difference in the left right disparity check.
	public int maxDisparityCheckDiff = 0;

	// Speckle post filtering. Window size between 50 and 200 or 0 for disabled.
	// Range usually between 1 and 2.
	public int speckeWidowSize = 0;
	public int speckleRange = 0;

	// Smooth parameters available only to SGBM P2>P1. A good estimate is:
	// 8*number_of_image_channels*SADWindowSize*SADWindowSize and
	// 32*number_of_image_channels*SADWindowSize*SADWindowSize
	public int sgbmSmoothP1 = 0;
	public int sgbmSmoothP2 = 0;
	
	public int prefilterCap = 0;
	
	public DepthParamsObject() {
		numberOfDisparities = 32;
		minDisparity = 0;
		SADWindowSize = 9;
		textureThresh = 100;
		// Margin in percentage by which the best (minimum) computed cost function
		// value should “win” the second best value to consider the found match
		// correct. Normally, a value within the 5-15 range is good enough.
		// Basically defines if it is only permitted one or various matches.
		uniquenessRatio = 18;

		// Maximum allowed difference in the left right disparity check.
		maxDisparityCheckDiff = 1;
		speckeWidowSize = 100;
		speckleRange = 32;

		// Smooth parameters available only to SGBM P2>P1. A good estimate is:
		// 8*number_of_image_channels*SADWindowSize*SADWindowSize and
		// 32*number_of_image_channels*SADWindowSize*SADWindowSize
		sgbmSmoothP1 = 0;
		sgbmSmoothP2 = 0;
		
		prefilterCap = 31;
	}

}
