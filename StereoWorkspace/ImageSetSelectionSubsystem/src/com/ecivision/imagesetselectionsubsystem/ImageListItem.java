package com.ecivision.imagesetselectionsubsystem;

import java.io.Serializable;

public class ImageListItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1653434675415028227L;
	private String imageFolder;
	private String imageExtention;
	private String imageBaseName;
	private int numberImages;
	private boolean doImageRemap;

	public ImageListItem(String _imageSetFolder, String _imageSetBaseName,
			String _imageSetExtention, int _numberImages, boolean _doImageRemap) {
		this.imageFolder = _imageSetFolder;
		this.imageExtention = "."+_imageSetExtention;
		this.imageBaseName = _imageSetBaseName;
		this.numberImages = _numberImages;
		this.doImageRemap = _doImageRemap;
	}

	public String getImagePathBase() {
		return imageFolder + "/" + imageBaseName + "_";
	}
	
	public String getImageDirectoryBase(){
		return imageFolder + "/";
	}
	public String getImageBaseName(){
		return imageBaseName + "_";
	}

	public String getImageExtention() {
		return imageExtention;
	}

	public int getNumberImages() {
		return numberImages;
	}
	
	public boolean getDoImageRemap(){
		return doImageRemap;
	}
	
	public void setDoImageRemap(boolean _doImageRemap){
		doImageRemap=_doImageRemap;
	}

	public void setImageExtention(String _imageSetExtention) {
		this.imageExtention = "."+_imageSetExtention;
	}

	public void setNumberImages(int _numberImages) {
		this.numberImages = _numberImages;
	}

	public void setImageFolder(String _imageSetExtention) {
		this.imageExtention = _imageSetExtention;
	}

	public void setImageBaseName(String _imageBaseName) {
		this.imageExtention = _imageBaseName;
	}

}
