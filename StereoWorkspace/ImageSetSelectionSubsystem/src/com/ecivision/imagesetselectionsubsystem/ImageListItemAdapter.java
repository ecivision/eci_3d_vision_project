package com.ecivision.imagesetselectionsubsystem;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ImageListItemAdapter extends ArrayAdapter<ImageListItem> {
	
	Context mContext;

	public ImageListItemAdapter(Context context, int textViewResourceId,
			ArrayList<ImageListItem> imagesListItems) {
		super(context, textViewResourceId, imagesListItems);
		mContext=context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
		ImageListItem i = getItem(position);
		
		if(v==null){
			LayoutInflater listItemViewInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			v = listItemViewInflater.inflate(R.layout.image_list_item, null);
		}
		if(i!=null){
			TextView directoryTextView = (TextView) v.findViewById(R.id.image_folder_textview);
			TextView numberTextView = (TextView) v.findViewById(R.id.image_number_textview);
			TextView extentionTextView = (TextView) v.findViewById(R.id.image_extension_textview);
			TextView doremapTextView  = (TextView) v.findViewById(R.id.do_image_remap_textview);
			
			if(directoryTextView != null){
				directoryTextView.setText(i.getImagePathBase());
			}
			if(numberTextView != null){
				numberTextView.setText(String.valueOf(i.getNumberImages()));
			}
			if(extentionTextView != null){
				extentionTextView.setText(i.getImageExtention());
			}
			if(doremapTextView != null){
				doremapTextView.setText(String.valueOf(i.getDoImageRemap()));
			}
		}
		return v;
	}
	
	public void setData(ArrayList<ImageListItem> data) {
	    clear();
	    if (data != null) {
	        //If the platform supports it, use addAll, otherwise add in loop
	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	            addAll(data);
	            notifyDataSetChanged();
	        }else{
	            for(ImageListItem item: data){
	                add(item);
	            }
	            notifyDataSetChanged();
	        }
	    }
	}

}
