package com.ecivision.imagesetselectionsubsystem;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class ImageSelectionActivity extends Activity {

	static private ArrayList<ImageListItem> imagesList;
	private ImageListItemAdapter imagesListAdapter;
	private ImageListItem imagesItemTemp;
	private ListView imagesListView;
	private Button okButton;
	private Button newItemButton;
	private Context mContext;
	private EditText folderNameEditText, imageBaseNameEditText,
			imageExtensionEditText, numberImagesEditText;
	private Button okButtonDialog;
	private Dialog newImageSetDialog;
	private CheckBox doImageRemapCheckbox;
//	private boolean doImageRemap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_selection);
		mContext = this;
		imagesListView = (ListView) findViewById(R.id.image_set_listview);
		okButton = (Button) findViewById(R.id.image_to_depth_button);
		newItemButton = (Button) findViewById(R.id.image_new_button);
		imagesList = new ArrayList<ImageListItem>();
		imagesListAdapter = new ImageListItemAdapter(this,
				R.layout.image_list_item, imagesList);
		imagesListView.setAdapter(imagesListAdapter);
		loadImagesListFile();

		newImageSetDialog = new Dialog(mContext);
		newImageSetDialog.setContentView(R.layout.dialog);
		newImageSetDialog.setTitle("New Image Set Entry");
		okButtonDialog = (Button) newImageSetDialog
				.findViewById(R.id.ok_button_dialog);
		folderNameEditText = (EditText) newImageSetDialog
				.findViewById(R.id.folder_name_edit_text);
		imageExtensionEditText = (EditText) newImageSetDialog
				.findViewById(R.id.extension_name_edit_text);
		numberImagesEditText = (EditText) newImageSetDialog
				.findViewById(R.id.number_images_dialog_edit_text);
		imageBaseNameEditText = (EditText) newImageSetDialog
				.findViewById(R.id.image_basename_edit_text);
		doImageRemapCheckbox = (CheckBox) newImageSetDialog
				.findViewById(R.id.do_image_remap_checkbox);
		// doImageRemapCheckbox.setChecked(true);

		newItemButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				newImageSetDialog.show();
			}
		});

		okButtonDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imagesList.add(new ImageListItem(folderNameEditText.getText()
						.toString(),
						imageBaseNameEditText.getText().toString(),
						imageExtensionEditText.getText().toString(), Integer
								.parseInt(numberImagesEditText.getText()
										.toString()), doImageRemapCheckbox
								.isChecked()));
//				imagesListAdapter.clear();
//				imagesListAdapter.addAll(imagesList);
				imagesListAdapter.setData(imagesList);
				imagesListAdapter.notifyDataSetChanged();
				imagesListView.invalidateViews();
				saveImagesListFile();
				newImageSetDialog.dismiss();
			}
		});

		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent _result = new Intent();
				_result.putExtra("pathbase", imagesItemTemp.getImagePathBase());
				_result.putExtra("extention",
						imagesItemTemp.getImageExtention());
				_result.putExtra("directorybase", imagesItemTemp.getImageDirectoryBase());
				_result.putExtra("namebase",
						imagesItemTemp.getImageBaseName());
				_result.putExtra("numberimages",
						imagesItemTemp.getNumberImages());
				_result.putExtra("doremap", imagesItemTemp.getDoImageRemap());
				setResult(Activity.RESULT_OK, _result);
				finish();
			}
		});

		imagesListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int position, long id) {
						imagesList.remove(position);
//						imagesListAdapter.clear();
//						imagesListAdapter.addAll(imagesList);
						imagesListAdapter.setData(imagesList);
						imagesListAdapter.notifyDataSetChanged();
						imagesListView.invalidateViews();
						okButton.setEnabled(false);
						saveImagesListFile();
						return true;
					}
				});

		imagesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				imagesItemTemp = imagesList.get(position);
				okButton.setEnabled(true);
			}
		});

	}

	@SuppressWarnings("unchecked")
	private void loadImagesListFile() {
		String ser2 = SerializeObject.ReadSettings(mContext,
				"images_list_object.dat");
		if (ser2 != null && !ser2.equalsIgnoreCase("")) {
			Object obj = SerializeObject.stringToObject(ser2);
			// Then cast it to your object and
			if (obj instanceof ArrayList) {
				// Do something
				imagesList = (ArrayList<ImageListItem>) obj;
				imagesListAdapter.setData(imagesList);
//				imagesListAdapter.addAll(imagesList);
				imagesListAdapter.notifyDataSetChanged();
				imagesListView.invalidateViews();
			}
		}
	}

	private void saveImagesListFile() {
		String ser = SerializeObject.objectToString(imagesList);
		if (ser != null && !ser.equalsIgnoreCase("")) {
			SerializeObject.WriteSettings(mContext, ser,
					"images_list_object.dat");
		} else {
			SerializeObject.WriteSettings(mContext, "",
					"images_list_object.dat");
		}
	}
	
	  public void onDestroy() {
	        super.onDestroy();
	        android.os.Process.killProcess(android.os.Process.myPid());
	    }  

}
