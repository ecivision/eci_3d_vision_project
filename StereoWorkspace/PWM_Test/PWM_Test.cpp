 // Do not remove the include below
#include "PWM_Test.h"
int pinPWM3 = 3;
int pinPWM2 = 2;
int inPWM=0;

void serialEvent(){
	inPWM = Serial.parseInt();
	Serial.print("Got : ");
	Serial.println(inPWM);
	if(inPWM<=170){
		analogWrite(pinPWM3,inPWM);
	}
}

void setup() {
	TCCR3B = (TCCR3B & 0b11111000) | 2;
	pinMode(pinPWM3,OUTPUT);
	pinMode(pinPWM2,OUTPUT);
	analogWrite(pinPWM3,50);
	analogWrite(pinPWM2,150);
	Serial.begin(57600);
	Serial.setTimeout(10);
}

// The loop function is called in an endless loop
void loop() {

}
