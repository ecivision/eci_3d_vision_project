/* LICENSE: You can do whatever you want with this, on four conditions.
 * 1) Share and share alike. This means source, too.
 * 2) Acknowledge attribution to spiritplumber@gmail.com in your code.
 * 3) Email me to tell me what you're doing with this code! I love to know people are doing cool stuff!
 * 4) You may NOT use this code in any sort of weapon.
 */

package re.serialout;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AudioSerialOutDemo extends Activity {

	static public final char cr = (char) 13; // because i don't want to type that in every time
	static public final char lf = (char) 10; // because i don't want to type that in every time
	public String datatosend = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		AudioSerialOutMono.activate();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		final EditText editbox = (EditText) findViewById(R.id.EditText01);
		final EditText baudbox = (EditText) findViewById(R.id.EditText02);
		final EditText charbox = (EditText) findViewById(R.id.EditText03);
		final Button savebutton = (Button) findViewById(R.id.Button01 );
		editbox.setText("Audio Serial Out Demo by mkb@robots-everywhere.com"+cr+lf+"http://bit.ly/bOVi2d"+cr+lf+"See our wiki for circuit schematics and layout!"+cr+lf+"NEW: Use the Output intent to integrate this into your own app!");
		baudbox.setText("14400");
		charbox.setText("002");
		savebutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				try{
					AudioSerialOutMono.new_baudRate = Integer.parseInt(baudbox.getText().toString());
				}catch(Exception e){
					AudioSerialOutMono.new_baudRate = 14400;
					baudbox.setText("14400");
				}
				try{
					AudioSerialOutMono.new_characterdelay = Integer.parseInt(charbox.getText().toString());
				}catch(Exception e){
					AudioSerialOutMono.new_characterdelay = 2;
					charbox.setText("002");
				}
				
				AudioSerialOutMono.new_levelflip = false;
				AudioSerialOutMono.UpdateParameters(true);
				AudioSerialOutMono.output(cr+editbox.getText().toString()+cr);
			}
		});

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// these show up in the primary screen: out of order for display reasons
		menu.add(0, 0, 0, "EXITING");
		return true;
	}
	public boolean onPrepareOptionsMenu(Menu menu) {
		android.os.Process.killProcess(android.os.Process.myPid());
		return true;
	}


}

