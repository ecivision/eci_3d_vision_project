package com.ecivision.bluetoothimagetransfer;

import java.io.File;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class BluetoothMain extends Activity {

	private Button SendButton;
	private Intent bdIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetooth_share);
		SendButton = (Button) findViewById(R.id.SendButton);
		bdIntent = new Intent();
		bdIntent.setAction(Intent.ACTION_SEND);
		bdIntent.setType("image/*");
		bdIntent.putExtra(
				Intent.EXTRA_STREAM,
				Uri.parse("file://" + Environment.getExternalStorageDirectory()
						+ "/DCIM/100ANDRO/" + "StereoChess_1_L.JPG"));

		SendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(bdIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth_share, menu);
		return true;
	}

}
