package com.ecivision.stereocameratest;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import com.ecivision.stereocameratest.StereoCameraBridgeViewBase.CvCameraViewFrame;
import com.ecivision.stereocameratest.StereoCameraBridgeViewBase.CvCameraViewListener2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;

public class StereoCameraTestActivity extends Activity  implements CvCameraViewListener2{

	private static final String TAG = "StereoTestActivity";
	private SurfaceView fakeview;
	private JavaStereoCameraView stereoCamera;
	private Mat temp;
	private FrameLayout cameraFrameLayout;
	private int PreviewSizeWidth = 640;
	private int PreviewSizeHeight = 480;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.e(TAG, "OpenCV Library Loaded Successfully");
				// stereoCam.setMaxFrameSize(640, 480);
				stereoCamera.enableView();
				// stereoCam.enableFpsMeter();
				break;
			}
			default: {
				Log.e(TAG, "ERROR Could not Load OpenCV Library");
				super.onManagerConnected(status);
				break;
			}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stereo_camera_test);
		
		cameraFrameLayout = (FrameLayout) findViewById(R.id.camera_frame_layout);
		stereoCamera = new JavaStereoCameraView(this, 2,PreviewSizeWidth,PreviewSizeHeight);
		stereoCamera.setCvCameraViewListener(this);
		cameraFrameLayout.addView(stereoCamera,new LayoutParams(PreviewSizeWidth,PreviewSizeHeight));
		
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
	Log.e(TAG,"Hello CameraView");
	temp=new Mat();
	}
	
	
	@Override
	public void onPause() {
		if (stereoCamera != null)
			stereoCamera.disableView();
		super.onPause();
	}


	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
	}
	
	@Override
	public void onCameraViewStopped() {
		temp.release();
	}



	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		Log.e(TAG,"Hello Callback");
		Core.bitwise_not(inputFrame.rgba(), temp);
		return temp;
	}

}
