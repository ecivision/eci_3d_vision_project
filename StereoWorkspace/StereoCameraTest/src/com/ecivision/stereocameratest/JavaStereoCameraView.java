package com.ecivision.stereocameratest;

import java.io.IOException;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;

public class JavaStereoCameraView extends StereoCameraBridgeViewBase implements
		Camera.PreviewCallback {

	// private static final int MAGIC_TEXTURE_ID = 10;
	private static final String TAG = "JavaCameraView";

	private byte mBuffer[];
	private Mat[] mFrameChain;
	private int mChainIdx = 0;
	private Thread mThread;
	private boolean mStopThread;
	private int mWidth, mHeight;

	protected Camera mCamera;
	protected JavaCameraFrame mCameraFrame;

	public static final String KEY_CAMERA_INDEX = "camera-index";

	// private SurfaceTexture mSurfaceTexture;

	public static class JavaCameraSizeAccessor implements ListItemAccessor {

		public int getWidth(Object obj) {
			Camera.Size size = (Camera.Size) obj;
			return size.width;
		}

		public int getHeight(Object obj) {
			Camera.Size size = (Camera.Size) obj;
			return size.height;
		}
	}

	public JavaStereoCameraView(Context context, int cameraId, int width,
			int height) {
		super(context, cameraId, width, height);
	}

	public JavaStereoCameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d(TAG, "Java camera view ctor");
	}

	protected boolean initializeCamera(int width, int height) {
		Log.d(TAG, "Initialize java camera");
		boolean result = true;
		synchronized (this) {
			mCamera = null;

			if (mCameraIndex == -1) {
				Log.e(TAG, "Trying to open camera with old open()");
				try {
					mCamera = Camera.open(super.mCameraIndex);
					Log.e(TAG, "Camera #" + " 2" + " opened");
				} catch (Exception e) {
					Log.e(TAG,
							"Camera is not available (in use or does not exist): "
									+ e.getLocalizedMessage());
				}

				if (mCamera == null
						&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
					boolean connected = false;
					for (int camIdx = 0; camIdx <= 2; ++camIdx) {
						Log.e(TAG, "Trying to open camera with new open("
								+ Integer.valueOf(camIdx) + ")");
						try {
							mCamera = Camera.open(camIdx);
							connected = true;
							Log.e(TAG, "Camera #" + camIdx + " opened");
						} catch (RuntimeException e) {
							Log.e(TAG, "Camera #" + camIdx + "failed to open: "
									+ e.getLocalizedMessage());
						}
						if (connected)
							break;
					}
				}
			} else {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
					Log.e(TAG,
							"Trying to open camera with new open("
									+ Integer.valueOf(mCameraIndex) + ")");
					try {
						mCamera = Camera.open(mCameraIndex);
						Log.e(TAG, "Trying to open camera with new open("
								+ Integer.valueOf(mCameraIndex) + ")");
						Log.e(TAG, "Camera #" + mCameraIndex + " opened");
					} catch (RuntimeException e) {
						Log.e(TAG, "Camera #" + mCameraIndex
								+ "failed to open: " + e.getLocalizedMessage());
					}
				}
			}

			if (mCamera == null)
				return false;

			/* Now set camera parameters */
			try {
				Camera.Parameters params = mCamera.getParameters();
				Log.d(TAG, "getSupportedPreviewSizes()");
				List<android.hardware.Camera.Size> sizes = params
						.getSupportedPreviewSizes();

				if (sizes != null) {
					/*
					 * Select the size that fits surface considering maximum
					 * size allowed
					 */
					org.opencv.core.Size frameSize = new Size(
							super.mFrameWidth, super.mFrameHeight);

					params.setPreviewFormat(ImageFormat.NV21);
					Log.d(TAG,
							"Set preview size to "
									+ Integer.valueOf((int) frameSize.width)
									+ "x"
									+ Integer.valueOf((int) frameSize.height));
					params.setPreviewSize((int) frameSize.width,
							(int) frameSize.height);

					List<String> FocusModes = params.getSupportedFocusModes();
					if (FocusModes != null) {
						if (FocusModes
								.contains(Camera.Parameters.FOCUS_MODE_FIXED)) {
							params.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
							Log.d(TAG, "Set Fixed Focus Mode");
						} else if (FocusModes
								.contains(Camera.Parameters.FOCUS_MODE_INFINITY)) {
							params.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
							Log.d(TAG, "Set Infinity Focus Mode");
						}

					}

					mCamera.setParameters(params);
					params = mCamera.getParameters();

					mFrameWidth = params.getPreviewSize().width;
					mFrameHeight = params.getPreviewSize().height;

					if (mFpsMeter != null) {
						mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
					}

					int size = mFrameWidth * mFrameHeight;
					size = size
							* ImageFormat.getBitsPerPixel(params
									.getPreviewFormat()) / 8;
					mBuffer = new byte[size];

					mCamera.addCallbackBuffer(mBuffer);
					mCamera.setPreviewCallbackWithBuffer(this);
//					mCamera.setPreviewDisplay();
					
					mFrameChain = new Mat[2];
					mFrameChain[0] = new Mat(mFrameHeight + (mFrameHeight / 2),
							mFrameWidth, CvType.CV_8UC1);
					mFrameChain[1] = new Mat(mFrameHeight + (mFrameHeight / 2),
							mFrameWidth, CvType.CV_8UC1);

					AllocateCache();

					mCameraFrame = new JavaCameraFrame(mFrameChain[mChainIdx],
							mFrameWidth, mFrameHeight);
					
					// try {
					// mCamera.setPreviewDisplay(super.mHolder);
					// } catch (IOException e1) {
					// mCamera.release();
					// Log.e(TAG, "Set Preview Display Wrong: " +
					// e1.toString());
					// }

					// if (Build.VERSION.SDK_INT >=
					// Build.VERSION_CODES.HONEYCOMB) {
					// mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
					// mCamera.setPreviewTexture(mSurfaceTexture);
					// } else
					// SurfaceView fakeview = (SurfaceView) ((View)
					// getParent()).findViewById(R.id.stereo_fake_cam_preview);
					// fakeview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
					// mCamera.setPreviewDisplay(fakeview.getHolder());

					/* Finally we are ready to start the preview */
					Log.e(TAG, "startPreview");
					mCamera.startPreview();
				} else
					result = false;
			} catch (Exception e) {
				result = false;
				e.printStackTrace();
			}
		}

		return result;
	}

	protected void releaseCamera() {
		synchronized (this) {
			if (mCamera != null) {
				mCamera.stopPreview();
				mCamera.release();
			}
			mCamera = null;
			if (mFrameChain != null) {
				mFrameChain[0].release();
				mFrameChain[1].release();
			}
			if (mCameraFrame != null)
				mCameraFrame.release();
		}
	}

	@Override
	protected boolean connectCamera(int width, int height) {

		/*
		 * 1. We need to instantiate camera 2. We need to start thread which
		 * will be getting frames
		 */
		/* First step - initialize camera connection */
		Log.e(TAG, "Connecting to camera");
		if (!initializeCamera(width, height)) {
			Log.e(TAG, "Error initializing camera");
			return false;
		}

		/* now we can start update thread */
		Log.e(TAG, "Starting processing thread");
		mStopThread = false;
		mThread = new Thread(new CameraWorker());
		mThread.start();
		Log.e(TAG, "Processing thread started");
		return true;
	}

	protected void disconnectCamera() {
		/*
		 * 1. We need to stop thread which updating the frames 2. Stop camera
		 * and release it
		 */
		Log.e(TAG, "Disconnecting from camera");
		try {
			mStopThread = true;
			Log.e(TAG, "Notify thread");
			synchronized (this) {
				this.notify();
			}
			Log.e(TAG, "Wating for thread");
			if (mThread != null)
				mThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mThread = null;
		}

		/* Now release camera */
		releaseCamera();
	}

	public void onPreviewFrame(byte[] frame, Camera arg1) {
		Log.e(TAG,
				"Preview Frame received. Need to create MAT and deliver it to clients");
		Log.e(TAG, "Frame size  is " + frame.length);
		synchronized (this) {
			mFrameChain[1 - mChainIdx].put(0, 0, frame);
			this.notify();
		}
		if (mCamera != null)
			mCamera.addCallbackBuffer(mBuffer);
	}

	private class JavaCameraFrame implements CvCameraViewFrame {
		public Mat gray() {
			return mYuvFrameData.submat(0, mHeight, 0, mWidth);
		}

		public Mat rgba() {
			Imgproc.cvtColor(mYuvFrameData, mRgba, Imgproc.COLOR_YUV2BGR_NV12,
					4);
			return mRgba;
		}

		public JavaCameraFrame(Mat Yuv420sp, int width, int height) {
			super();
			mWidth = width;
			mHeight = height;
			mYuvFrameData = Yuv420sp;
			mRgba = new Mat();
		}

		public void release() {
			mRgba.release();
		}

		@SuppressWarnings("unused")
		private JavaCameraFrame(CvCameraViewFrame obj) {
		}

		private Mat mYuvFrameData;
		private Mat mRgba;
		private int mWidth;
		private int mHeight;
	};

	private class CameraWorker implements Runnable {

		public void run() {
			do {
				synchronized (JavaStereoCameraView.this) {
					Log.e(TAG, "Hello CameraWorker");
					try {
						Log.e(TAG, "Stuck Here");
						JavaStereoCameraView.this.wait();				
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (!mStopThread) {
					if (!mFrameChain[mChainIdx].empty()) {
						// Log.e(TAG, "Frame Delivered");
						deliverAndDrawFrame(mCameraFrame);
					}
					mChainIdx = 1 - mChainIdx;
				}
			} while (!mStopThread);
			Log.e(TAG, "Finish processing thread");
		}
	}
}
