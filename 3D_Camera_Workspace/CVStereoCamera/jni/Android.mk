LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include ${OPENCV_ANDROID_SDK}/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := stereo_cam
LOCAL_SRC_FILES := #jni_part.cpp
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
