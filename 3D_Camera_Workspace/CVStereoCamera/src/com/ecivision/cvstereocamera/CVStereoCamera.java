package com.ecivision.cvstereocamera;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

public class CVStereoCamera extends Activity implements CvCameraViewListener2 {
	
	private static final String TAG = "OCVSample::Activity";
//	private static final boolean CameraNative = false;

	private EciCameraBridgeViewBase mOpenCvCameraView;
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				//System.loadLibrary("ecivision-calibration");
				mOpenCvCameraView.enableView();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cvstereo_camera);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
//		if (CameraNative) {
//			mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.stereo_surface_native_view);
//		} else {
			mOpenCvCameraView = (EciCameraBridgeViewBase) findViewById(R.id.stereo_surface_java_view);
//		}
		mOpenCvCameraView.setCvCameraViewListener(this);
	}


	@Override
	public void onCameraViewStarted(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCameraViewStopped() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		return inputFrame.rgba();	
	}
	
	@Override
	protected void onResume() {
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_4, this,
				mLoaderCallback);
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
		super.onDestroy();
	}
	

}
