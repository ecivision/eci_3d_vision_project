package com.ecivision.lgstereocamtest;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.FrameLayout;

public class StereoCameraTest extends Activity {

	private static final String TAG = "Ecivision::StereoCamera::Activity";
	private Camera StereoCamera;
	private StereoCameraPreview StereoPreview;
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stereo_camera_test);
		mContext = this;
		StereoCamera= null;
		checkCameraHardware(mContext);
		StereoCamera = getCameraInstance(2);
		Log.e(TAG,"Camera instantiated correctly");
		StereoPreview = new StereoCameraPreview(mContext, StereoCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(StereoPreview);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stereo_camera_test, menu);
		return true;
	}

	private boolean checkCameraHardware(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        int numCams = Camera.getNumberOfCameras();
	        Log.e(TAG,numCams + " Cameras Found");
	        return true;
	    } else {
	        // no camera on this device
	        return false;
	    }
	}
	
	public static Camera getCameraInstance(int i){
	    Camera c = null;
	    try {
	        c = Camera.open(i); // attempt to get a Camera instance
	    }
	    catch (Exception e){
	        Log.e(TAG,"Camera error: "+ e.toString());
	    }
	    return c; // returns null if camera is unavailable
	}
	
	@Override
	protected void onPause() {
		StereoCamera.release();
		super.onPause();
	}
	
//	@Override
//	protected void onResume() {
//		StereoCamera = getCameraInstance(2);
//		super.onResume();
//	}
}


